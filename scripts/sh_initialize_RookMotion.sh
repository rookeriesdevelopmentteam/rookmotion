#!/usr/bin/env bash

sudo apt-get update

# System automations
cd /home/pi/RookMotion/scripts
# sudo sh ./sh_install_requirements.sh

#RookMotion
cd /home/pi/RookMotion/
sudo python3 SetDB.py
sudo sh /home/pi/RookMotion/scripts/sh_install_guacamole.sh

#WiFi config
cd /home/pi/RookMotion/
sudo python3 SetWiFi.py

#rev1 14/JAN/2019 Removed proxy, run_sh called
