#!/usr/bin/env bash

cat /dev/null > ~/.bash_history
sudo cat /dev/null > ~/.bash_history
rm ~/.bash_history
sudo rm ~/.bash_history

find . -name "*.c" -type f -delete
sudo rm -r ./build || true
sudo rm -r ./__pycache__ || true

sudo rm -rf ~/.local/share/Trash/* || true
sudo rm -rf /home/pi/.local/share/Trash/files/* || true
sudo rm -rf /home/pi/.local/share/Trash/info/* || true
sudo rm -rf .local/share/Trash/info/* || true
sudo rm -rf ~/root/.local/share/Trash/* || true


