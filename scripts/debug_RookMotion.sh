#!/usr/bin/env bash

cd /home/pi/RookMotion
find . -name "*.c" -type f -delete
find . -name "*.so" -type f -delete
sudo rm -r ./build || true

# git clean  -d  -fx

# sudo git fetch --all
sudo chmod -R 777 /home/pi/RookMotion
sudo chmod +x /home/pi/RookMotion/Tools/bdaddr

# Run RookMotion
sudo sh /home/pi/RookMotion/scripts/sh_run_RookMotion.sh


#rev1 24/JUN/2018 First release
#rev2 14/JAN/2019 Removed proxy, run_sh called