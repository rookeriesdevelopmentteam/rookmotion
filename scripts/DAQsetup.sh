#!/bin/bash
sudo leafpad /etc/wpa_supplicant/wpa_supplicant.conf
echo "Internal BT MAC"
hciconfig
echo "WiFi MAC"
cat /sys/class/net/wlan0/address
echo "Ethernet MAC"
cat /sys/class/net/eth0/address
echo "Serial number"
cat /proc/cpuinfo | grep Serial | cut -d ' ' -f 2
read -p "Presiona enter para configurar las credenciales del DAQ"
cd /home/pi/RookMotion/
sudo python3 create_server_settings.py