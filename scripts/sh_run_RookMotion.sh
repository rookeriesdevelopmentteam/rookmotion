#!/usr/bin/env bash

sudo mkdir -p /home/pi/RookMotion/logs
sudo mkdir -p /home/pi/RookMotion/reports
sudo chmod -R 777 /home/pi/RookMotion

cd /home/pi/RookMotion
sudo rm -f ./.git/index.lock || yes
sudo rm -f ./.git/index
sudo git reset --hard && sudo git pull
sudo git config --global user.name "DAQ"
sudo git config --global user.email daq@rookmotion.com

sudo chmod -R 777 /home/pi/RookMotion/Tools/bdaddr
sudo chmod -R 777 /home/pi/RookMotion

sudo rm ./reports/*.png

sudo sh scripts/sh_install_requirements.sh
sudo python3 ./RookMotion.py
