#!/usr/bin/env bash

sudo mkdir -p /home/pi/backup/
# sudo rsync -a /home/pi/RookMotion/logs/ /home/pi/backup/logs/
sudo rsync -a /home/pi/RookMotion/settings/ /home/pi/backup/settings/
# sudo rsync -a /home/pi/RookMotion/reports/ /home/pi/backup/reports/

sudo rm -r /home/pi/RookMotion/

# Clear History
cat /dev/null > ~/.bash_history
sudo cat /dev/null > ~/.bash_history
rm ~/.bash_history
sudo rm ~/.bash_history

# Clear Files
sudo rm -rf ~/.local/share/Trash/* || true
sudo rm -rf /home/pi/.local/share/Trash/files/* || true
sudo rm -rf /home/pi/.local/share/Trash/info/* || true
sudo rm -rf .local/share/Trash/info/* || true
sudo rm -rf ~/root/.local/share/Trash/* || true

# Get new
cd
sudo git clone https://RookeriesDevelopment@bitbucket.org/rookeriesdevelopmentteam/rookmotion.git /home/pi/RookMotion
# sudo rsync -a /home/pi/backup/logs/ /home/pi/RookMotion/logs/
sudo rsync -a /home/pi/backup/settings/ /home/pi/RookMotion/settings/
# sudo rsync -a /home/pi/backup/reports/ /home/pi/RookMotion/reports/

cd
sudo sh RookMotion/scripts/sh_run_RookMotion.sh