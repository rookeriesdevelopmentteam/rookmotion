#!/usr/bin/env bash
# Uninstall the required
sudo apt-get update
# Install the required
sudo apt-get --assume-yes install libcairo2-dev || true
sudo apt-get --assume-yes install libjpeg62-turbo-dev || true
# sudo apt-get --assume-yes install libjpeg62-dev || true
# sudo apt-get --assume-yes install libpng12-dev || true
sudo apt-get --assume-yes install libossp-uuid-dev || true
sudo apt-get --assume-yes install libavcodec-dev || true
sudo apt-get --assume-yes install libavutil-dev || true
sudo apt-get --assume-yes install libswscale-dev || true
sudo apt-get --assume-yes install libfreerdp-dev || true
sudo apt-get --assume-yes install libpango1.0-dev || true
sudo apt-get --assume-yes install libssh2-1-dev || true
sudo apt-get --assume-yes install libtelnet-dev || true
sudo apt-get --assume-yes install libvncserver-dev || true
sudo apt-get --assume-yes install libpulse-dev || true
sudo apt-get --assume-yes install libssl-dev || true
sudo apt-get --assume-yes install libvorbis-dev || true
sudo apt-get --assume-yes install libwebp-dev || true
sudo apt-get --assume-yes install libwebsockets || true
sudo apt-get --assume-yes install libfreerdp2 || true
sudo apt-get --assume-yes install libavformat || true

# CLean past installations
sudo rm -r /etc/guacamole
sudo rm -r guacamole-server-1.0.0 || true
sudo rm -r guacamole-server-1.2.0 || true
# sudo rm -r guacamole-client-0.9.14 || true
#
# Build and install the server:
cp /home/pi/RookMotion/Tools/guacamole-server-1.2.0.tar.gz /home/pi/
cd /home/pi/ || exit
tar xzf guacamole-server-1.2.0.tar.gz
cd guacamole-server-1.2.0 || exit
./configure --with-init-dir=/etc/init.d
make
sudo make install
sudo update-rc.d guacd defaults
sudo ldconfig

## Build the client:
#cp /home/pi/RookMotion/Tools/guacamole-client-0.9.14.tar.gz /home/pi/
#sudo apt-get --assume-yes install maven
#cd /home/pi/
#tar xzf guacamole-client-0.9.14.tar.gz
#cd guacamole-client-0.9.14
#mvn package

# Install jetty9 servlet container:
sudo apt-get --assume-yes install jetty9

# Deploy Guacamole:
# sudo cp guacamole/target/guacamole-0.9.14.war /var/lib/jetty9/webapps/guacamole.war
sudo cp /home/pi/RookMotion/Tools/guacamole-1.2.0.war /var/lib/jetty9/webapps/guacamole.war
sudo mkdir -p /etc/guacamole/extensions
# sudo cp extensions/guacamole-auth-noauth/target/guacamole-auth-noauth-0.9.14.jar /etc/guacamole/extensions/.

sudo cp /home/pi/RookMotion/scripts/guacamole.properties /etc/guacamole/guacamole.properties
# sudo cp /home/pi/RookMotion/scripts/noauth-config.xml /etc/guacamole/noauth-config.xml
# echo -n rook | md5sum
sudo cp /home/pi/RookMotion/scripts/user-mapping.xml /etc/guacamole/user-mapping.xml

sudo apt-get --assume-yes install x11vnc
sudo cp /home/pi/RookMotion/scripts/x11vnc.desktop /home/pi/.config/autostart/x11vnc.desktop
sudo chmod 644 /home/pi/.config/autostart/x11vnc.desktop