#!/usr/bin/env bash

cd /home/pi/RookMotion/Tools
hciconfig
# for i in {0..5}
# do
#   sudo ./bdaddr -i hci$i -r 0A:00:00:00:00:0$i
#done

sudo ./bdaddr -i hci0 -r 0A:00:00:00:00:00
sudo ./bdaddr -i hci1 -r 0A:00:00:00:00:01
sudo ./bdaddr -i hci2 -r 0A:00:00:00:00:02
sudo ./bdaddr -i hci3 -r 0A:00:00:00:00:03
sudo ./bdaddr -i hci4 -r 0A:00:00:00:00:04
sudo ./bdaddr -i hci5 -r 0A:00:00:00:00:05

sleep 1
hciconfig

#Notes:
# Complete steps: From https://scribles.net/changing-a-bluetooth-device-address-on-raspberry-pi/
# It's ok to try to change the internal address it won't work and nothing bad will occur

#rev1 2/FEB/2019 The tool is now in the Tools folder already compiled to save time and te requirements are pre-installed