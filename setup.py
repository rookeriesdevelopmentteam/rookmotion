import datetime
import multiprocessing
import sys
import os
import time
from setuptools import setup
from distutils.core import setup
from Cython.Build import cythonize
import __version__

NB_COMPILE_JOBS = 2

extensions = ["Acquisition/AcquisitionStorageHelper.py",
              "Acquisition/ThreadAcquisition.py",
              "BluetoothLE/BluetoothHelper.py",
              "BluetoothLE/BluetoothInterface.py",
              "BluetoothLE/HciTool.py",
              "BluetoothLE/Peripheral.py",
              "BluetoothLE/Scanner.py",
              "Cluster.py",
              "ClustersTools.py",
              "ColorPalettes.py",
              "ComboBoxesTools.py",
              "DebugTools.py",
              "DataBase/CreateDB.py",
              "DataBase/DB.py",
              "DataBase/DBCommon.py",
              "DataBase/DBGroups.py",
              "DataBase/DBRewards.py",
              "DataBase/DBSensors.py",
              "DataBase/DBSessions.py",
              "DataBase/DBSystem.py",
              "DataBase/DBUsers.py",
              "DataBase/DBUserSessionHelper.py",
              "DataBase/SQL.py",
              "ElasticLogger.py",
              "FilePaths.py",
              "Language.py",
              "Logger.py",
              "Network.py",
              "Rewards.py",
              "SessionRecords.py",
              "Sensors/SensorHR.py",
              "Sensors/SensorsHRModels.py",
              "Sensors/SensorsUtilities.py",
              "Settings.py",
              "ThreadConnections.py",
              "ThreadPlot.py",
              "ThreadScanner.py",
              "ThreadSchedule.py",
              "TrainingType.py",
              "GUI/TrainingTypeComboBox.py",
              "WebService/WS.py",
              "WebService/WSCommon.py",
              "WebService/WSSensors.py",
              "WebService/WSSessions.py",
              "WebService/WSSystem.py",
              "WebService/WSUsers.py",
              "Utilities.py"
              ]


def setup_given_extensions(extensions_):
    setup(
        name='RookMotion',
        version=__version__,
        description='RookMotion Data Acquisition System',
        author='RookDev',
        author_email='rookeries.dev@gmail.com',
        url='www.rookmotion.com/',

        ext_modules=cythonize(extensions_, language_level="3"),

        requires=['Cython', 'SIP', 'PyQt5', 'Crypto', 'MySQLdb', 'bluepy', 'numpy', 'matplotlib', 'validate_email',
                  'MySQLdb', 'argon2', 'schedule', 'getpass', 'shutil', 'scipy', 'pystache', 'getch']
    )


def setup_extensions_in_sequential():
    setup_given_extensions(extensions)


def setup_extensions_in_parallel():
    cythonize(extensions, nthreads=NB_COMPILE_JOBS, language_level="3")
    pool = multiprocessing.Pool(processes=NB_COMPILE_JOBS)
    pool.map(setup_given_extensions, extensions)
    pool.close()
    pool.join()


def write_version():
    file_path = '__version__.py'
    text = []
    date_compilation = datetime.datetime.now().strftime("%y.%m.%dr%H.%M.%S")
    text.append("__version__ = '{}'".format(date_compilation))

    f = open(file_path, "w")
    for line in text:
        f.write(line + '\r\n')
        f.close()


def delete_version():
    file_dir = './__version__.py'
    print("Deleting Version")
    if os.path.exists(file_dir):
        os.remove(file_dir)


def delete_sources():
    base_dir = "./"
    extensions.append("Setup.py")

    for file in extensions:
        file_dir = base_dir + file
        if os.path.exists(file_dir):
            print("Deleting {}".format(file_dir))
            os.remove(file_dir)


delete_version()
start = time.time()
if "build_ext" in sys.argv:
    setup_extensions_in_parallel()
else:
    setup_extensions_in_sequential()

delete_sources()
write_version()
end = time.time()
elapsed = end - start
if elapsed > 60:
    elapsed = elapsed/60.0
    elapsed = "{0:.2f} min".format(elapsed)

else:
    elapsed = "{0:.2f} sec".format(elapsed)

print("Compilation time: " + elapsed + " with {} threads".format(NB_COMPILE_JOBS))


