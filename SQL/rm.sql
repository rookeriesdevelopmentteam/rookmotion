-- MySQL Workbench Forward Engineering

SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_ENGINE_SUBSTITUTION';

-- -----------------------------------------------------
-- Schema rm
-- -----------------------------------------------------
DROP SCHEMA IF EXISTS `rm` ;

-- -----------------------------------------------------
-- Schema rm
-- -----------------------------------------------------
CREATE SCHEMA IF NOT EXISTS `rm` DEFAULT CHARACTER SET utf8 ;
USE `rm` ;

-- -----------------------------------------------------
-- Table `rm`.`automations`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `rm`.`automations` ;

CREATE TABLE IF NOT EXISTS `rm`.`automations` (
  `automation` VARCHAR(45) NULL DEFAULT NULL,
  `rationale` VARCHAR(100) NULL DEFAULT NULL,
  `executed` DATETIME NULL DEFAULT CURRENT_TIMESTAMP,
  UNIQUE INDEX `automation_UNIQUE` (`automation` ASC))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `rm`.`groups`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `rm`.`groups` ;

CREATE TABLE IF NOT EXISTS `rm`.`groups` (
  `group_id` INT(11) NOT NULL AUTO_INCREMENT,
  `group_name` VARCHAR(20) NULL DEFAULT NULL,
  `created` DATETIME NULL DEFAULT CURRENT_TIMESTAMP,
  `updated` DATETIME NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`group_id`),
  UNIQUE INDEX `group_name_UNIQUE` (`group_name` ASC))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `rm`.`users`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `rm`.`users` ;

CREATE TABLE IF NOT EXISTS `rm`.`users` (
  `user_id` INT(11) NOT NULL AUTO_INCREMENT,
  `user_id_server` INT(11) NULL DEFAULT NULL,
  `name` VARBINARY(128) NULL DEFAULT NULL,
  `email` VARBINARY(128) NOT NULL,
  `pseudonym` VARCHAR(16) NOT NULL,
  `sex` CHAR(1) NOT NULL,
  `birthdate` DATE NOT NULL,
  `active` TINYINT(4) NOT NULL DEFAULT '1',
  `created` DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated` DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`user_id`),
  UNIQUE INDEX `email_uniq` (`email` ASC),
  UNIQUE INDEX `user_id_server_uniq` (`user_id_server` ASC))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `rm`.`sensors`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `rm`.`sensors` ;

CREATE TABLE IF NOT EXISTS `rm`.`sensors` (
  `sensor_id` INT(11) NOT NULL AUTO_INCREMENT,
  `sensor_id_center` SMALLINT(6) NULL DEFAULT NULL,
  `sensor_id_server` INT(11) NULL DEFAULT NULL,
  `name` VARCHAR(45) NULL DEFAULT NULL,
  `model` VARCHAR(17) NOT NULL,
  `own_id` VARCHAR(17) NULL DEFAULT NULL,
  `brand` VARCHAR(45) NULL DEFAULT NULL,
  `mac` VARCHAR(18) NOT NULL,
  `user_id` INT(11) NULL DEFAULT NULL,
  `owner_type` VARCHAR(17) NOT NULL COMMENT 'Center or User or Borrowed',
  `active` TINYINT(4) NOT NULL DEFAULT '1',
  `created` DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated` DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`sensor_id`),
  UNIQUE INDEX `unique` (`mac` ASC, `model` ASC, `owner_type` ASC),
  INDEX `fk_sensor_user1_idx` (`user_id` ASC),
  CONSTRAINT `fk_sensor_user1`
    FOREIGN KEY (`user_id`)
    REFERENCES `rm`.`users` (`user_id`)
    ON DELETE CASCADE
    ON UPDATE CASCADE)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `rm`.`clusters`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `rm`.`clusters` ;

CREATE TABLE IF NOT EXISTS `rm`.`clusters` (
  `cluster_id` INT(11) NOT NULL AUTO_INCREMENT COMMENT 'id is the id of the record, it\'s not used currently\\\\nBut clusted_id is the possition on the GUI grid if clusters',
  `group_id` INT(11) NOT NULL,
  `cluster_id_gui` SMALLINT(6) NOT NULL COMMENT 'id is the id of the record, it\'s not used currently\\\\nBut clusted_id is the possition on the GUI grid if clusters',
  `user_id` INT(11) NULL DEFAULT NULL,
  `sensor_id` INT(11) NULL DEFAULT NULL,
  PRIMARY KEY (`cluster_id`, `group_id`),
  UNIQUE INDEX `unique` (`group_id` ASC, `cluster_id_gui` ASC),
  INDEX `fk_clusters_user1_idx` (`user_id` ASC),
  INDEX `fk_clusters_sensor1_idx` (`sensor_id` ASC),
  INDEX `fk_clusters_groups1_idx` (`group_id` ASC),
  CONSTRAINT `fk_clusters_groups1`
    FOREIGN KEY (`group_id`)
    REFERENCES `rm`.`groups` (`group_id`)
    ON DELETE CASCADE
    ON UPDATE CASCADE,
  CONSTRAINT `fk_clusters_sensor1`
    FOREIGN KEY (`sensor_id`)
    REFERENCES `rm`.`sensors` (`sensor_id`)
    ON DELETE SET NULL
    ON UPDATE CASCADE,
  CONSTRAINT `fk_clusters_user1`
    FOREIGN KEY (`user_id`)
    REFERENCES `rm`.`users` (`user_id`)
    ON DELETE SET NULL
    ON UPDATE CASCADE)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `rm`.`licence`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `rm`.`licence` ;

CREATE TABLE IF NOT EXISTS `rm`.`licence` (
  `status` VARCHAR(20) NULL DEFAULT NULL,
  `expiration` DATETIME NULL DEFAULT NULL,
  `updated` DATETIME NULL DEFAULT CURRENT_TIMESTAMP)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `rm`.`records_types`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `rm`.`records_types` ;

CREATE TABLE IF NOT EXISTS `rm`.`records_types` (
  `type_id` INT(11) NOT NULL AUTO_INCREMENT,
  `type_name` VARCHAR(45) NULL DEFAULT NULL,
  `created` DATETIME NULL DEFAULT CURRENT_TIMESTAMP,
  `updated` DATETIME NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`type_id`),
  UNIQUE INDEX `record_type_UNIQUE` (`type_name` ASC))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `rm`.`rewards_types`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `rm`.`rewards_types` ;

CREATE TABLE IF NOT EXISTS `rm`.`rewards_types` (
  `type_id` INT(11) NOT NULL AUTO_INCREMENT,
  `type_name` VARCHAR(45) NOT NULL,
  `created` DATETIME NULL DEFAULT CURRENT_TIMESTAMP,
  `updated` DATETIME NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`type_id`),
  UNIQUE INDEX `reward_type_UNIQUE` (`type_name` ASC))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `rm`.`training_types`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `rm`.`training_types` ;

CREATE TABLE IF NOT EXISTS `rm`.`training_types` (
  `type_id` INT(11) NOT NULL AUTO_INCREMENT,
  `type_name` VARCHAR(45) NULL DEFAULT NULL,
  `created` DATETIME NULL DEFAULT CURRENT_TIMESTAMP,
  `updated` DATETIME NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`type_id`),
  UNIQUE INDEX `trainig_type_UNIQUE` (`type_name` ASC))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `rm`.`sessions`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `rm`.`sessions` ;

CREATE TABLE IF NOT EXISTS `rm`.`sessions` (
  `session_id` INT(11) NOT NULL AUTO_INCREMENT,
  `training_type_id` INT(11) NOT NULL,
  `duration` INT(11) NULL DEFAULT NULL,
  `status` VARCHAR(45) NULL DEFAULT NULL,
  `created` DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated` DATETIME NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`session_id`, `training_type_id`),
  UNIQUE INDEX `unique` (`created` ASC, `training_type_id` ASC),
  INDEX `fk_sessions_training_types1_idx` (`training_type_id` ASC),
  CONSTRAINT `fk_sessions_training_types1`
    FOREIGN KEY (`training_type_id`)
    REFERENCES `rm`.`training_types` (`type_id`)
    ON DELETE CASCADE
    ON UPDATE CASCADE)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `rm`.`settings`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `rm`.`settings` ;

CREATE TABLE IF NOT EXISTS `rm`.`settings` (
  `setting_id` TINYINT(4) NOT NULL AUTO_INCREMENT,
  `setting` VARCHAR(20) NULL DEFAULT NULL,
  `value` VARCHAR(30) NULL DEFAULT NULL,
  `created` DATETIME NULL DEFAULT CURRENT_TIMESTAMP,
  `updated` DATETIME NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`setting_id`),
  UNIQUE INDEX `id_UNIQUE` (`setting_id` ASC),
  UNIQUE INDEX `setting_UNIQUE` (`setting` ASC))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `rm`.`summary_types`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `rm`.`summary_types` ;

CREATE TABLE IF NOT EXISTS `rm`.`summary_types` (
  `type_id` INT(11) NOT NULL,
  `type_id_server` INT(11) NULL,
  `type_name` VARCHAR(45) NULL DEFAULT NULL,
  `created` DATETIME NULL DEFAULT CURRENT_TIMESTAMP,
  `updated` DATETIME NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`type_id`),
  UNIQUE INDEX `record_type_UNIQUE` (`type_name` ASC),
  UNIQUE INDEX `type_id_server_UNIQUE` (`type_id_server` ASC))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `rm`.`sync`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `rm`.`sync` ;

CREATE TABLE IF NOT EXISTS `rm`.`sync` (
  `sync_id` INT(11) NOT NULL AUTO_INCREMENT,
  `component` VARCHAR(45) NULL DEFAULT NULL,
  `created` DATETIME NULL DEFAULT CURRENT_TIMESTAMP,
  `updated` DATETIME NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`sync_id`),
  UNIQUE INDEX `component_UNIQUE` (`component` ASC))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `rm`.`user_sessions`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `rm`.`user_sessions` ;

CREATE TABLE IF NOT EXISTS `rm`.`user_sessions` (
  `user_session_id` INT(11) NOT NULL AUTO_INCREMENT,
  `session_id` INT(11) NOT NULL,
  `user_id` INT(11) NOT NULL,
  `sensor_id` INT(11) NOT NULL,
  `status` VARCHAR(45) NULL DEFAULT 'running',
  `email_sent` DATETIME NULL DEFAULT NULL,
  `records_uploaded` DATETIME NULL DEFAULT NULL,
  `rewards_uploaded` DATETIME NULL DEFAULT NULL,
  `created` DATETIME NULL DEFAULT CURRENT_TIMESTAMP,
  `updated` DATETIME NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`user_session_id`, `session_id`, `user_id`, `sensor_id`),
  UNIQUE INDEX `unique` (`session_id` ASC, `user_id` ASC),
  INDEX `fk_user_session_sessions1_idx` (`session_id` ASC),
  INDEX `fk_user_session_users1_idx` (`user_id` ASC),
  INDEX `fk_user_session_sensors1_idx` (`sensor_id` ASC),
  CONSTRAINT `fk_user_session_sensors1`
    FOREIGN KEY (`sensor_id`)
    REFERENCES `rm`.`sensors` (`sensor_id`)
    ON DELETE CASCADE
    ON UPDATE CASCADE,
  CONSTRAINT `fk_user_session_sessions1`
    FOREIGN KEY (`session_id`)
    REFERENCES `rm`.`sessions` (`session_id`)
    ON DELETE CASCADE
    ON UPDATE CASCADE,
  CONSTRAINT `fk_user_session_users1`
    FOREIGN KEY (`user_id`)
    REFERENCES `rm`.`users` (`user_id`)
    ON DELETE CASCADE
    ON UPDATE CASCADE)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `rm`.`user_session_records`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `rm`.`user_session_records` ;

CREATE TABLE IF NOT EXISTS `rm`.`user_session_records` (
  `record_id` BIGINT(20) NOT NULL AUTO_INCREMENT,
  `user_session_id` INT(11) NOT NULL,
  `record_type_id` INT(11) NOT NULL,
  `value` VARCHAR(12) NOT NULL,
  `created` DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`record_id`, `user_session_id`, `record_type_id`),
  UNIQUE INDEX `UNIQUE` (`user_session_id` ASC, `created` ASC, `record_type_id` ASC),
  INDEX `fk_records_user_session1_idx` (`user_session_id` ASC),
  INDEX `fk_records_records_types2_idx` (`record_type_id` ASC),
  CONSTRAINT `fk_records_records_types2`
    FOREIGN KEY (`record_type_id`)
    REFERENCES `rm`.`records_types` (`type_id`)
    ON DELETE CASCADE
    ON UPDATE CASCADE,
  CONSTRAINT `fk_records_user_session1`
    FOREIGN KEY (`user_session_id`)
    REFERENCES `rm`.`user_sessions` (`user_session_id`)
    ON DELETE CASCADE
    ON UPDATE CASCADE)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `rm`.`user_session_rewards`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `rm`.`user_session_rewards` ;

CREATE TABLE IF NOT EXISTS `rm`.`user_session_rewards` (
  `reward_id` BIGINT(20) NOT NULL,
  `user_session_id` INT(11) NOT NULL,
  `reward_type_id` INT(11) NOT NULL,
  `value` VARCHAR(12) NOT NULL DEFAULT '0',
  `created` DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated` DATETIME NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`reward_id`, `user_session_id`, `reward_type_id`),
  UNIQUE INDEX `reward_unique` (`user_session_id` ASC, `reward_type_id` ASC),
  INDEX `fk_rewards_user_session1_idx` (`user_session_id` ASC),
  INDEX `fk_rewards_rewards_types1_idx` (`reward_type_id` ASC),
  CONSTRAINT `fk_rewards_rewards_types1`
    FOREIGN KEY (`reward_type_id`)
    REFERENCES `rm`.`rewards_types` (`type_id`)
    ON DELETE CASCADE
    ON UPDATE CASCADE,
  CONSTRAINT `fk_rewards_user_session1`
    FOREIGN KEY (`user_session_id`)
    REFERENCES `rm`.`user_sessions` (`user_session_id`)
    ON DELETE CASCADE
    ON UPDATE CASCADE)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `rm`.`user_session_summary`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `rm`.`user_session_summary` ;

CREATE TABLE IF NOT EXISTS `rm`.`user_session_summary` (
  `summary_id` BIGINT(20) NOT NULL AUTO_INCREMENT,
  `summary_type_id` INT(11) NOT NULL,
  `user_session_id` INT(11) NOT NULL,
  `value` VARCHAR(12) NOT NULL DEFAULT '0',
  `created` DATETIME NULL DEFAULT CURRENT_TIMESTAMP,
  `updated` DATETIME NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`summary_id`, `summary_type_id`, `user_session_id`),
  INDEX `fk_user_session_summary_user_session1_idx` (`user_session_id` ASC),
  INDEX `fk_user_session_summary_summary_types1_idx` (`summary_type_id` ASC),
  UNIQUE INDEX `unique_summary` (`summary_type_id` ASC, `user_session_id` ASC),
  CONSTRAINT `fk_user_session_summary_user_session1`
    FOREIGN KEY (`user_session_id`)
    REFERENCES `rm`.`user_sessions` (`user_session_id`)
    ON DELETE CASCADE
    ON UPDATE CASCADE,
  CONSTRAINT `fk_user_session_summary_summary_types1`
    FOREIGN KEY (`summary_type_id`)
    REFERENCES `rm`.`summary_types` (`type_id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `rm`.`user_variables`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `rm`.`user_variables` ;

CREATE TABLE IF NOT EXISTS `rm`.`user_variables` (
  `variable_id` INT(11) NOT NULL AUTO_INCREMENT,
  `user_id` INT(11) NOT NULL,
  `variable` VARCHAR(45) NOT NULL,
  `value` VARCHAR(45) NOT NULL,
  `created` DATETIME NULL DEFAULT CURRENT_TIMESTAMP,
  `updated` DATETIME NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`variable_id`, `user_id`),
  UNIQUE INDEX `variables_unique` (`user_id` ASC, `variable` ASC),
  INDEX `fk_user_variables_users1_idx` (`user_id` ASC),
  CONSTRAINT `fk_user_variables_users1`
    FOREIGN KEY (`user_id`)
    REFERENCES `rm`.`users` (`user_id`)
    ON DELETE CASCADE
    ON UPDATE CASCADE)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `rm`.`version`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `rm`.`version` ;

CREATE TABLE IF NOT EXISTS `rm`.`version` (
  `version` VARCHAR(20) NULL DEFAULT NULL,
  `updated` DATETIME NULL DEFAULT CURRENT_TIMESTAMP)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;


SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;
