-- MySQL dump 10.16  Distrib 10.1.23-MariaDB, for debian-linux-gnueabihf (armv7l)
-- mysqldump -u root -pdesarrolloM --no-data -B rm_api2 | sed 's/ AUTO_INCREMENT=[0-9]*\b//' > rm_api2.sql
-- Host: localhost    Database: rm_api2
-- ------------------------------------------------------
-- Server version	10.1.23-MariaDB-9+deb9u1

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Current Database: `rm_api2`
--

CREATE DATABASE /*!32312 IF NOT EXISTS*/ `rm_api2` /*!40100 DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci */;

USE `rm_api2`;

--
-- Table structure for table `automations`
--

DROP TABLE IF EXISTS `automations`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `automations` (
  `automation` varchar(45) DEFAULT NULL,
  `rationale` varchar(100) DEFAULT NULL,
  `executed` datetime DEFAULT CURRENT_TIMESTAMP,
  UNIQUE KEY `automation_UNIQUE` (`automation`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `center_info`
--

DROP TABLE IF EXISTS `center_info`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `center_info` (
  `info_id` tinyint(4) NOT NULL AUTO_INCREMENT,
  `name` varchar(20) DEFAULT NULL,
  `value` varchar(128) DEFAULT NULL,
  `created` datetime DEFAULT CURRENT_TIMESTAMP,
  `updated` datetime DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`info_id`),
  UNIQUE KEY `id_UNIQUE` (`info_id`),
  UNIQUE KEY `name_UNIQUE` (`name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `clusters`
--

DROP TABLE IF EXISTS `clusters`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `clusters` (
  `cluster_id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'id is the id of the record, it''s not used currently\\\\nBut clusted_id is the possition on the GUI grid if clusters',
  `group_id` int(11) NOT NULL,
  `cluster_id_gui` smallint(6) NOT NULL COMMENT 'id is the id of the record, it''s not used currently\\\\nBut clusted_id is the possition on the GUI grid if clusters',
  `user_id` int(11) DEFAULT NULL,
  `sensor_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`cluster_id`,`group_id`),
  UNIQUE KEY `unique` (`group_id`,`cluster_id_gui`),
  KEY `fk_clusters_user1_idx` (`user_id`),
  KEY `fk_clusters_sensor1_idx` (`sensor_id`),
  KEY `fk_clusters_groups1_idx` (`group_id`),
  CONSTRAINT `fk_clusters_groups1` FOREIGN KEY (`group_id`) REFERENCES `groups` (`group_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `fk_clusters_sensor1` FOREIGN KEY (`sensor_id`) REFERENCES `sensors` (`sensor_id`) ON DELETE SET NULL ON UPDATE CASCADE,
  CONSTRAINT `fk_clusters_user1` FOREIGN KEY (`user_id`) REFERENCES `users` (`user_id`) ON DELETE SET NULL ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `groups`
--

DROP TABLE IF EXISTS `groups`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `groups` (
  `group_id` int(11) NOT NULL AUTO_INCREMENT,
  `group_name` varchar(20) DEFAULT NULL,
  `created` datetime DEFAULT CURRENT_TIMESTAMP,
  `updated` datetime DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`group_id`),
  UNIQUE KEY `group_name_UNIQUE` (`group_name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `licence`
--

DROP TABLE IF EXISTS `licence`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `licence` (
  `status` varchar(20) DEFAULT NULL,
  `expiration` datetime DEFAULT NULL,
  `updated` datetime DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `records_types`
--

DROP TABLE IF EXISTS `records_types`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `records_types` (
  `type_id` int(11) NOT NULL AUTO_INCREMENT,
  `type_name` varchar(45) DEFAULT NULL,
  `created` datetime DEFAULT CURRENT_TIMESTAMP,
  `updated` datetime DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`type_id`),
  UNIQUE KEY `record_type_UNIQUE` (`type_name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `rewards_types`
--

DROP TABLE IF EXISTS `rewards_types`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `rewards_types` (
  `type_id` int(11) NOT NULL AUTO_INCREMENT,
  `type_name` varchar(45) NOT NULL,
  `created` datetime DEFAULT CURRENT_TIMESTAMP,
  `updated` datetime DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`type_id`),
  UNIQUE KEY `reward_type_UNIQUE` (`type_name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `sensors`
--

DROP TABLE IF EXISTS `sensors`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `sensors` (
  `sensor_id` int(11) NOT NULL AUTO_INCREMENT,
  `sensor_id_center` varchar(17) DEFAULT NULL,
  `sensor_uuid` char(36) DEFAULT NULL,
  `name` varchar(45) DEFAULT NULL,
  `model` varchar(17) DEFAULT NULL,
  `own_id` varchar(17) DEFAULT NULL,
  `mac` varchar(18) DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL,
  `owner_type` varchar(17) NOT NULL COMMENT 'Center or Owned or Borrowed',
  `active` tinyint(4) NOT NULL DEFAULT '1',
  `created` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`sensor_id`),
  UNIQUE KEY `unique` (`mac`,`model`,`owner_type`),
  KEY `fk_sensor_user1_idx` (`user_id`),
  CONSTRAINT `fk_sensor_user1` FOREIGN KEY (`user_id`) REFERENCES `users` (`user_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `sessions`
--

DROP TABLE IF EXISTS `sessions`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `sessions` (
  `session_id` int(11) NOT NULL AUTO_INCREMENT,
  `training_type_id` int(11) NOT NULL,
  `duration` int(11) DEFAULT NULL,
  `groupal_mode` tinyint(1) NOT NULL DEFAULT '1',
  `status` varchar(45) DEFAULT NULL,
  `created` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated` datetime DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`session_id`,`training_type_id`),
  UNIQUE KEY `unique` (`created`,`training_type_id`),
  KEY `fk_sessions_training_types1_idx` (`training_type_id`),
  CONSTRAINT `fk_sessions_training_types1` FOREIGN KEY (`training_type_id`) REFERENCES `training_types` (`type_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `settings`
--

DROP TABLE IF EXISTS `settings`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `settings` (
  `setting_id` tinyint(4) NOT NULL AUTO_INCREMENT,
  `setting` varchar(20) DEFAULT NULL,
  `value` varchar(36) DEFAULT NULL,
  `created` datetime DEFAULT CURRENT_TIMESTAMP,
  `updated` datetime DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`setting_id`),
  UNIQUE KEY `id_UNIQUE` (`setting_id`),
  UNIQUE KEY `setting_UNIQUE` (`setting`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `summary_types`
--

DROP TABLE IF EXISTS `summary_types`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `summary_types` (
  `type_id` int(11) NOT NULL,
  `type_uuid` char(36) DEFAULT NULL,
  `type_name` varchar(45) DEFAULT NULL,
  `type_group` varchar(45) DEFAULT NULL,
  `type_units` varchar(45) DEFAULT NULL,
  `created` datetime DEFAULT CURRENT_TIMESTAMP,
  `updated` datetime DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`type_id`),
  UNIQUE KEY `record_type_UNIQUE` (`type_name`),
  UNIQUE KEY `type_uuid_UNIQUE` (`type_uuid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `sync`
--

DROP TABLE IF EXISTS `sync`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `sync` (
  `sync_id` int(11) NOT NULL AUTO_INCREMENT,
  `component` varchar(45) DEFAULT NULL,
  `created` datetime DEFAULT CURRENT_TIMESTAMP,
  `updated` datetime DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`sync_id`),
  UNIQUE KEY `component_UNIQUE` (`component`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `training_types`
--

DROP TABLE IF EXISTS `training_types`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `training_types` (
  `type_id` int(11) NOT NULL AUTO_INCREMENT,
  `type_name` varchar(45) NOT NULL,
  `type_uuid` char(36) NOT NULL,
  `use_heart_rate` tinyint(1) DEFAULT NULL,
  `use_steps` tinyint(1) DEFAULT NULL,
  `step_option` varchar(45) DEFAULT NULL,
  `created` datetime DEFAULT CURRENT_TIMESTAMP,
  `updated` datetime DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`type_id`),
  UNIQUE KEY `type_name_UNIQUE` (`type_name`),
  UNIQUE KEY `type_uuid_UNIQUE` (`type_uuid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `user_session_records`
--

DROP TABLE IF EXISTS `user_session_records`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `user_session_records` (
  `record_id` bigint(20) NOT NULL AUTO_INCREMENT,
  `user_session_id` int(11) NOT NULL,
  `record_type_id` int(11) NOT NULL,
  `value` varchar(12) NOT NULL,
  `created` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`record_id`,`user_session_id`,`record_type_id`),
  UNIQUE KEY `UNIQUE` (`user_session_id`,`created`,`record_type_id`),
  KEY `fk_records_user_session1_idx` (`user_session_id`),
  KEY `fk_records_records_types2_idx` (`record_type_id`),
  CONSTRAINT `fk_records_records_types2` FOREIGN KEY (`record_type_id`) REFERENCES `records_types` (`type_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `fk_records_user_session1` FOREIGN KEY (`user_session_id`) REFERENCES `user_sessions` (`user_session_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `user_session_rewards`
--

DROP TABLE IF EXISTS `user_session_rewards`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `user_session_rewards` (
  `reward_id` bigint(20) NOT NULL AUTO_INCREMENT,
  `user_session_id` int(11) NOT NULL,
  `reward_type_id` int(11) NOT NULL,
  `value` varchar(12) NOT NULL DEFAULT '0',
  `created` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated` datetime DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`reward_id`,`user_session_id`,`reward_type_id`),
  UNIQUE KEY `reward_unique` (`user_session_id`,`reward_type_id`),
  KEY `fk_rewards_user_session1_idx` (`user_session_id`),
  KEY `fk_rewards_rewards_types1_idx` (`reward_type_id`),
  CONSTRAINT `fk_rewards_rewards_types1` FOREIGN KEY (`reward_type_id`) REFERENCES `rewards_types` (`type_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `fk_rewards_user_session1` FOREIGN KEY (`user_session_id`) REFERENCES `user_sessions` (`user_session_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `user_session_summary`
--

DROP TABLE IF EXISTS `user_session_summary`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `user_session_summary` (
  `summary_id` bigint(20) NOT NULL AUTO_INCREMENT,
  `summary_type_id` int(11) NOT NULL,
  `user_session_id` int(11) NOT NULL,
  `value` varchar(12) NOT NULL DEFAULT '0',
  `created` datetime DEFAULT CURRENT_TIMESTAMP,
  `updated` datetime DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`summary_id`,`summary_type_id`,`user_session_id`),
  UNIQUE KEY `unique_summary` (`summary_type_id`,`user_session_id`),
  KEY `fk_user_session_summary_user_session1_idx` (`user_session_id`),
  KEY `fk_user_session_summary_summary_types1_idx` (`summary_type_id`),
  CONSTRAINT `fk_user_session_summary_summary_types1` FOREIGN KEY (`summary_type_id`) REFERENCES `summary_types` (`type_id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_user_session_summary_user_session1` FOREIGN KEY (`user_session_id`) REFERENCES `user_sessions` (`user_session_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `user_sessions`
--

DROP TABLE IF EXISTS `user_sessions`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `user_sessions` (
  `user_session_id` int(11) NOT NULL AUTO_INCREMENT,
  `session_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `sensor_id` int(11) NOT NULL,
  `status` varchar(45) DEFAULT 'running',
  `email_sent` datetime DEFAULT NULL,
  `records_uploaded` datetime DEFAULT NULL,
  `rewards_uploaded` datetime DEFAULT NULL,
  `session_uuid` char(36) DEFAULT NULL,
  `created` datetime DEFAULT CURRENT_TIMESTAMP,
  `updated` datetime DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`user_session_id`,`session_id`,`user_id`,`sensor_id`),
  UNIQUE KEY `unique` (`session_id`,`user_id`),
  KEY `fk_user_session_sessions1_idx` (`session_id`),
  KEY `fk_user_session_users1_idx` (`user_id`),
  KEY `fk_user_session_sensors1_idx` (`sensor_id`),
  CONSTRAINT `fk_user_session_sensors1` FOREIGN KEY (`sensor_id`) REFERENCES `sensors` (`sensor_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `fk_user_session_sessions1` FOREIGN KEY (`session_id`) REFERENCES `sessions` (`session_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `fk_user_session_users1` FOREIGN KEY (`user_id`) REFERENCES `users` (`user_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `user_variables`
--

DROP TABLE IF EXISTS `user_variables`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `user_variables` (
  `variable_id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `variable` varchar(45) NOT NULL,
  `value` varchar(45) NOT NULL,
  `created` datetime DEFAULT CURRENT_TIMESTAMP,
  `updated` datetime DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`variable_id`,`user_id`),
  UNIQUE KEY `variables_unique` (`user_id`,`variable`),
  KEY `fk_user_variables_users1_idx` (`user_id`),
  CONSTRAINT `fk_user_variables_users1` FOREIGN KEY (`user_id`) REFERENCES `users` (`user_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `users`
--

DROP TABLE IF EXISTS `users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `users` (
  `user_id` int(11) NOT NULL AUTO_INCREMENT,
  `pseudonym` varchar(16) COLLATE utf8mb4_unicode_ci NOT NULL,
  `user_uuid` char(36) CHARACTER SET utf8 DEFAULT NULL,
  `name` varbinary(128) DEFAULT NULL,
  `last_name_1` varbinary(128) DEFAULT NULL,
  `last_name_2` varbinary(128) DEFAULT NULL,
  `email` varbinary(128) NOT NULL,
  `sex` char(1) CHARACTER SET utf8 NOT NULL,
  `birthdate` date DEFAULT NULL,
  `active` tinyint(4) NOT NULL DEFAULT '1',
  `created` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`user_id`),
  UNIQUE KEY `email_uniq` (`email`),
  UNIQUE KEY `user_uuid_uniq` (`user_uuid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `version`
--

DROP TABLE IF EXISTS `version`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `version` (
  `version` varchar(20) DEFAULT NULL,
  `updated` datetime DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2021-10-07 19:09:53
