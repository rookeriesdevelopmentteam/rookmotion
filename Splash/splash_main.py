import inspect
import os
import sys
from PyQt5.QtWidgets import QApplication, QMainWindow, QLabel, QProgressBar, QVBoxLayout, QWidget
from PyQt5.QtGui import QMovie, QPixmap, QFont
from PyQt5.QtCore import Qt, QTimer

import gi
gi.require_version('Wnck','3.0')
from gi.repository import Wnck

current_dir = os.path.dirname(os.path.abspath(inspect.getfile(inspect.currentframe())))
parent_dir = os.path.dirname(current_dir)
sys.path.insert(0, parent_dir)

from Language import _
from ElasticLogger import ElasticLogger


def minimize_windows():
    screen = Wnck.Screen.get_default()
    screen.force_update()
    windows = screen.get_windows()
    for window in windows:
        window.minimize()


def create_progressbar():
    progress_bar = QProgressBar()
    progress_bar.setRange(0,0)
    progress_bar.setFixedHeight(20)  # Height
    progress_bar.setFixedWidth(1000)
    style = "QProgressBar {border: 2px solid grey; border-radius: 8px; text-align: center;}"
    style += "QProgressBar::chunk {background-color: #"
    style += 'E42823' + "; border-radius: 8px;}"
    progress_bar.setStyleSheet(style)
    progress_bar.setFormat('')
    return progress_bar


class Splash(QMainWindow):
    def __init__(self):
        try:
            self.elastic_logger = ElasticLogger(self)
            # Init window
            super(Splash, self).__init__()
            self.setWindowFlags(Qt.CustomizeWindowHint | Qt.FramelessWindowHint)
            self.setStyleSheet('background-color: black;')
            self.showFullScreen()

            self.logo = QLabel()
            pixmap = QPixmap('/home/pi/RookMotion/images/splash_logo.png')
            self.logo.setPixmap(pixmap)

            self.progress_bar = create_progressbar()

            self.message_label = QLabel()
            self.message_label.setText(_('Initializing'))
            self.message_label.setAlignment(Qt.AlignCenter)
            self.message_label.setStyleSheet('color: white;')
            self.message_label.setFont(QFont('Arial', 40))

            main_layout = QVBoxLayout()
            main_layout.addWidget(self.logo)
            main_layout.addWidget(self.progress_bar)
            main_layout.addWidget(self.message_label)
            main_layout.setAlignment(Qt.AlignHCenter)
            widget_layout = QWidget()
            widget_layout.setLayout(main_layout)

            self.timer = QTimer()
            self.timer.timeout.connect(self.rookmotion_failed)
            self.timer.start(600000)

            self.setCentralWidget(widget_layout)
            self.show()

        except Exception as e:
            print("Exception: {}".format(e))

    def rookmotion_failed(self):
        self.elastic_logger.error("rookmotion_failed_start")
        raise Exception("RookMotion may have failed")


if __name__ == '__main__':
    minimize_windows()
    app = QApplication(sys.argv)
    splash = Splash()
    sys.exit(app.exec_())
