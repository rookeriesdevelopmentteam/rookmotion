from PyQt5 import QtGui

# Font messages
font_messages = QtGui.QFont()
font_messages.setPointSize(16)

# Define fonts sizes
font_session_time_spin = QtGui.QFont()
font_session_time_spin.setPointSize(18)
font_session_time_label = QtGui.QFont()
font_session_time_label.setPointSize(14)
font_session_progressbar = QtGui.QFont()
font_session_progressbar.setPointSize(30)

# Font dialog
font_dialogs = QtGui.QFont()
font_dialogs.setPointSize(17)

# Font unconnected sensor "Sin sensor"
font_disconnected_sensor_42 = QtGui.QFont()
font_disconnected_sensor_42.setPointSize(13)
font_disconnected_sensor_35 = QtGui.QFont()
font_disconnected_sensor_35.setPointSize(15)
font_disconnected_sensor_30 = QtGui.QFont()
font_disconnected_sensor_30.setPointSize(18)
font_disconnected_sensor_25 = QtGui.QFont()
font_disconnected_sensor_25.setPointSize(22)
font_disconnected_sensor_20 = QtGui.QFont()
font_disconnected_sensor_20.setPointSize(26)
font_disconnected_sensor_12 = QtGui.QFont()
font_disconnected_sensor_12.setPointSize(36)
font_disconnected_sensor_4 = QtGui.QFont()
font_disconnected_sensor_4.setPointSize(72)

# Font time label
font_label_hora = QtGui.QFont()
font_label_hora.setPointSize(56)  # 28 for 30
font_time = QtGui.QFont()
font_time.setPointSize(116)  # 58 for 30

# Font time session
font_session_timer = QtGui.QFont()
font_session_timer.setPointSize(30)

####################################################
# Cluster Selection frame
####################################################
# Font user cluster select
font_label_combobox = QtGui.QFont()
font_label_combobox.setPointSize(20)
font_sensor_select = QtGui.QFont()
font_sensor_select.setPointSize(15)
font_combobox = QtGui.QFont()
font_combobox.setPointSize(23)

# Font name combobox selection
font_user_name_selection_42 = QtGui.QFont()
font_user_name_selection_42.setPointSize(11)
font_user_name_selection_42.setBold(True)
font_user_name_selection_35 = QtGui.QFont()
font_user_name_selection_35.setPointSize(12)
font_user_name_selection_35.setBold(True)
font_user_name_selection_30 = QtGui.QFont()
font_user_name_selection_30.setPointSize(12)
font_user_name_selection_30.setBold(True)
font_user_name_selection_25 = QtGui.QFont()
font_user_name_selection_25.setPointSize(12)
font_user_name_selection_25.setBold(True)
font_user_name_selection_20 = QtGui.QFont()
font_user_name_selection_20.setPointSize(12)
font_user_name_selection_20.setBold(True)
font_user_name_selection_12 = QtGui.QFont()
font_user_name_selection_12.setPointSize(22)
font_user_name_selection_12.setBold(True)
font_user_name_selection_4 = QtGui.QFont()
font_user_name_selection_4.setPointSize(38)
font_user_name_selection_4.setBold(True)

# Font sensor combobox selection
font_sensor_selection_42 = QtGui.QFont()
font_sensor_selection_42.setPointSize(11)
font_sensor_selection_42.setBold(True)
font_sensor_selection_35 = QtGui.QFont()
font_sensor_selection_35.setPointSize(12)
font_sensor_selection_35.setBold(True)
font_sensor_selection_30 = QtGui.QFont()
font_sensor_selection_30.setPointSize(12)
font_sensor_selection_30.setBold(True)
font_sensor_selection_25 = QtGui.QFont()
font_sensor_selection_25.setPointSize(12)
font_sensor_selection_25.setBold(True)
font_sensor_selection_20 = QtGui.QFont()
font_sensor_selection_20.setPointSize(12)
font_sensor_selection_20.setBold(True)
font_sensor_selection_12 = QtGui.QFont()
font_sensor_selection_12.setPointSize(22)
font_sensor_selection_12.setBold(True)
font_sensor_selection_4 = QtGui.QFont()
font_sensor_selection_4.setPointSize(38)
font_sensor_selection_4.setBold(True)

####################################################
# Cluster Acquisition frame
####################################################
# Font HR
font_user_hr_42 = QtGui.QFont()
font_user_hr_42.setPointSize(16)
font_user_hr_35 = QtGui.QFont()
font_user_hr_35.setPointSize(18)
font_user_hr_30 = QtGui.QFont()
font_user_hr_30.setPointSize(18)
font_user_hr_25 = QtGui.QFont()
font_user_hr_25.setPointSize(22)
font_user_hr_20 = QtGui.QFont()
font_user_hr_20.setPointSize(26)
font_user_hr_12 = QtGui.QFont()
font_user_hr_12.setPointSize(36)
font_user_hr_4 = QtGui.QFont()
font_user_hr_4.setPointSize(55)

# Font sensor Model
font_sensor_model = QtGui.QFont()
font_sensor_model.setPointSize(12)


# Font Percentage
font_user_hr_percentage_42 = QtGui.QFont()
font_user_hr_percentage_42.setPointSize(35)
font_user_hr_percentage_35 = QtGui.QFont()
font_user_hr_percentage_35.setPointSize(38)
font_user_hr_percentage_30 = QtGui.QFont()
font_user_hr_percentage_30.setPointSize(35)
font_user_hr_percentage_25 = QtGui.QFont()
font_user_hr_percentage_25.setPointSize(45)
font_user_hr_percentage_20 = QtGui.QFont()
font_user_hr_percentage_20.setPointSize(55)
font_user_hr_percentage_12 = QtGui.QFont()
font_user_hr_percentage_12.setPointSize(80)
font_user_hr_percentage_4 = QtGui.QFont()
font_user_hr_percentage_4.setPointSize(135)

# Font calories
font_user_calories_42 = QtGui.QFont()
font_user_calories_42.setPointSize(16)
font_user_calories_35 = QtGui.QFont()
font_user_calories_35.setPointSize(18)
font_user_calories_30 = QtGui.QFont()
font_user_calories_30.setPointSize(18)
font_user_calories_25 = QtGui.QFont()
font_user_calories_25.setPointSize(22)
font_user_calories_20 = QtGui.QFont()
font_user_calories_20.setPointSize(26)
font_user_calories_12 = QtGui.QFont()
font_user_calories_12.setPointSize(36)
font_user_calories_4 = QtGui.QFont()
font_user_calories_4.setPointSize(55)

# Font name
font_user_name_42 = QtGui.QFont()
font_user_name_42.setPointSize(22)
font_user_name_35 = QtGui.QFont()
font_user_name_35.setPointSize(24)
font_user_name_30 = QtGui.QFont()
font_user_name_30.setPointSize(26)
font_user_name_25 = QtGui.QFont()
font_user_name_25.setPointSize(27)
font_user_name_20 = QtGui.QFont()
font_user_name_20.setPointSize(32)
font_user_name_12 = QtGui.QFont()
font_user_name_12.setPointSize(44)
font_user_name_4 = QtGui.QFont()
font_user_name_4.setPointSize(88)

# Font sensor number
font_sensor_number_42 = QtGui.QFont()
font_sensor_number_42.setPointSize(5)
font_sensor_number_42.setBold(True)
font_sensor_number_35 = QtGui.QFont()
font_sensor_number_35.setPointSize(5)
font_sensor_number_35.setBold(True)
font_sensor_number_30 = QtGui.QFont()
font_sensor_number_30.setPointSize(6)
font_sensor_number_30.setBold(True)
font_sensor_number_25 = QtGui.QFont()
font_sensor_number_25.setPointSize(8)
font_sensor_number_25.setBold(True)
font_sensor_number_20 = QtGui.QFont()
font_sensor_number_20.setPointSize(10)
font_sensor_number_20.setBold(True)
font_sensor_number_12 = QtGui.QFont()
font_sensor_number_12.setPointSize(12)
font_sensor_number_12.setBold(True)
font_sensor_number_4 = QtGui.QFont()
font_sensor_number_4.setPointSize(16)
font_sensor_number_4.setBold(True)


#############################
# Carousel
#############################
font_carousel_pseudonym = QtGui.QFont()
font_carousel_pseudonym.setPointSize(55)

font_carousel_hr = QtGui.QFont()
font_carousel_hr.setPointSize(45)

font_carousel_calories = font_carousel_hr
