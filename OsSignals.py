#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on 02/Dec/2020
@author: ein
This class handles the signaling between independent python scripts
"""

import os
import signal

# Line:  9234 pts/0    Sl+    0:38 python3 rookmotion_main.py
from Logger import Logger

rookmotion_cmd = "python3 rookmotion_main.py"
logger = Logger("OsSignals", level="info")


def get_rookmotion_pid():
    process_name = "rookmotion_main.py "
    return get_pid(process_name, rookmotion_cmd)


def notify_users_sensors_db_has_changed():
    try:
        pid = get_rookmotion_pid()
        if pid is None:
            logger.warning("rookmotion_main is not running")
            return
        os.kill(pid, signal.SIGUSR1)
        logger.debug("notified: users_sensors_db_has_changed")

    except Exception as e:
        logger.exception("notify_users_sensors_has_changed", e)


def get_splash_rookmotion_pid():
    process_name = "splash_main.py "
    splash_cmd = "python3 ./splash_main.py"
    return get_pid(process_name, splash_cmd)


def notify_rookmotion_ready():
    try:
        pid = get_splash_rookmotion_pid()
        if pid is None:
            logger.warning("splash_rookmotion_main is not running")
            return
        os.kill(pid, signal.SIGKILL)
        logger.debug("notified: RookMotion is ready")

    except Exception as e:
        logger.exception("notify_rookmotion_ready", e)


def get_pid(process_name, process_cmd):
    try:
        for line in os.popen("ps ax | grep " + process_name + " | grep -v grep"):
            try:
                fields = line.split()

                if len(fields) == 6:
                    cmd = fields[-2] + ' ' + fields[-1]

                    if cmd == process_cmd:
                        pid = fields[0]
                        if pid is None:
                            logger.warning(process_cmd + " PID is None")
                            return
                        pid = int(pid)
                        logger.debug(process_name + " pid: {}".format(pid))
                        return pid
            except Exception as e:
                logger.exception("get_"+process_name[:-3]+"_pid loop", e)
        logger.warning(process_name[:-3] + " is not running")
        return None
    except Exception as e:
        logger.exception("get_"+process_name[:-3]+"_pid loop", e)