#!/usr/bin/env python2
# -*- coding: utf-8 -*-
"""
Created on 21/01/2017

@author: ein
"""

import time
from PyQt5 import QtCore
from BluetoothLEQt.Scanner import Scanner
from Logger import Logger

logger = Logger(name='ThScan', level="debug")


class ThreadScanner:
    class ThreadQt(QtCore.QThread):
        scanner_results = QtCore.pyqtSignal(list)  # Sensors found, rssi
        sensor_discovered_signal = QtCore.pyqtSignal(dict)

        def __init__(self, hci, parent=None):
            QtCore.QThread.__init__(self, parent)

            self.scanning_is_enabled = False
            self.scanners_x_interface = []

            # Logger initialized
            self.logger = logger

            self.hci = hci
            if hci is None:
                self.logger.error("Hci is None")
                return

            self.scanner = Scanner(hci)

            self.flag_keep_running = True
            self.flag_scan_once = False

            self.visible_sensors = []
            self.status = "idle"

        def run(self):
            self.scan_routine()
            # self.logger.info('Thread scanner finished')
            # self.logger.removeHandler(self.log_handler)
            try:
                self.quit()
                # self.wait() RMSOFT-267
            except Exception as e:
                self.logger.exception("Quiting thread", e)

        def scan_routine(self):
            self.logger.info('Routine started')

            while self.flag_keep_running:
                try:
                    # self.logger.debug("Scanning with hci{}".format(self.hci))
                    if self.scanning_is_enabled or self.flag_scan_once:
                        self.flag_scan_once = False
                        self.scan_once_and_emit_discovered()
                except Exception as e:
                    self.logger.exception("scan_routine", e)
                time.sleep(0.25)  # for memory safety wait time
            # self.logger.debug("Thread Scanner terminated")

        def scan_once_and_emit_discovered(self):
            self.status = "scanning"
            try:
                sensors_discovered = self.scanner.scan_for_sensors(timeout=10)
                for sensor_discovered in sensors_discovered:
                    try:
                        # self.logger.debug(sensor_discovered)
                        self.sensor_discovered_signal.emit(sensor_discovered)
                    except Exception as e:
                        self.logger.exception("scan_routine for", e)
            except Exception as e:
                self.logger.exception("scan_once_and_emit_discovered", e)
            self.status = "idle"

        def get_status_(self):
            return self.status

        def classify_found_devices(self, found_devices):
            """
            Over checking is ok, because when performing an scan, different devices are found, smartwatches, tvs...
            When they're found, if any of them is protected or not connectible, indexing it's descriptor will throw an
            error, this causes the scan to exit and therefore a delay in the sensors waiting to be connected
            :param found_devices:
            """
            for dev in found_devices:
                try:
                    device_data = dev.getScanData()
                    # Finding one coospo
                    """[(9, 'Complete Local Name', 'H603B 0447272'), (3, 'Complete 16b Services', '0d18'),
                     (255, 'Manufacturer', '03ff014d0641'), (1, 'Flags', '05')]"""

                    model_found = False
                    if device_data is not None:
                        if len(device_data) > 2 and model_found is not True:  # The name for Polar is on index 1
                            if len(device_data[2]) == 3:
                                if device_data[2][1] == 'Complete Local Name':
                                    device_name = device_data[2][2]
                                    if len(device_name) >= 5:
                                        models = ['Polar H7']
                                        for i_model, model in enumerate(models):
                                            if device_name.startswith(models[i_model]):
                                                # self.logger.info(model_one[i_model])
                                                self.validate_if_exists_on_db(dev)
                                                model_found = True
                                                break
                        if len(device_data) >= 1 and model_found is not True:
                            if len(device_data[0]) == 3:  # Just to be sure that I can index the name
                                if device_data[0][1] == 'Complete Local Name':
                                    # print('Complete Local Name')
                                    device_name = device_data[0][2]
                                    if len(device_name) >= 5:
                                        # Keep COOSPO H6, Because this is the real advertising name
                                        models = ["COOSPO H6", 'H603B', 'H603B', 'HW702', 'RookHRc', 'RkHRam']
                                        for i_model, model in enumerate(models):
                                            if device_name.startswith(models[i_model]):
                                                # self.logger.info(model_one[i_model])
                                                self.validate_if_exists_on_db(dev)
                                                model_found = True
                                                break

                                    ''' Código paa polar y wahoo
                                    # print('0= ' + str(len(datos)) + '1= ' + str(len(datos[0])))
                                    if datos[0][2][:9] == 'COOSPO H6':
                                        self.valida_sensor(dev, 'COOSPO')
                                    elif datos[0][2][:5] == "TICKR":
                                        self.valida_sensor(dev, 'WAHOO')
                                    else:
                                        try:
                                            print(len(datos))
                                            self.logger.debug(str(datos))
                                            if len(datos) > 2:

                                                if datos[2][2][:8] == 'Polar H7':
                                                    self.valida_sensor(dev, 'POLAR')
                                        except:
                                            self.logger.warning('Excepción al extraer nombre > 2')
                                    '''

                except Exception as e:
                    if type(e).__name__ == "UnicodeDecodeError":
                        pass  # Normal when bad utf8 devices names are found
                    else:
                        template = "Exception while extracting name: {0} {1!r}"
                        message = template.format(type(e).__name__, e.args)
                        self.logger.error(message)

        def terminate_thread_(self):
            self.stop_scanning_()
            self.flag_keep_running = False

        def start_scanning_(self):
            self.scanning_is_enabled = True

        def stop_scanning_(self):
            self.scanning_is_enabled = False

    def __init__(self, hci):
        self.logger = logger
        try:
            self.hci = hci
            self.thread = self.ThreadQt(self.hci)
            self.thread.finished.connect(self.thread_finished_handler, QtCore.Qt.QueuedConnection)
            self.thread.start()
        except Exception as e:
            self.logger.exception("Starting thread", e)
            self.thread = None

    def terminate(self):
        if self.thread is not None:
            try:
                self.logger.info('Finishing Scann')
                self.thread.terminate_thread_()
            except Exception as e:
                self.logger.exception("terminating thread", e)

        else:
            self.logger.warning('Cannot terminate a terminated thread')

    def is_finished(self):
        if self.thread is not None:
            return self.thread.isFinished()
        return False

    def start_scanning(self):
        if self.thread is not None:
            self.thread.start_scanning_()

    def stop_scanning(self):
        if self.thread is not None:
            self.thread.stop_scanning_()

    def get_status(self):
        self.thread.get_status_()

    def thread_finished_handler(self):
        self.logger.info('Thread Scanner has finished')
        # self.logger.remove_handlers()
