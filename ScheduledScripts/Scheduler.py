#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on 07 Nov 2020
@author: ein
"""
import inspect
import os
import sys

from crontab import CronTab

current_dir = os.path.dirname(os.path.abspath(inspect.getfile(inspect.currentframe())))
parent_dir = os.path.dirname(current_dir)
sys.path.insert(0, parent_dir)

import FilePaths


class Scheduler:
    def __init__(self):
        self.cron = CronTab(user=True)

    # noinspection PyMethodMayBeStatic
    def schedule_automations(self):
        self.remove_automations()

        ##################################################
        # Sync WS with local DB
        ##################################################
        script_name = "SyncDbWs.py"
        script_path = FilePaths.Scheduled_path + script_name
        command = "sudo python3 " + script_path
        # run periodically
        job = self.cron.new(command=command, comment=script_name)
        job.minute.every(1)
        self.cron.write()

        # UPDATE: Do not sync at boot, mysql and network are down
        # run at reboot
        # job = self.cron.new(command=command, comment=script_name)
        # job.every_reboot()
        # self.cron.write()

        for job in self.cron:
            print(job)

    def remove_automations(self):
        self.cron.remove_all()


if __name__ == "__main__":
    scheduler = Scheduler()
    scheduler.schedule_automations()
