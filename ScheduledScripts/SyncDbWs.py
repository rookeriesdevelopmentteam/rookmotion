import inspect
import os
import sys

current_dir = os.path.dirname(os.path.abspath(inspect.getfile(inspect.currentframe())))
parent_dir = os.path.dirname(current_dir)
sys.path.insert(0, parent_dir)

from Logger import Logger
from WebService.WS import WS

logger = Logger(name='SyncWS', level="debug")
try:
    with WS() as web_service:
        web_service.sync()
except Exception as e:
    logger.exception("Syncing WS", e)
