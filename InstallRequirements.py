import os
import shutil
# import getch
import hashlib
import time

from DataBase.DB import DB
from DataBase.SQL import SQL
from WebService.WS import WS
from Encryption import Encryption
from FilePaths import create_folder_if_missing, logs_path, images_path, reports_path
# from BluetoothLE.HciTool import HciTool
import apt

from __version__ import __version__

base_path = "/home/pi/RookMotion"
settings_path = base_path + "/settings/"


def execute_all(db_):
    print("\nPausing scheduled automations")
    pause_scheduled_events()

    print("\nCreating required Folders")
    create_required_folders()

    print("\nChange_folder_properties")
    change_folder_properties()

    print("\nUpdating server settings")
    update_server_settings()

    print("\nCreate DB")
    create_db(db_)

    print("\nRunning Once Automations")
    once_automations()

    print("\nCreating settings files")
    create_settings_files()

    print("\nCreating center logo")
    create_center_logo()

    print("\nCreating Run script")
    create_run_script()

    print("\nCreating Desktop launcher")
    create_desktop_launcher()

    print("\nCreating auto start")
    create_auto_start()

    """print("\nCreating Database elements")
    update_local_db()"""

    print("\nInstalling system packages")
    install_apt_packages()

    print("\nInstalling python modules")
    install_modules()

    print("\nInstalling wheels")
    install_wheels()

    print("\nScheduling automations")
    schedule_automations()


def create_smtp_settings():
    pass


# noinspection PyListCreation
def create_email_settings():
    file_path = settings_path + 'email.py'
    if os.path.isfile(file_path) is False:    # True
        print("\nWriting Email settings file")
        text = []

        # SMTP
        text.append("SMTP_usr = None")
        text.append("SMTP_pwd = None")
        text.append("SMTP_srv = None")
        text.append("SMTP_port = None")
        text.append("")

        # Images
        text.append("report_background_url = \"https://rookmotion.com/DAQ/Personalized/Rook/report_background.jpg\"")
        text.append("logo_url = \"https://rookmotion.com/DAQ/Personalized/Rook/logo.png\"")
        text.append("welcome_background_url = \"https://rookmotion.com/DAQ/Personalized/test/welcome_background.png\"")
        text.append("")

        # Text
        f = open(file_path, "w")
        for line in text:
            f.write(line + '\r\n')
        f.close()


# noinspection PyListCreation
def create_server_settings(update=False, empty=False):
    file_path = settings_path + 'server.py'
    if os.path.isfile(file_path) is False or update is True:    # True
        print("\nWriting Server settings file")
        text = []
        encryption = Encryption()

        # User
        if empty is True:
            response = None
        else:
            # TODO: Implement getpass
            response = input("Input token level (no quotes): ")

        if response is not None and len(response) > 0:
            tmp = encryption.aes_encrypt(response, 'server')
            text.append("token_level = {}".format(tmp))
        else:
            text.append("token_level = None")
            print("None token_level")

        # Pwd
        if empty is True:
            response = None
        else:
            response = input("Input token authorization (no quotes): ")
        if response is not None and len(response) > 0:
            tmp = encryption.aes_encrypt(response, 'server')
            text.append("token_auth = {}".format(tmp))
        else:
            text.append("token_auth = None")
            print("None token_auth")

        text.append("")
        with open(file_path, "w") as f:
            for line in text:
                f.write(line + '\r\n')

        with WS() as web_service:
            web_service.system.get_center_info(store_center=True)
            web_service.users.sync_all()
            web_service.sensors.sync_all()
            web_service.sessions.sync_training_types()
            web_service.sessions.sync_summaries_types()


def update_server_settings():
    from settings import server
    file_path = settings_path + "server.py"

    if hasattr(server, "token_level"):
        print("Server settings already updated")
        return

    text = []
    encryption = Encryption()

    try:
        print("\nUpdating server settings file")
        old_token = encryption.aes_decrypt(server.server_user)
        old_token = (hashlib.sha256(old_token.encode('utf-8')).hexdigest())
        token = ("room_" + old_token)[:36]
        token = encryption.aes_encrypt(token, 'server')
        text.append("token_level = {}".format(token))
        text.append("token_auth = {}".format(b'619bb9d44050ccfb51da84c63b65facb80595a846e87e81213df1e408b8ca55821fd779b40ad9e41'))
        text.append("")
        f = open(file_path, "a")
        for line in text:
            f.write(line + "\r\n")
        f.close()
        # time.sleep(10)
    except Exception as e:
        print("Exception updating tokens", e)


# noinspection PyListCreation
def create_center_settings():
    file_path = settings_path + 'center.py'
    if os.path.isfile(file_path) is False:    # True
        print("\nWriting Center settings file")
        text = []

        text.append("name = None")
        text.append("id = None")
        text.append("training_type = 1")
        text.append("")

        f = open(file_path, "w")
        for line in text:
            f.write(line + '\r\n')
        f.close()

    # if file already exists, add new elements
    else:
        text = []
        try:
            import settings.center
            training_type = settings.center.training_type
        except:
            print("\nAdding training type to Center settings")
            text.append("training_type = 1")

        if len(text) > 0:
            text.append("")
            f = open(file_path, "a")
            for line in text:
                f.write(line + '\r\n')
            f.close()


# upd: Disabled because now we autodetect interfaces quantity on each boot
# noinspection PyListCreation,PyBroadException
# def create_bluetooth_settings(connection_mode=None):
#     file_path = settings_path + 'bluetooth.py'
#     text = []
#     write = False
#
#     try:
#         import settings.bluetooth
#         interfaces = settings.bluetooth.bt_interfaces
#         text.append("bt_interfaces = {}".format(interfaces))
#     except:
#         hci_tool = HciTool()
#         # interfaces = hci_tool.get_interfaces_for_connections_hcis()
#         interfaces = 4
#         text.append("bt_interfaces = {}".format(len(interfaces)))
#         write = True
#
#     if connection_mode is not None:
#         write = True
#         text.append("connection_mode = '{}'   # Normal/Fast".format(connection_mode))
#     else:
#         try:
#             import settings.bluetooth
#             connection_mode = settings.bluetooth.connection_mode
#             text.append("connection_mode = '{}'   # Normal/Fast".format(connection_mode))
#         except:
#             text.append("connection_mode = 'Normal'  # Normal/Fast")
#             write = True
#
#     if write:
#         print("\nWriting Bluetooth settings file")
#         text.append("")
#
#         f = open(file_path, "w")
#         for line in text:
#             f.write(line + '\r\n')
#         f.close()


# noinspection PyListCreation
def create_gui_settings(users_to_show, rewrite):
    file_path = settings_path + 'gui.py'

    # If file doesn't exists
    if os.path.isfile(file_path) is False:
        text = []
        text.append("default_users_to_show = {}  # options are: 4, 12, 20, 25, 30 , 35 and 42".format(users_to_show))
        text.append("")

        f = open(file_path, "w")
        for line in text:
            f.write(line + '\r\n')
        f.close()
    elif rewrite:
        text = []
        text.append("default_users_to_show = {}  # options are: 4, 12, 20, 25, 30 , 35 and 42".format(users_to_show))
        text.append("")

        f = open(file_path, "w")
        for line in text:
            f.write(line + '\r\n')
        f.close()


# noinspection PyListCreation
def create_display_mode_settings(display_mode, rewrite):
    file_path = settings_path + 'display_mode.py'

    if rewrite:
        text = []
        text.append("display_mode = '" + display_mode + "' # options are: rm or top_bar")
        text.append("")

        f = open(file_path, "w")
        for line in text:
            f.write(line + '\r\n')
        f.close()

    # If file doesn't exists
    elif os.path.isfile(file_path) is False:
        # print("\033[92m" + "Enable top bar logos? y/n")
        # response = getch.getch()
        response = "N"
        if response.upper() == "Y" or response.upper() == "S":
            # print("Yes" + "\033[39m")
            text = []
            text.append("display_mode = 'top_bar'  # options are: rm or top_bar")
            text.append("")

            f = open(file_path, "w")
            for line in text:
                f.write(line + '\r\n')
            f.close()

        else:
            # print("\033[31m" + "No" + "\033[39m")
            text = []
            text.append("display_mode = 'rm'  # options are: rm or top_bar")
            text.append("")

            f = open(file_path, "w")
            for line in text:
                f.write(line + '\r\n')
            f.close()

    else:
        #Check for kangoo word
        try:
            import settings.display_mode
            display_mode = settings.display_mode.display_mode
            if display_mode == "kangoo":
                text = []
                text.append("display_mode = '" + display_mode + "' # options are: rm or top_bar")
                text.append("")

                f = open(file_path, "w")
                for line in text:
                    f.write(line + '\r\n')
                f.close()
        except:
            print("Display mode file already exists")


# noinspection PyListCreation
def create_silent_mode_settings(silent_mode, rewrite):
    file_path = settings_path + 'discrete_mode.py'

    # If file doesn't exists
    if rewrite:
        text = []
        text.append("discrete_mode = {}  # options are: True or False".format(silent_mode))
        text.append("")

        f = open(file_path, "w")
        for line in text:
            f.write(line + '\r\n')
        f.close()

    elif os.path.isfile(file_path) is False:
        # print("\033[92m" + "¿Deseas activar el modo discreto? y/n")
        # response = getch.getch()
        response = "Y"
        if response.upper() == "Y":
            # print("Yes" + "\033[39m")
            text = []
            text.append("discrete_mode = True  # options are: True or False")
            text.append("")

            f = open(file_path, "w")
            for line in text:
                f.write(line + '\r\n')
            f.close()

        else:
            print("\033[31m" + "No" + "\033[39m")
            text = []
            text.append("discrete_mode = False  # options are: True or False")
            text.append("")

            f = open(file_path, "w")
            for line in text:
                f.write(line + '\r\n')
            f.close()

    else:
        print("Silent mode file already defined")


def create_units_settings(si_units, rewrite):
    try:
        with DB(['system'], caller='Req') as db_:
            si_units_ = db_.system.get_setting("si_units")
            if si_units_ is None or si_units_ not in ["1", "0"]:
                db_.system.set_setting('si_units', si_units)
                print("Units stored")
            return
    except Exception as e:
        print("Exception storing units: {}".format(e))
    file_path = settings_path + 'units.py'

    # If file doesn't exists
    if rewrite:
        text = []
        text.append("si_units = {}  # options are: True or False".format(si_units))
        text.append("")

        f = open(file_path, "w")
        for line in text:
            f.write(line + '\r\n')
        f.close()

    elif os.path.isfile(file_path) is False:
        # print("\033[92m" + "¿Deseas usar el Sistema Intenacional de medidas? y/n")
        # response = getch.getch()
        response = "Y"
        if response.upper() == "Y":
            # print("Yes" + "\033[39m")
            text = []
            text.append("si_units = True  # options are: True or False")
            text.append("")

            f = open(file_path, "w")
            for line in text:
                f.write(line + '\r\n')
            f.close()

        else:
            # print("\033[31m" + "No" + "\033[39m")
            text = []
            text.append("si_units = False  # options are: True or False")
            text.append("")

            f = open(file_path, "w")
            for line in text:
                f.write(line + '\r\n')
            f.close()

    else:
        print("Units file already defined")


def create_activity_mode_settings(activity_mode, rewrite):
    file_path = settings_path + 'activity_mode.py'

    # If file doesn't exists
    if os.path.isfile(file_path) is False:
        text = []
        text.append("activity_mode = 'group'  # options are: group or solo")
        text.append("")

        f = open(file_path, "w")
        for line in text:
            f.write(line + '\r\n')
        f.close()

    elif rewrite:
        text = []
        text.append("activity_mode = '{}'  # options are: group or solo".format(activity_mode))
        text.append("")

        f = open(file_path, "w")
        for line in text:
            f.write(line + '\r\n')
        f.close()


def create_center_logo():
    file_path = settings_path + 'logo_gim.png'

    # If file doesn't exists
    if os.path.isfile(file_path) is False:
        # print("\nWriting logo File")
        try:
            shutil.copy2(base_path + '/images/cluster_logo.png', file_path)
        except FileNotFoundError:
            print("Cluster logo not found")


def create_desktop_launcher():
    try:
        shutil.copy2(base_path + '/scripts/Desktop_Launcher', '/home/pi/Desktop/RookMotion')  # Rm is the launcher
    except FileNotFoundError:
        print("Desktop Launcher not found")


def create_run_script():
    try:
        shutil.copy2(base_path + '/scripts/sh_run_RookMotion.sh', '/home/pi/sh_run_RookMotion.sh')  # The Run sh
    except FileNotFoundError:
        print("Run sh not found")


def create_auto_start():
    try:
        # print("\nWriting AutoStart file")
        shutil.copy2(base_path + '/scripts/autostart', '/home/pi/.config/lxsession/LXDE-pi/autostart')
    except FileNotFoundError:
        shutil.copy2(base_path + '/scripts/autostart', '/etc/xdg/lxsession/LXDE-pi/autostart')


def create_settings_files():
    create_email_settings()
    create_center_settings()
    # create_bluetooth_settings()
    create_gui_settings(20, False)
    create_display_mode_settings("rm", False)
    create_silent_mode_settings(True, False)
    create_units_settings(True, False)
    create_activity_mode_settings("group", False)
    create_server_settings(False, empty=True)


def create_required_folders():
    create_folder_if_missing(logs_path)
    create_folder_if_missing(settings_path)
    create_folder_if_missing(images_path)
    create_folder_if_missing(reports_path)


def change_folder_properties():
    try:
        os.system('sudo chmod -R 777 /home/pi/RookMotion')
        os.system('sudo chmod +x /home/pi/RookMotion/Tools/bdaddr')
    except:
        print("Exception change_folder_properties")


def pause_scheduled_events():
    try:
        from ScheduledScripts.Scheduler import Scheduler
        scheduler = Scheduler()
        scheduler.remove_automations()
    except:
        pass


def schedule_automations():
    try:
        from ScheduledScripts.Scheduler import Scheduler
        scheduler = Scheduler()
        scheduler.schedule_automations()
    except:
        pass


def update_local_db():
    db_ = SQL(id_cluster="ir")
    db_.connect()
    # db_.add_new_elements()
    # db_.disconnect()


def install_apt_packages():
    db_ = DB(['system'])
    # db_.connect()
    automations_executed = db_.system.get_automation_executed()

    # noinspection PyListCreation
    packages = []
    packages.append('libatlas-base-dev')
    packages.append('libbluetooth-dev')
    packages.append('python3-scipy')
    packages.append('gettext')
    # To control apps windows
    packages.append('python3-gi')
    packages.append('gir1.2-wnck-3.0')
    # Gattlib dependencies
    packages.append("pkg-config")
    packages.append("libboost-python-dev")
    packages.append("libboost-thread-dev")
    packages.append("libglib2.0-dev")
    packages.append("python-dev")

    for package in packages:
        automation = 'install {}'.format(package)
        rationale = automation
        if automation not in automations_executed:
            cache = apt.cache.Cache()
            cache.update()
            cache.open()

            cache = apt.cache.Cache()
            pkg = cache[package]
            if pkg.is_installed:
                print("{} already installed".format(package))
            else:
                pkg.mark_install()
                try:
                    cache.commit()
                except:
                    print(sys.stderr, "Exception installing package {}".format(package))

            # Succeed?
            if pkg.is_installed:
                db_.system.add_automation_executed(automation, rationale)
    db_.disconnect()


def create_db(db_):
    try:
        automations_executed = db_.system.get_automation_executed()
        automation = 'create_db'
        if automation not in automations_executed:
            import DataBase.CreateDB as CreateDB
            if CreateDB.create():
                rationale = "Create rm DB"
                with DB(['system'], caller='Req') as db__:
                    db__.system.add_automation_executed(automation, rationale)
                print("")
                with WS() as web_service:
                    web_service.system.get_center_info(store_center=True)
                    web_service.users.sync_all()
                    web_service.sensors.sync_all()
                    # web_service.sessions.sync_all()
                    web_service.sessions.sync_training_types()
                    web_service.sessions.sync_summaries_types()

            else:
                print("create_db failed")
    except:
        print("Exception creating db")


def once_automations():
    try:
        db_ = DB(['system'], caller='Req')
        automations_executed = db_.system.get_automation_executed()
        automations = ["sql_disable_auto_adding_070122",
                       "sh_update_lxsession_200422"]
        for automation in automations:
            if automation not in automations_executed:
                try:
                    if "sync" in automation:
                        with WS() as web_service:
                            if "users" in automation:
                                web_service.users.sync_all()
                            elif "sensors" in automation:
                                web_service.sensors.sync_all()
                    elif "sql" in automation:
                        os.system('sudo mysql -u root -pdesarrolloM < {}/SQL/{}.sql '.format(base_path, automation))
                    elif "sh" in automation:
                        os.system('sudo sh {}/scripts/{}.sh'.format(base_path, automation))
                    db_.system.add_automation_executed(automation, automation)
                    print("Successfully executed {}".format(automation))
                except:
                    print("Exception running {} automation".format(automation))
    except:
        print("Exception running once_automations")


def install_modules():
    # noinspection PyBroadException
    try:
        db_ = DB(['system'])
        automations_executed = db_.system.get_automation_executed()
        with open("{}/requirements.txt".format(base_path)) as requirements:
            for line in requirements:
                try:
                    if line[0] != "#" and line[0] != "\n":
                        module, required_version = line.split("==")
                        module = module.strip()
                        required_version = (required_version.strip()).strip("\n")

                        automation = module+"=="+required_version
                        rationale = automation

                        if automation in automations_executed:
                            continue

                        try:
                            pip_line = os.popen("sudo pip3 show " + module + " | grep Version:")
                            current_version_ = pip_line.read().split()[1]
                            pip_line.close()
                        except:
                            current_version_ = None

                        if current_version_ == required_version:
                            print("{}=={} already installed".format(module, current_version_))
                        else:
                            print("Updating module {} to version {}".format(module, required_version))
                            os.system("sudo pip3 install {}=={}".format(module, required_version))
                        db_.system.add_automation_executed(automation, rationale)
                except Exception as e:
                    print("Exception installing module: {}".format(line), e)
        db_.disconnect()
    except Exception as e:
        print("Exception installing modules:", e)


def install_wheels():
    try:
        db_ = DB(["system"])
        automations_executed = db_.system.get_automation_executed()
        with open("{}/Wheels/install.txt".format(base_path)) as requirements:
            for line in requirements:
                try:
                    if line[0] != "#" and line[0] != "\n":
                        line_s = line.split("-")
                        module, required_version = line_s[0], line_s[1]
                        automation = module + "==" + required_version
                        rationale = automation

                    if automation in automations_executed:
                        continue

                    print("Updating module {} to version {}".format(module, required_version))
                    os.system("sudo pip3 install {}/Wheels/{}".format(base_path, line))
                    db_.system.add_automation_executed(automation, rationale)
                except Exception as e:
                    print("Exception installing wheel: {}".format(line), e)
    except Exception as e:
        print("Exception installing wheels:", e)


class InstallRequirements:

    def __init__(self):
        pass


if __name__ == "__main__":
    import sys

    with DB(['system', 'sensors'], caller='Req') as db:
        if len(sys.argv) == 2:
            execute_all(db)
        else:
            current_version = db.system.get_version()
            if __version__ != current_version:
                execute_all(db)
                db.system.set_version(__version__)

