#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on 2/Aug/2019
@author: DMA
"""

class SetWiFi:

    def __init__(self):
        pass


if __name__ == "__main__":

    file_path = "/etc/wpa_supplicant/wpa_supplicant.conf"
    text = []

    text.append("ctrl_interface=DIR=/var/run/wpa_supplicant GROUP=netdev")
    text.append("update_config=1")
    text.append("country=MX")
    text.append("")
    text.append("network={")
    text.append("	ssid=" + '"' + "RookDev" + '"')
    text.append("	psk=" + '"' + "developmentW" + '"')
    text.append("	key_mgmt=WPA-PSK")
    text.append("}")
    text.append("")
    text.append("")
    text.append("network={")
    text.append("	ssid=" + '"' + "DaHouse" + '"')
    text.append("	psk=" + '"' + "12358132134" + '"')
    text.append("	key_mgmt=WPA-PSK")
    text.append("}")
    try:
        f = open(file_path, "w")
        for line in text:
            f.write(line + '\r\n')
        f.close()

    except Exception as e:
        template = "Exception saving wifi settings: {0} {1!r}"
        message = template.format(type(e).__name__, e.args)
        print(message)
