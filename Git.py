#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on 08/Jan/2021
@author: ein
"""

from git import Repo
from Logger import Logger
from FilePaths import base_path
from __version__ import __version__

logger = Logger("OsSignals", level="info")


def get_current_branch() -> str:
    try:
        repository = Repo(base_path)
        return str(repository.active_branch).lower()
    except Exception as e:
        logger.exception("get_current_branch", e)


def checkout(branch):
    try:
        repository = Repo(base_path)
        repository.remote().fetch()
        if __version__ != "debug":
            repository.git.reset('--hard')
        repository.git.checkout(branch)
    except Exception as e:
        logger.exception("checkout", e)


def update_available():
    try:
        branch = get_current_branch()
        checkout(branch)
        repository = Repo(base_path)
        commits = repository.iter_commits("{}..origin/{}".format(branch, branch))
        commits_count = sum(1 for commit in commits)
        if commits_count > 0:
            return True
        return False
    except Exception as e:
        logger.exception("update_available", e)
