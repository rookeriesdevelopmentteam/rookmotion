from PyQt5 import QtWidgets, QtCore, QtGui
from PyQt5.QtCore import Qt
import ColorPalettes

class ExtendedQLabel(QtWidgets.QLabel):
    click_signal = QtCore.pyqtSignal(object)

    def __init__(self, parent=None):
        super(ExtendedQLabel, self).__init__(parent)
        self.id_label = None
        self.slot_handler = None

    def connect(self, slot_handler):
        """
        Modified Label to be click able
        :param slot_handler: Function to handle the click
        """
        self.click_signal.connect(slot_handler)
        self.slot_handler = slot_handler

    # noinspection PyMethodOverriding
    def disconnect(self, slot_handler):
        """
        Modified Label to be click able
        """
        self.click_signal.disconnect(slot_handler)

    def set_id(self, id_button):
        """
        :param id_button: Required only if indexation is required
        """
        self.id_label = id_button

    def mousePressEvent(self, event):
        self.click_signal.emit(self.id_label)
        super(ExtendedQLabel, self).mousePressEvent(event)


class ExtendedQPushButton(QtWidgets.QPushButton):
    click_signal = QtCore.pyqtSignal(object)

    def __init__(self, parent=None):
        super(ExtendedQPushButton, self).__init__(parent)
        self.id_button = None

        dark_palette = ColorPalettes.dark_palette()
        self.setPalette(dark_palette)

    def connect(self, slot_handler, id_button):
        """
        Modified Button to contain a button id.
        :param slot_handler: Function to handle the click
        :param id_button: identifier
        """
        self.click_signal.connect(slot_handler)
        self.id_button = id_button

    def human_accepted(self, event):
        # Note if this function is renamed, we need to rename all the strings listening to this caller
        self.setPalette(ColorPalettes.pushed_button_palette())
        self.click_signal.emit(self.id_button)

    def mousePressEvent(self, event):
        self.human_accepted(event)

    def enterEvent(self, event):  # This causer an event when the mouse is over the button
        # self.accepted(event)
        pass

    def keyPressEvent(self, event):
        self.human_accepted(event)

    def button_released(self, event):
        self.setPalette(ColorPalettes.dark_palette())

    def mouseReleaseEvent(self, event):
        self.button_released(event)

    def leaveEvent(self, event):
        self.button_released(event)

    def keyReleaseEvent(self, event):
        self.button_released(event)


# noinspection PyUnresolvedReferences
class ExtendedQCombobox(QtWidgets.QComboBox):
    def __init__(self, parent=None):
        super(ExtendedQCombobox, self).__init__(parent)
        # _A_REGEXP = "[aA\x00C0\x00C1\x00C4\x00E0\x00E1\x00E4]"
        # _E_REGEXP = "[eE\x00C8\x00C9\x00CB\x00E8\x00E9\x00EB]"
        # _I_REGEXP = "[iI\x00CC\x00CD\x00CF\x00EC\x00ED\x00EF]"
        # _O_REGEXP = "[oO\x00D2\x00D3\x00D6\x00F2\x00F3\x00F6]"
        # _U_REGEXP = "[uU\x00D9\x00DA\x00DC\x00F9\x00FA\x00FC]"
        #        #
        # filter.

        # dark palette
        dark_palette = ColorPalettes.dark_palette()
        self.setPalette(dark_palette)

        self.font_extended_combobox = QtGui.QFont()
        self.font_extended_combobox.setPointSize(22)
        self.setFont(self.font_extended_combobox)
        self.setFocusPolicy(Qt.StrongFocus)
        self.setEditable(True)

        self.pFilterModel = QtCore.QSortFilterProxyModel(self)
        self.pFilterModel.setFilterCaseSensitivity(Qt.CaseInsensitive)
        self.pFilterModel.setSortLocaleAware(True)

        self.pFilterModel.setSourceModel(self.model())

        self.completer = QtWidgets.QCompleter(self.pFilterModel, self)
        # self.setFont(self.font_extended_combobox)
        self.setInsertPolicy(QtWidgets.QComboBox.NoInsert)
        self.completer.setCompletionMode(QtWidgets.QCompleter.UnfilteredPopupCompletion)
        self.completer.popup().setPalette(dark_palette)
        self.popup = self.completer.popup()
        self.completer.popup().setFont(self.font_extended_combobox)

        self.setCompleter(self.completer)

        # self.completer.setPopup(self.completer.popup())

        # self.lineEdit().textEdited.connect(self.pFilterModel.setFilterFixedString)
        self.lineEdit().textEdited.connect(self.pFilterModel.setFilterFixedString)
        self.completer.activated.connect(self.on_completer_activated)

    def update_popup_font_size(self, font_size):
        self.completer.popup().setFont(font_size)

    def on_completer_activated(self, text):
        if text:
            # TODO: Fill Flags
            index = self.findText(text)
            self.setCurrentIndex(index)
            # str is correct, it's not  mistake its a magic PyQt
            self.activated[str].emit(self.itemText(index))

    def setModel(self, model):
        super(ExtendedQCombobox, self).setModel(model)
        self.pFilterModel.setSourceModel(model)
        self.completer.setModel(self.pFilterModel)

    def setModelColumn(self, column):
        self.completer.setCompletionColumn(column)
        self.pFilterModel.setFilterKeyColumn(column)
        super(ExtendedQCombobox, self).setModelColumn(column)

    def currentIndexChanged(self, *__args):
        print(self.currentData())


