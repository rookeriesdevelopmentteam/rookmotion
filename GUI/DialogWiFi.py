# -*- coding: utf-8 -*-
"""
Created on Tue Aug 25 17:38:23 2019

@author: DMA
"""
from PyQt5 import QtWidgets, QtGui, QtCore

import os
import time
import ColorPalettes
import FontsGUI
from Language import _
from Logger import Logger
from ExtendedWidgets import ExtendedQCombobox


class DialogWiFi (QtWidgets.QDialog):
    def __init__(self, parent=None):
        QtWidgets.QDialog.__init__(self, parent)

        # Logger
        self.logger = Logger(name="Dialog WiFi", level="info")

        # GUI
        self.font_sensor_select = QtGui.QFont()
        self.font_sensor_select.setPointSize(15)

        self.setFont(FontsGUI.font_dialogs)
        self.setFixedSize(650, 230)

        # dark theme
        dark_palette = ColorPalettes.dialog_palette()
        self.autoFillBackground()
        self.setPalette(dark_palette)
        # remove window title
        self.setWindowFlags(QtCore.Qt.SplashScreen)

        # editUserListLayout
        window_layout = QtWidgets.QVBoxLayout(self)

        self.body_text_label = QtWidgets.QLabel(_("Fill your WiFi settings"))
        self.wifi_name_label = QtWidgets.QLabel(_("Network name (SSID)") + ":")
        self.wifi_name = ExtendedQCombobox()
        self.wifi_name.setFont(self.font_sensor_select)
        self.wifi_name.addItems(self.get_wifi_list())
        self.password_label = QtWidgets.QLabel(_("Password") + ":")
        self.password = QtWidgets.QLineEdit(None)
        self.password.setMaxLength(100)
        self.password.textChanged.connect(self.enable_save_button)

        self.save_and_close = ExtendedQPushButton(_("Save & connect"))
        self.save_and_close.clicked.connect(self.accept)
        self.disable_save_and_close_button()

        self.button_close = ExtendedQPushButton(_("Close"))
        self.button_close.clicked.connect(self.reject)

        wifi_name_h_layout = QtWidgets.QHBoxLayout()
        wifi_name_h_layout.addWidget(self.wifi_name_label)
        wifi_name_h_layout.addWidget(self.wifi_name)
        wifi_name_h_frame = QtWidgets.QFrame()
        wifi_name_h_frame.setLayout(wifi_name_h_layout)

        wifi_password_h_layout = QtWidgets.QHBoxLayout()
        wifi_password_h_layout.addWidget(self.password_label)
        wifi_password_h_layout.addWidget(self.password)
        wifi_password_h_frame = QtWidgets.QFrame()
        wifi_password_h_frame.setLayout(wifi_password_h_layout)

        configure_wifi_v_layout = QtWidgets.QVBoxLayout()
        configure_wifi_v_layout.addWidget(self.body_text_label)
        configure_wifi_v_layout.addWidget(wifi_name_h_frame)
        configure_wifi_v_layout.addWidget(wifi_password_h_frame)

        select_wifi_h_frame = QtWidgets.QFrame()
        select_wifi_h_frame.setLayout(configure_wifi_v_layout)

        buttons_h_layout = QtWidgets.QHBoxLayout()
        buttons_frame = QtWidgets.QFrame()
        buttons_h_layout.addWidget(self.button_close)
        buttons_h_layout.addWidget(self.save_and_close)

        buttons_frame.setLayout(buttons_h_layout)
        window_layout.addWidget(select_wifi_h_frame)
        window_layout.addWidget(buttons_frame)

    def enable_save_button(self):
        if len(self.password.text()) > 7:
            enabled_palette = QtGui.QPalette()
            enabled_palette.setColor(QtGui.QPalette.ButtonText, QtGui.QColor(255, 255, 255))
            self.save_and_close.setPalette(enabled_palette)
            self.save_and_close.setEnabled(True)
            self.save_and_close.setDefault(True)
            self.save_and_close.setAutoDefault(True)
            return True
        self.disable_save_and_close_button()
        # self.save_and_close.setFocus()
        return False

    def disable_save_and_close_button(self):
        disabled_palette = QtGui.QPalette()
        disabled_palette.setColor(QtGui.QPalette.ButtonText, QtGui.QColor(53, 53, 53))
        self.save_and_close.setDisabled(True)
        self.save_and_close.setPalette(disabled_palette)

    def format_dialog(self, title, font):
        self.setWindowTitle(title)

    def get_wifi_name(self):
        return self.wifi_name.currentText()

    def get_wifi_password(self):
        return self.password.text()

    def get_wifi_list(self):
        wifi_list = ['']
        try:
            retries = 0
            while retries < 3:
                for line in os.popen("iwlist wlan0 s | grep SSID"):
                    ssid = line.lstrip()[7:-2]
                    if len(ssid) > 0:
                        if ssid[0] != '\\':
                            wifi_list.append(ssid)
                if len(wifi_list) > 1:
                    break
                retries += 1
                time.sleep(0.5)
        except Exception as e:
            self.logger.exception("get_wifi_list", e)
        return wifi_list


class ExtendedQPushButton(QtWidgets.QPushButton):
    click_signal = QtCore.pyqtSignal(object)

    def __init__(self, parent=None):
        super(ExtendedQPushButton, self).__init__(parent)
        self.id_button = None

    def connect(self, slot_handler, id_button):
        """
        Modified Button to contain a button id.
        :param slot_handler: Function to handle the click
        :param id_button: identifier
        """
        self.click_signal.connect(slot_handler)
        self.id_button = id_button

    def mousePressEvent(self, event):
        self.click_signal.emit(self.id_button)
        super(ExtendedQPushButton, self).mousePressEvent(event)
