# -*- coding: utf-8 -*-
"""
Created on Tue Feb 14 09:47:23 2017

@author: Develupertz
"""
from PyQt5 import QtGui, QtWidgets, QtCore

import ColorPalettes
import FontsGUI
from DataBase.DB import DB
from DataBase.SQL import SQL
from Language import _


class DialogNewGroup (QtWidgets.QDialog):
    def __init__(self, parent=None):
        QtWidgets.QDialog.__init__(self, parent)

        # db
        self.db = DB(['users', 'sensors', 'groups'])
        # self.db.connect()

        # Format
        # self.setFixedSize(900, 200)
        self.setFont(FontsGUI.font_dialogs)

        # dark theme
        dark_palette = ColorPalettes.dialog_palette()

        self.autoFillBackground()
        self.setPalette(dark_palette)
        # remove window title
        self.setWindowFlags(QtCore.Qt.SplashScreen)

        self.groups_names = self.db.groups.get_groups_names()
        self.warning_label = QtWidgets.QLabel("")

        self.name = QtWidgets.QLineEdit(None)
        self.name.setMaxLength(35)
        self.name.textChanged.connect(self.enable_button_ok)

        header_label = QtWidgets.QLabel(_("Enter group name"))
        name_label = QtWidgets.QLabel(_("Name") + ": ")

        # Buttons
        self.buttonOk = QtWidgets.QPushButton(_("Add"), self)
        self.buttonOk.clicked.connect(self.accept)
        self.buttonOk.setDisabled(True)
        self.buttonCancel = QtWidgets.QPushButton(_("Cancel"), self)
        self.buttonCancel.clicked.connect(self.reject)

        # Layout
        new_group_layout = QtWidgets.QGridLayout(self)

        new_group_layout.addWidget(header_label, 0, 0, 1, 4)

        new_group_layout.addWidget(name_label, 1, 0, 1, 1)
        new_group_layout.addWidget(self.name, 1, 1, 1, 3)

        new_group_layout.addWidget(self.warning_label, 2, 2, 1, 4)

        new_group_layout.addWidget(self.buttonCancel, 3, 0, 1, 2)
        new_group_layout.addWidget(self.buttonOk, 3, 2, 1, 2)
        
        self.name.setFocus()
        # ok Button enable
        self.enable_button_ok()

    def enable_button_ok(self):
        if self.test_name() and len(self.name.text()) != 0:
            self.buttonOk.setEnabled(True)
            enabled_palette = QtGui.QPalette()
            enabled_palette.setColor(QtGui.QPalette.ButtonText, QtGui.QColor(255, 255, 255))
            self.buttonOk.setPalette(enabled_palette)
        else:
            self.disable_button_ok()

    def disable_button_ok(self):
        disabled_palette = QtGui.QPalette()
        disabled_palette.setColor(QtGui.QPalette.ButtonText, QtGui.QColor(53, 53, 53))
        self.buttonOk.setDisabled(True)
        self.buttonOk.setPalette(disabled_palette)

    def test_name(self):
        name_to_save = self.get_group_name().title()
        if name_to_save == _("Last"):
            self.warning_label.setText(_("This name is reserved"))
            self.buttonOk.setText(_("Modify"))
            return False
        elif name_to_save in self.groups_names:
            self.warning_label.setText(_("This group name already exists? \nDo you want to overwrite it?"))
            self.buttonOk.setText(_("Overwrite"))
            return True
        else:
            self.warning_label.setText("")
            self.buttonOk.setText(_("Add"))
            return True

    def format_dialog(self, title, font):
        self.setWindowTitle(title)

    def get_group_name(self):
        name = self.name.text()
        name = name.title()
        return name
