#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on 12/Jul/2020
@author: ein
"""
from PyQt5 import QtGui, QtCore, QtWidgets

import ColorPalettes
import TrainingType
from ExtendedWidgets import ExtendedQLabel
from FilePaths import images_path
from Logger import Logger
from Language import _


def set_image_on_label(image_path: str, label: QtWidgets.QLabel):
    image = QtGui.QIcon(images_path + image_path)
    pixmap = image.pixmap(73, 73, image.Active, image.On)
    label.setPixmap(pixmap)


class CarouselFrame(QtWidgets.QFrame):
    def __init__(self, parent=None):
        super(CarouselFrame, self).__init__(parent)
        self.logger = Logger(name='CarouselFrame', level='debug')

        # noinspection PyArgumentList,PyArgumentList
        try:

            self.pseudonym_label = QtWidgets.QLabel()
            # self.empty_trophy_image = QtGui.QImage(images_path + 'null_trophy_image.png')

            self.calories_trophy_icon = QtWidgets.QLabel()
            self.hr_trophy_icon = QtWidgets.QLabel()
            self.hr_avg_label = QtWidgets.QLabel("0 {}".format(_('bpm')))
            self.calories_label = QtWidgets.QLabel("0 {}".format(_('kcal')))
            self.steps_label = QtWidgets.QLabel("  {}".format(TrainingType.get_steps_units()))
            self.zones_chart = ExtendedQLabel('icon')
            self.effort_chart = ExtendedQLabel('icon')
            self.steps_chart = ExtendedQLabel('icon')

            top_bar_layout = QtWidgets.QHBoxLayout()
            top_bar_frame = QtWidgets.QFrame()
            top_bar_frame.setLayout(top_bar_layout)

            # Font Percentage
            self.pseudonym_font = QtGui.QFont()
            self.pseudonym_font.setPointSize(49)

            # Top Bar items
            self.pseudonym_label.setPalette(ColorPalettes.palette_white_text)
            self.pseudonym_label.setFont(self.pseudonym_font)
            self.pseudonym_label.setAlignment(QtCore.Qt.AlignLeft)
            self.pseudonym_label.setMinimumWidth(700)
            self.pseudonym_label.setMaximumHeight(120)

            # Calories trophy
            self.calories_trophy_icon.setMaximumWidth(120)
            self.calories_trophy_icon.setMaximumHeight(120)

            set_image_on_label('summary_trophy_calories.png', self.calories_trophy_icon)
            self.calories_trophy_icon.setAlignment(QtCore.Qt.AlignRight)
            self.calories_trophy_icon.hide()

            set_image_on_label('summary_trophy_heart.png', self.hr_trophy_icon)
            self.hr_trophy_icon.setAlignment(QtCore.Qt.AlignRight)
            self.hr_trophy_icon.hide()

            # Space label
            empty_space_label = QtWidgets.QLabel("      ")
            empty_space_label.setText("      ")
            empty_space_label.setMinimumWidth(100)
            empty_space_label.setMaximumHeight(72)

            heart_icon = QtWidgets.QLabel()
            set_image_on_label('summary_hr.png', heart_icon)
            heart_icon.setAlignment(QtCore.Qt.AlignRight)

            # hr_avg_label
            # Font Percentage
            self.labels_font = QtGui.QFont()
            self.labels_font.setPointSize(40)
            self.hr_avg_label.setMinimumWidth(300)
            self.hr_avg_label.setPalette(ColorPalettes.palette_white_text)
            self.hr_avg_label.setFont(self.labels_font)
            self.hr_avg_label.setAlignment(QtCore.Qt.AlignLeft)
            self.hr_avg_label.setMaximumHeight(80)

            hr_layout = QtWidgets.QHBoxLayout()
            hr_frame = QtWidgets.QFrame()
            hr_frame.setLayout(hr_layout)
            hr_layout.addWidget(heart_icon)
            hr_layout.addWidget(self.hr_avg_label)

            # CaloriesImage
            calories_icon = QtWidgets.QLabel()
            set_image_on_label('summary_calories.png', calories_icon)
            calories_icon.setAlignment(QtCore.Qt.AlignRight)

            # Calories label
            self.calories_label.setPalette(ColorPalettes.palette_white_text)
            self.calories_label.setFont(self.labels_font)
            self.calories_label.setAlignment(QtCore.Qt.AlignLeft)
            self.calories_label.setMaximumHeight(80)

            calories_layout = QtWidgets.QHBoxLayout()
            calories_frame = QtWidgets.QFrame()
            calories_frame.setLayout(calories_layout)
            calories_layout.addWidget(calories_icon)
            calories_layout.addWidget(self.calories_label)

            # steps Image
            self.steps_icon = QtWidgets.QLabel()
            self.steps_icon.setAlignment(QtCore.Qt.AlignRight)
            self.steps_icon.setMaximumWidth(73)
            self.steps_icon.setMaximumHeight(73)
            self.steps_icon.hide()

            # steps label
            self.steps_label.setPalette(ColorPalettes.palette_white_text)
            self.steps_label.setFont(self.labels_font)
            self.steps_label.setAlignment(QtCore.Qt.AlignLeft)
            self.steps_label.setMaximumHeight(80)

            steps_layout = QtWidgets.QHBoxLayout()
            steps_frame = QtWidgets.QFrame()
            steps_frame.setLayout(steps_layout)
            steps_layout.addWidget(self.steps_icon)
            steps_layout.addWidget(self.steps_label)

            top_bar_layout.addWidget(self.pseudonym_label)
            top_bar_layout.addWidget(self.calories_trophy_icon)
            top_bar_layout.addWidget(self.hr_trophy_icon)
            top_bar_layout.addWidget(QtWidgets.QLabel("      "))
            top_bar_layout.addWidget(steps_frame)
            top_bar_layout.addWidget(QtWidgets.QLabel("      "))
            top_bar_layout.addWidget(hr_frame)
            top_bar_layout.addWidget(QtWidgets.QLabel("      "))
            top_bar_layout.addWidget(calories_frame)

            image = QtGui.QIcon(images_path + 'graph_empty_zones.png')
            pixmap = image.pixmap(1100, 490, image.Active, image.On)
            self.zones_chart.setPixmap(pixmap)
            self.zones_chart.setAlignment(QtCore.Qt.AlignCenter)

            image = QtGui.QIcon(images_path + 'graph_time_dummy.png')
            pixmap = image.pixmap(1100, 490, image.Active, image.On)
            self.effort_chart.setPixmap(pixmap)
            self.effort_chart.setAlignment(QtCore.Qt.AlignCenter)

            self.layout_charts = QtWidgets.QGridLayout()
            # noinspection PyArgumentList
            self.layout_charts.addWidget(self.effort_chart, 0, 0, 1, 1, QtCore.Qt.AlignCenter)
            # noinspection PyArgumentList
            self.layout_charts.addWidget(self.zones_chart, 1, 0, 1, 1, QtCore.Qt.AlignCenter)
            frame_charts = QtWidgets.QFrame()
            frame_charts.setLayout(self.layout_charts)

            layout = QtWidgets.QVBoxLayout()
            layout.addWidget(top_bar_frame)
            layout.addWidget(frame_charts)

            # Add Items to Layout
            self.setLayout(layout)
        except Exception as e:
            self.logger.exception("init", e)

    def add_charts_second_column(self):
        # noinspection PyArgumentList
        self.layout_charts.addWidget(self.steps_chart, 0, 1, 1, 1, QtCore.Qt.AlignCenter)

    def set_clicked_handler(self, handler):
        try:
            self.effort_chart.connect(handler)
            self.zones_chart.connect(handler)
            self.steps_chart.connect(handler)
        except Exception as e:
            self.logger.exception("set_clicked_handler", e)
