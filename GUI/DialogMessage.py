from PyQt5 import QtWidgets, QtCore

import ColorPalettes
import FontsGUI


class DialogMessage(QtWidgets.QMessageBox):
    def __init__(self, level='normal', parent=None):
        super(DialogMessage, self).__init__(parent)

        if level == 'warning':
            palette = ColorPalettes.warning_palette()
        else:
            palette = ColorPalettes.dialog_palette()

        self.autoFillBackground()
        self.setFont(FontsGUI.font_messages)
        self.setPalette(palette)

        self.setWindowFlags(QtCore.Qt.SplashScreen)
        self.buttons = []

        self.time_to_wait = None
        self.timer = None
        self.dynamic_message = None

    def set_icon(self, icon_type):
        return
        # current icons are ugly
        #if icon_type == 'question':
        #    self.setIcon(QtWidgets.QMessageBox.Question)

    def set_title(self, title):
        return
        # self.message.insert(0, "<b>{}</b> \n".format(title),0)
        # self.setWindowTitle(title)

    def set_text(self, text):
        lines = text.splitlines()
        formatted_text = "<p align='center'>"
        for line in lines:
            formatted_text += line + "<br>"

        formatted_text += "</p>"
        formatted_text += "\t"
        self.dynamic_message = formatted_text
        self.setText(formatted_text)
        # self.setFixedWidth(self.h_size)

    def set_informative_text(self, text):
        self.setInformativeText(text + "\t")
        # return

    def set_font(self, font):
        # TODO: Store fonts in a separate file
        self.setFont(font)

    def add_button(self, text, role=None):
        if role == 'accept':
            q_role = QtWidgets.QMessageBox.AcceptRole
            button = self.addButton(text, q_role)
            self.setDefaultButton(button)
        elif role == 'yes':
            q_role = QtWidgets.QMessageBox.YesRole
            button = self.addButton(text, q_role)
        elif role == 'cancel':
            q_role = QtWidgets.QMessageBox.NoRole
            button = self.addButton(text, q_role)
            self.setEscapeButton(button)
        else:
            q_role = QtWidgets.QMessageBox.NoRole
            button = self.addButton(text, q_role)

        self.buttons.append([button, text])

        # if self.h_size is not None and self.h_size > 0:
        #     self.setFixedWidth(self.h_size)

    def execute_message(self):
        self.exec_()
        button_clicked = self.clickedButton()

        for button in self.buttons:
            if button[0] == button_clicked:
                return button[1]  # Return the text
        return None

    def show_message(self):
        self.show()

    def remove_buttons(self):
        self.setStandardButtons(QtWidgets.QMessageBox.NoButton)

    def set_timer(self, seconds):
        self.remove_buttons()
        self.time_to_wait = seconds
        self.timer = QtCore.QTimer(self)
        self.timer.setInterval(999)
        self.timer.timeout.connect(self.change_content)
        self.change_content()
        self.time_to_wait += 1
        self.timer.start()

    def change_content(self):
        msg_tmp = str(self.dynamic_message).format(self.time_to_wait)
        self.setText(msg_tmp)
        # # self.setFixedWidth(self.h_size)
        self.time_to_wait -= 1
        if self.time_to_wait == -2:
            self.close()

    def closeEvent(self, event):
        try:
            self.timer.stop()
        except:
            pass
        event.accept()
