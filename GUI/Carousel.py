#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on 12/Jul/2020
@author: ein

"""
from PyQt5 import QtCore
import ClustersTools
import TrainingType
from GUI.CarouselFrame import CarouselFrame
from Logger import Logger


class Carousel(QtCore.QThread):
    def __init__(self, users_to_show):
        super().__init__()
        self.logger = Logger(name='Carousel', level='debug')

        try:
            self.next_cluster_id_to_show = 0

            # The 0 frame will bhe all the clusters grid
            self.summaries = [{} for _ in range(users_to_show)]
            # self.summaries.append({'cluster_id': 'all_clusters'})
            # self.trophies = [{} for x in range(users_to_show)]

            self.summaries_have_content = False

            # Timer carousel
            self.timer_carousel = QtCore.QTimer()

            self.frame = CarouselFrame()
        except Exception as e:
            self.logger.exception("carousel init", e)

    def reset(self):
        try:
            self.next_cluster_id_to_show = 0
            self.summaries = [{} for _ in range(len(self.summaries))]
            self.summaries_have_content = False
        except Exception as e:
            self.logger.exception("carousel reset", e)

    def get_frame(self):
        return self.frame

    def set_show_carousel_handler(self, handler):
        self.timer_carousel.timeout.connect(handler)

    def start_timer(self):
        try:
            self.timer_carousel.start(5000)
        except Exception as e:
            self.logger.exception("start_timer", e)

    def stop_timer(self):
        try:
            self.timer_carousel.stop()
        except Exception as e:
            self.logger.exception("stop_timer", e)

    def resize_lists(self, new_users_to_show):
        try:
            current_lists_size = len(self.summaries)
            required_lists_size = new_users_to_show

            if current_lists_size > required_lists_size:
                # Shrink
                self.summaries = self.summaries[:new_users_to_show]
            elif current_lists_size < required_lists_size:
                # Extend
                difference = required_lists_size - current_lists_size
                list_extension = [{} for _ in range(difference)]
                self.summaries.extend(list_extension)
            else:
                # Lists sizes are the same
                return
            return True
        except Exception as e:
            self.logger.exception("resize_lists", e)

    def set_trophy_calories(self, winners, clusters):
        """
        We need clusters for ClustersTools.find_cluster_from_user
        """
        for sex in winners.keys():
            winner = winners[sex]
            self.set_trophy(winner, 'trophy_calories', clusters)

    def set_trophy(self, winner, trophy_name, clusters):
        """
        We need clusters for ClustersTools.find_cluster_from_user
        """
        if winner is None:
            self.logger.warning("Winner {} is None".format(trophy_name))
            return

        user_id = winner.get('user_id', None)
        if user_id is None:
            self.logger.warning("Value is None for {}".format(trophy_name))

        cluster_id = ClustersTools.find_cluster_from_user(clusters, user_id)
        if cluster_id is None:
            self.logger.warning("cluster_id is None for {}".format(trophy_name))

        self.summaries[cluster_id][trophy_name] = True

    def add_summary(self, cluster, charts_file_paths, session_records):
        try:
            cluster_id = cluster.cluster['id']
            # self.logger.debug("add_summary cluster: {} ".format(cluster_id))
            # Set each key instead saving complete dict to prevent deleting trophies
            self.summaries[cluster_id]['cluster_id'] = cluster_id
            self.summaries[cluster_id]['effort_chart_path'] = charts_file_paths.get('effort', '')
            self.summaries[cluster_id]['zones_chart_path'] = charts_file_paths.get('zones', '')
            self.summaries[cluster_id]['steps_chart_path'] = charts_file_paths.get('steps', '')
            self.summaries[cluster_id]['calories'] = session_records.get_calories()
            self.summaries[cluster_id]['steps'] = session_records.get_steps()
            self.summaries[cluster_id]['hr_avg'] = session_records.get_hr('heart_rate_avg')
            self.summaries[cluster_id]['pseudonym'] = cluster.user['pseudonym']

            self.summaries_have_content = True
            return True
        except Exception as e:
            self.logger.exception('Adding carousel summary finished', e)

    def get_next_cluster_id(self):
        try:
            next_ = self.next_cluster_id_to_show
            self.next_cluster_id_to_show += 1
            if self.next_cluster_id_to_show > len(self.summaries):
                self.next_cluster_id_to_show = 0
                return 'show_grid'
            return next_
        except Exception as e:
            self.logger.exception('Getting next cluster_id', e)

    def get_next_summary(self):
        try:
            if self.summaries_have_content:
                while True:  # TODO: Be aware of infinite loops
                    try:
                        cluster_to_show = self.get_next_cluster_id()
                        if cluster_to_show == 'show_grid':
                            return {'cluster_id': 'show_grid'}

                        cluster_id = self.summaries[cluster_to_show].get('cluster_id', None)
                        if cluster_id is not None:
                            return self.summaries[cluster_to_show]
                    except Exception as e:
                        self.logger.exception('Finding next summary', e)
        except Exception as e:
            self.logger.exception('Getting next summary', e)
        return None

    def get_summary(self, cluster_id):
        try:
            if self.summaries_have_content:
                cluster_has_summary = self.summaries[cluster_id].get('cluster_id', False)
                if cluster_has_summary:
                    return self.summaries[cluster_id]
        except Exception as e:
            self.logger.exception('Getting summary of cluster_id: {}'.format(cluster_id), e)
        return None

    def show_steps_fields(self):
        try:
            # Carousel
            self.frame.steps_label.show()

            steps_image = TrainingType.get_steps_icon()
            steps_pixmap = steps_image.pixmap(73, 73, steps_image.Active, steps_image.On)

            self.frame.steps_icon.setPixmap(steps_pixmap)
            self.frame.steps_icon.show()

            self.frame.add_charts_second_column()

        except Exception as e:
            self.logger.exception("show_acquisition_cluster_steps_field", e)

    def hide_steps_fields(self):
        try:
            self.frame.steps_icon.hide()
            self.frame.steps_label.hide()
        except Exception as e:
            self.logger.exception("hide_acquisition_cluster_steps_field", e)
