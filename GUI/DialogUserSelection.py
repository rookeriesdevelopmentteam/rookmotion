# -*- coding: utf-8 -*-
"""
Created on 24/Jun/2018
@author: Jesquivel
"""
import operator

from PyQt5 import QtGui, QtWidgets, QtCore

import ColorPalettes
import FontsGUI
from DataBase.DB import DB
from DataBase.SQL import SQL
from Language import _
from Logger import Logger


class MyListWidget(QtWidgets.QListWidget):
    def clicked(self, item=None):
        if not item:
            item = self.mplfigs.currentItem()


class DialogUserSelection (QtWidgets.QDialog):
    def __init__(self, parent=None):
        QtWidgets.QDialog.__init__(self, parent)

        self.setWindowTitle(_("Select user"))
        self.resize(700, 500)

        self.setFont(FontsGUI.font_dialogs)
        # dark theme

        self.autoFillBackground()
        dark_palette = ColorPalettes.dialog_palette()
        self.setPalette(dark_palette)
        # remove window title
        self.setWindowFlags(QtCore.Qt.SplashScreen)

        self.selection_type = None

        # Logger
        self.logger = Logger(name="UserSelection", level="info")

        # db
        self.db = DB(['users'], caller='UsrSel')
        # self.db.connect()

        # To be returned when selected
        self.user_selected_id = None

        # Table to list
        # self.user_table = self.db.get_user_table()

        users = self.db.users.get_users_full_name()
        if users is not None:
            users.sort(key=operator.itemgetter(1))  # Sorted by name, the id will be in disorder
        self.users_formatted = []
        if users is not None:
            max_user = max(m[0] for m in users)
            digits = len(str(abs(max_user)))
            for user_raw in users:
                self.users_formatted.append(str(user_raw[0]).rjust(digits, "0") + ' - ' + str(user_raw[1]))

        # List
        self.users_list = MyListWidget()
        self.users_list.addItems(self.users_formatted)
        self.users_list.itemSelectionChanged.connect(self.enable_select_button)
        self.users_list.itemDoubleClicked.connect(self.button_select_clicked)  # Double click to select

        # Buttons
        self.button_select_user = None  # is defined in the type selection
        self.button_cancel = None  # is defined in the type selection
        self.button_delete = None  # Only required if we're modifying an user # is defined in the type selection
        self.button_delete = None  # Only required if we're modifying an user

        # Dialog
        self.dialog_layout = QtWidgets.QVBoxLayout()
        self.dialog_frame = QtWidgets.QFrame()

        self.dialog_layout.addWidget(self.users_list)

        self.dialog_stacked_layout = QtWidgets.QStackedLayout(self)

    def set_dialog(self, font, selection_type=None):

        self.selection_type = selection_type

        # Selection Button
        if self.selection_type == "edit_user":
            self.button_select_user = QtWidgets.QPushButton(_("Modify"), self)
        else:
            self.button_select_user = QtWidgets.QPushButton(_("Select"), self)

        self.button_select_user.clicked.connect(self.button_select_clicked)
        self.disable_button_select_user()
        self.button_select_user.setAutoDefault(True)

        # Cancel Button
        self.button_cancel = QtWidgets.QPushButton(_("Cancel"), self)
        self.button_cancel.setAutoDefault(False)
        self.button_cancel.clicked.connect(self.reject)

        buttons_layout = QtWidgets.QHBoxLayout()
        buttons_frame = QtWidgets.QFrame()
        buttons_layout.addWidget(self.button_cancel)
        buttons_layout.addWidget(self.button_select_user)
        buttons_frame.setLayout(buttons_layout)

        # Delete Button
        if self.selection_type == "edit_user":
            self.button_delete = QtWidgets.QPushButton(_("Delete user"), self)
            self.button_delete.clicked.connect(self.delete_user)
            self.disable_button_delete_user()
            self.button_delete.setAutoDefault(False)
            self.dialog_layout.addWidget(self.button_delete)

        self.dialog_layout.addWidget(buttons_frame)
        self.dialog_frame.setLayout(self.dialog_layout)
        self.users_list.setFocus()
        if self.users_list.count() is not 0:
            self.users_list.setCurrentRow(0)

        self.dialog_stacked_layout.addWidget(self.dialog_frame)

    def button_select_clicked(self):
        user_selected = self.users_list.currentItem().text()  # Text
        self.user_selected_id = int(user_selected.split(" - ")[0])
        self.accept()

    def enable_select_button(self):
        if self.selection_type == "edit_user":
            if not self.button_delete.isEnabled():
                self.enable_button_delete_user()
                self.button_cancel.setFocus()

        if not self.button_select_user.isEnabled():
            self.enable_button_select_user()
            self.button_select_user.setAutoDefault(True)

    def enable_button_delete_user(self):
        if self.button_delete is not None:
            self.button_delete.setEnabled(True)
            enabled_palette = QtGui.QPalette()
            enabled_palette.setColor(QtGui.QPalette.ButtonText, QtGui.QColor(255, 255, 255))
            self.button_delete.setPalette(enabled_palette)

    def enable_button_select_user(self):
        if self.button_select_user is not None:
            self.button_select_user.setEnabled(True)
            enabled_palette = QtGui.QPalette()
            enabled_palette.setColor(QtGui.QPalette.ButtonText, QtGui.QColor(255, 255, 255))
            self.button_select_user.setPalette(enabled_palette)

    def disable_button_delete_user(self):
        if self.button_delete is not None:
            self.button_delete.setDisabled(True)
            disabled_palette = QtGui.QPalette()
            disabled_palette.setColor(QtGui.QPalette.ButtonText, QtGui.QColor(53, 53, 53))
            self.button_delete.setPalette(disabled_palette)

    def disable_button_select_user(self):
        if self.button_select_user is not None:
            self.button_select_user.setDisabled(True)
            disabled_palette = QtGui.QPalette()
            disabled_palette.setColor(QtGui.QPalette.ButtonText, QtGui.QColor(53, 53, 53))
            self.button_select_user.setPalette(disabled_palette)

    def delete_user(self):
        user_selected = self.users_list.currentItem().text()  # Text
        self.user_selected_id = int(user_selected.split(" - ")[0])
        user_selected_name = user_selected.split(" - ")[1]

        # TODO: use messageClass
        # Message
        msg_confirm_delete = QtWidgets.QMessageBox()
        msg_confirm_delete.setText(_("Deleting an user will also erase his local sessions. This is permanent"))
        msg_confirm_delete.setInformativeText(_("Confirm deletion of user") + ": " + str(user_selected_name) + "?")

        msg_confirm_delete.autoFillBackground()
        msg_confirm_delete.setPalette(ColorPalettes.dialog_palette())

        msg_confirm_delete.setFont(FontsGUI.font_dialogs)
        msg_confirm_delete.setWindowFlags(QtCore.Qt.SplashScreen)
        # TODO: Change text to Delete
        msg_confirm_delete.setStandardButtons(QtWidgets.QMessageBox.Cancel | QtWidgets.QMessageBox.Ok)

        answer = msg_confirm_delete.exec_()

        if answer == QtWidgets.QMessageBox.Cancel:
            pass
        else:
            # Delete from db
            try:
                self.db.users.delete_user(self.user_selected_id)
                # Update List
                self.users_formatted.remove(user_selected)
                self.logger.info("Deleted user: {}".format(user_selected))
            except Exception as e:
                self.logger.exception("deleting user", e)

            # Update List
            self.users_list.clear()
            self.users_list.addItems(self.users_formatted)

            if self.button_select_user.isEnabled():
                self.disable_button_select_user()

            if self.selection_type == "edit_user":
                if self.button_delete.isEnabled():
                    self.disable_button_delete_user()

    def enable_button_delete(self):
        pass
