# -*- coding: utf-8 -*-
"""
Created on Tue Feb 14 09:47:23 2017

@author: Develupertz
"""
from PyQt5 import QtWidgets, QtGui, QtCore

import ColorPalettes
import FontsGUI
from DataBase.SQL import SQL
from Language import _
from Logger import Logger


class DialogSensorInfo (QtWidgets.QDialog):
    def __init__(self, parent=None):
        QtWidgets.QDialog.__init__(self, parent)

        # Logger
        self.logger = Logger(name="Dialog Sensor", level="info")

        self.selected_sensor = None
        self.button_close = None

        # Format
        self.setFixedSize(650, 370)
        self.setFont(FontsGUI.font_dialogs)
        # dark theme
        dark_palette = ColorPalettes.dialog_palette()

        self.autoFillBackground()
        self.setPalette(dark_palette)
        # remove window title
        self.setWindowFlags(QtCore.Qt.SplashScreen)

        # GUI
        self.font_sensor_select = QtGui.QFont()
        self.font_sensor_select.setPointSize(15)

        # db
        self.db = SQL()
        # self.db.connect()

        sensors = self.db.sensors.get_sensors_ids()

        sensors.sort()
        sensors_formatted = [""]
        for sensor in sensors:
            sensors_formatted.append(str(sensor))

        self.combobox_select_sensor_cluster = None
        self.combobox_select_sensor_cluster = ExtendedQCombobox()
        self.combobox_select_sensor_cluster.setFont(self.font_sensor_select)
        self.combobox_select_sensor_cluster.clear()
        self.combobox_select_sensor_cluster.addItems(sensors_formatted)

        # editUserListLayout
        window_layout = QtWidgets.QVBoxLayout(self)

        self.body_text_label = QtWidgets.QLabel(_("Select the sensor"))
        self.sensor_characteristics_label = QtWidgets.QLabel("")
        self.space = QtWidgets.QLabel("")
        self.space.setMaximumWidth(50)

        self.button_test = ExtendedQPushButton(_("Test sensor"))
        self.button_test.clicked.connect(self.accept)
        self.disable_test_button()

        self.button_close = ExtendedQPushButton(_("Close"))
        self.button_close.clicked.connect(self.reject)

        self.combobox_select_sensor_cluster.currentIndexChanged.connect(self.update_info_and_enable_buttons)

        select_sensor_h_layout = QtWidgets.QHBoxLayout()
        select_sensor_h_frame = QtWidgets.QFrame()
        select_sensor_h_layout.addWidget(self.body_text_label)
        select_sensor_h_layout.addWidget(self.space)
        select_sensor_h_layout.addWidget(self.combobox_select_sensor_cluster)

        select_sensor_h_frame.setLayout(select_sensor_h_layout)

        buttons_h_layout = QtWidgets.QHBoxLayout()
        buttons_frame = QtWidgets.QFrame()
        buttons_h_layout.addWidget(self.button_close)
        buttons_h_layout.addWidget(self.button_test)

        buttons_frame.setLayout(buttons_h_layout)
        self.button_test.setPalette(ColorPalettes.dialog_palette())

        window_layout.addWidget(select_sensor_h_frame)
        window_layout.addWidget(self.sensor_characteristics_label)
        window_layout.addWidget(buttons_frame)

    def update_info_and_enable_buttons(self):
        self.selected_sensor = self.get_selected_sensor()
        if self.selected_sensor is not None:
            self.button_close.setDisabled(False)
            # enable connect button when the module is developed

            sensor_last_usage = self.db.get_sensor_last_usage(self.selected_sensor)
            if sensor_last_usage is None:
                text = _("Sensor never used")
            else:
                user_id = sensor_last_usage[self.db.sql_records['i_user']]
                time_stamp = sensor_last_usage[self.db.sql_records['timestamp']]
                session = sensor_last_usage[self.db.sql_records['session']]

                user_name = self.db.users.get_user_info(user_id, 'name', decrypt=True)
                if user_name is None:
                    user_name = _("No info")

                user_email = self.db.users.get_user_info(user_id, 'email', decrypt=True)
                if user_email is None:
                    user_email = _("No info")

                text = _("Sensor {0}:\n\n"
                         "Last session used: {1}\n"
                         "Time of last connection: {2}\n"
                         "Last user: {3}\n"
                         "User email: {4}".format(self.selected_sensor, session, time_stamp, user_name, user_email))

            self.sensor_characteristics_label.setText(str(text))

        else:
            self.disable_test_button()
            self.button_close.setFocus()

    def disable_test_button(self):
        self.button_test.setDisabled(True)

    def format_dialog(self, title, font):
        self.setWindowTitle(title)

    def get_selected_sensor(self):
        if self.combobox_select_sensor_cluster.currentText() is not None:
            return str(self.combobox_select_sensor_cluster.currentText())
        else:
            return None


class ExtendedQPushButton(QtWidgets.QPushButton):
    click_signal = QtCore.pyqtSignal(object)

    def __init__(self, parent=None):
        super(ExtendedQPushButton, self).__init__(parent)
        self.id_button = None

    def connect(self, slot_handler, id_button):
        """
        Modified Button to contain a button id.
        :param slot_handler: Function to handle the click
        :param id_button: identifier
        """
        self.click_signal.connect(slot_handler)
        self.id_button = id_button

    def mousePressEvent(self, event):
        self.click_signal.emit(self.id_button)
        super(ExtendedQPushButton, self).mousePressEvent(event)


# TODO: Re use this combo
class ExtendedQCombobox(QtWidgets.QComboBox):
    def __init__(self, parent=None):
        super(ExtendedQCombobox, self).__init__(parent)

        # _A_REGEXP = "[aA\x00C0\x00C1\x00C4\x00E0\x00E1\x00E4]"
        # _E_REGEXP = "[eE\x00C8\x00C9\x00CB\x00E8\x00E9\x00EB]"
        # _I_REGEXP = "[iI\x00CC\x00CD\x00CF\x00EC\x00ED\x00EF]"
        # _O_REGEXP = "[oO\x00D2\x00D3\x00D6\x00F2\x00F3\x00F6]"
        # _U_REGEXP = "[uU\x00D9\x00DA\x00DC\x00F9\x00FA\x00FC]"
        #
        # self.filter = self.filt
        #
        # filter.

        # self.font_extended_combobox = QtGui.QFont()
        # self.font_extended_combobox.setPointSize(23)
        self.setFocusPolicy(QtCore.Qt.StrongFocus)
        self.setEditable(True)

        self.pFilterModel = QtCore.QSortFilterProxyModel(self)
        self.pFilterModel.setFilterCaseSensitivity(QtCore.Qt.CaseInsensitive)
        self.pFilterModel.setSortLocaleAware(True)
        # self.pFilterModel.
        self.pFilterModel.setSourceModel(self.model())

        self.completer = QtWidgets.QCompleter(self.pFilterModel, self)
        # self.setFont(self.font_extended_combobox)
        self.setInsertPolicy(QtWidgets.QComboBox.NoInsert)
        self.completer.setCompletionMode(QtWidgets.QCompleter.UnfilteredPopupCompletion)
        # self.completer.popup().setFont(self.font_extended_combobox)
        self.setCompleter(self.completer)

        # self.completer.setPopup(self.completer.popup())

        # self.lineEdit().textEdited.connect(self.pFilterModel.setFilterFixedString)
        self.lineEdit().textEdited.connect(self.pFilterModel.setFilterFixedString)
        self.completer.activated.connect(self.on_completer_activated)

        # self.lineEdit().hasFocus().connect(self.showPopup)

    def show_popup(self):
        self.showPopup()

    def on_completer_activated(self, text):
        if text:
            index = self.findText(text)
            self.setCurrentIndex(index)
            self.activated[str].emit(self.itemText(index))

    def setModel(self, model):
        super(ExtendedQCombobox, self).setModel(model)
        self.pFilterModel.setSourceModel(model)
        self.completer.setModel(self.pFilterModel)

    def setModelColumn(self, column):
        self.completer.setCompletionColumn(column)
        self.pFilterModel.setFilterKeyColumn(column)
        super(ExtendedQCombobox, self).setModelColumn(column)