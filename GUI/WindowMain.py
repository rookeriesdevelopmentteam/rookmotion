"""
Created on Wed Dec 28 00:18:32 2016

@author: root
"""

import datetime
import inspect
import os
import signal
import subprocess
import time
from threading import Thread
from typing import List, Dict

from PyQt5 import QtCore, QtGui, QtWidgets
from PyQt5.QtWidgets import QShortcut

import ClustersTools
import ColorPalettes
import ComboBoxesTools
import FontsGUI
import Git
import InstallRequirements
import OsSignals
import Utilities
import constants
from Cluster import Cluster
from DataBase.DB import DB
from ExtendedWidgets import ExtendedQCombobox, ExtendedQLabel, ExtendedQPushButton
from GUI.DialogConnections import DialogConnections
from GUI.DialogGroupSelection import DialogGroupSelection
from GUI.DialogMessage import DialogMessage
from GUI.DialogNewGroup import DialogNewGroup
from GUI.DialogSensorInfo import DialogSensorInfo
from GUI.DialogUser import DialogUser
from GUI.DialogUserSelection import DialogUserSelection
from GUI.DialogWiFi import DialogWiFi
from GUI.Carousel import Carousel
from FilePaths import images_path
import TrainingType
from GUI.TrainingTypeComboBox import TrainingTypeComboBox

from Settings import Settings

from Language import _, available_languages
from ElasticLogger import ElasticLogger
from Logger import Logger
from Network import Network
from Rewards import Rewards
from SessionRecords import SessionRecords

from Acquisition.ThreadAcquisition import ThreadAcquisition
from ThreadConnections import ThreadConnections
from ThreadPlot import ThreadPlot
from ThreadSchedule import ThreadSchedule
from WebService.WS import WS

from __version__ import __version__

caller_format_id = "{} id: {} c: {}"
caller_format = "F: {} c: {}"

# Size: Index
logos_cluster_indexes = {4: None, 12: 3, 20: 4, 25: 4, 30: 5, 35: 6, 42: 6}


# noinspection PyBroadException
def delete_dialog(dialog):  # I'm getting some linux Qt errors, so I'm better try/catching this instruction
    try:
        dialog.deleteLater()  # Cannot be deleted until another execution occurs
    except Exception as e:
        template = "Exception deleting dialog, SI: {0} {1!r}"
        message = template.format(type(e).__name__, e.args)
        print(message)


# noinspection PyBroadException
def show_dialog(message, timeout=0, accept_button_text=None, cancel_button_text=None):
    message_box = DialogMessage()
    message_box.set_text(message)
    message_box.set_timer(timeout)
    if accept_button_text is not None:
        message_box.add_button(accept_button_text, 'accept')
    if cancel_button_text is not None:
        message_box.add_button(cancel_button_text, 'cancel')
    return message_box.execute_message()


# noinspection PyBroadException
def show_critical_dialog(message, timeout=30, accept_button_text=_('Restart'), cancel_button_text=_('Ignore')):
    message_box = DialogMessage("warning")
    message_box.set_text(message)
    # message_box.set_timer(timeout)
    message_box.set_informative_text(_("Do you want to restart to fix the problem?"))
    message_box.set_icon("question")
    if accept_button_text is not None:
        message_box.add_button(accept_button_text, 'accept')
    if cancel_button_text is not None:
        message_box.add_button(cancel_button_text, 'cancel')
    return message_box.execute_message()


class MainWindow(QtWidgets.QMainWindow):
    def __init__(self):
        try:
            # GUI
            super(MainWindow, self).__init__()  # TODO: Check this
            # self.setWindowFlags(QtCore.Qt.CustomizeWindowHint | QtCore.Qt.WindowTitleHint) # With Title bar
            self.setWindowFlags(QtCore.Qt.CustomizeWindowHint)

            # Flags
            self.flag_debug_mode = False
            self.session_started_flag = False
            self.session_finished_flag = False
            # self.generate_graphs_flag = False
            self.timer_update_ip_on_config_flag = False
            self.show_no_internet_dialog_flag = True

            # Logger
            self.elastic_logger = ElasticLogger(self)
            self.logger = Logger(name="GUI", level="debug")
            self.logger.info("")
            self.logger.info(" ======= RookMotion (v%s) =======" % __version__)
            self.logger.info("")
            self.logger.info('Starting GUI')

            self.cluster_is_being_added = False
            # Network
            self.network = Network()

            # db
            self.db = DB(['users', 'groups', 'sessions', 'sensors', 'system'])
            self.db.sessions.delete_uploaded_sessions_thread()
            self.rewards = Rewards()

            # WebService
            # self.web_service = WS()
            if self.network.test_internet():
                with WS() as web_service:
                    web_service.system.post_daq_ip(self.network.get_ip())
                    web_service.system.get_center_info()

            # Center
            self.id_center = constants.id_center

            # Settings
            self.settings = Settings()
            self.activity_mode = self.settings.get_activity_mode()
            self.units = self.settings.get_si_units()
            self.display_mode = self.settings.get_display_mode()
            self.discrete_mode = self.settings.get_discrete_mode()
            self.users_to_show = self.settings.get_users_to_show()
            # self.users_to_show = constants.max_users_to_show

            # Menus
            self.mode_menu = QtWidgets.QMenu(_("Mode"))
            self.units_menu = QtWidgets.QMenu(_("Units"))
            self.language_menu = QtWidgets.QMenu(_("Language"))
            self.auto_adding_menu = QtWidgets.QMenu(_("Auto adding"))

            # Sensors
            self.sensors_and_owners = None
            self.sensors = None

            # Work-horse lists
            self.session_records = []  # type: List[SessionRecords]
            self.clusters = []  # type: List[Cluster]

            # Threads
            self.threads_acquisition = []  # type: List[ThreadAcquisition]
            self.threads_plot = []  # type: List[ThreadPlot]
            self.thread_welcome_email = None
            self.thread_connections = ThreadConnections()
            self.thread_connections_init()

            self.thread_schedule = ThreadSchedule()
            self.thread_schedule.initiate()
            self.thread_schedule.thread.update_available.connect(self.show_update_available_message, QtCore.Qt.QueuedConnection)
            self.thread_schedule.thread.ip_results.connect(self.handle_internet_test, QtCore.Qt.QueuedConnection)
            self.thread_schedule.thread.reboot_scheduled.connect(self.handle_reboot_scheduled,
                                                                 QtCore.Qt.QueuedConnection)

            # Time
            self.session_timestamp = None  # The date time when session started
            self.session_id = None
            self.session_dict = {'status': 'idle', 'id': self.session_id, 'created_at': self.session_timestamp}
            self.last_action = None

            self.session_solo_mode = [None] * self.users_to_show
            self.session_time_left = None
            self.session_time_duration = None
            self.session_time_finish = None  # The date time when session will end

            # Define fonts sizes
            self.font_session_time_spin = QtGui.QFont()
            self.font_session_time_spin.setPointSize(18)
            self.font_session_time_label = QtGui.QFont()
            self.font_session_time_label.setPointSize(14)
            self.font_session_progressbar = QtGui.QFont()
            self.font_session_progressbar.setPointSize(30)

            # Font messages
            self.font_messages = FontsGUI.font_messages

            # Font dialog
            self.font_dialogs = QtGui.QFont()
            self.font_dialogs.setPointSize(17)

            # Font unconnected sensor "Sin sensor"
            self.font_disconnected_sensor_42 = QtGui.QFont()
            self.font_disconnected_sensor_42.setPointSize(13)
            self.font_disconnected_sensor_35 = QtGui.QFont()
            self.font_disconnected_sensor_35.setPointSize(15)
            self.font_disconnected_sensor_30 = QtGui.QFont()
            self.font_disconnected_sensor_30.setPointSize(18)
            self.font_disconnected_sensor_25 = QtGui.QFont()
            self.font_disconnected_sensor_25.setPointSize(22)
            self.font_disconnected_sensor_20 = QtGui.QFont()
            self.font_disconnected_sensor_20.setPointSize(26)
            self.font_disconnected_sensor_12 = QtGui.QFont()
            self.font_disconnected_sensor_12.setPointSize(36)
            self.font_disconnected_sensor_4 = QtGui.QFont()
            self.font_disconnected_sensor_4.setPointSize(72)

            # Font time label
            self.font_label_hora = QtGui.QFont()
            self.font_label_hora.setPointSize(56)  # 28 for 30
            self.font_time = QtGui.QFont()
            self.font_time.setPointSize(116)  # 58 for 30

            # Font time session
            self.font_session_timer = QtGui.QFont()
            self.font_session_timer.setPointSize(30)

            # Font user cluster select
            self.font_label_combobox = QtGui.QFont()
            self.font_label_combobox.setPointSize(20)
            self.font_sensor_select = QtGui.QFont()
            self.font_sensor_select.setPointSize(15)
            self.font_combobox = QtGui.QFont()
            self.font_combobox.setPointSize(23)

            # Font sensor Model
            self.font_sensor_model = QtGui.QFont()
            self.font_sensor_model.setPointSize(12)

            # Define Colors
            self.palette_dark_gray = QtGui.QPalette()
            self.palette_dark_gray.setColor(QtGui.QPalette.Background, QtGui.QColor(QtCore.Qt.darkGray))

            self.palette_gray = QtGui.QPalette()
            self.palette_gray.setColor(QtGui.QPalette.Background, QtGui.QColor(134, 134, 134))

            self.palette_black_color = QtGui.QPalette()
            self.palette_black_color.setColor(QtGui.QPalette.Background, QtGui.QColor(0, 0, 0))

            # Color zones
            self.zone_1_pallete_color = QtGui.QPalette()
            self.zone_1_pallete_color.setColor(QtGui.QPalette.Background, QtGui.QColor(70, 70, 70))

            self.zone_2_pallete_color = QtGui.QPalette()
            self.zone_2_pallete_color.setColor(QtGui.QPalette.Background,
                                               QtGui.QColor(constants.zones_colors_hex_str[1]))

            self.zone_3_pallete_color = QtGui.QPalette()
            self.zone_3_pallete_color.setColor(QtGui.QPalette.Background,
                                               QtGui.QColor(constants.zones_colors_hex_str[2]))

            self.zone_4_pallete_color = QtGui.QPalette()
            self.zone_4_pallete_color.setColor(QtGui.QPalette.Background,
                                               QtGui.QColor(191, 154, 72))

            self.zone_5_pallete_color = QtGui.QPalette()
            self.zone_5_pallete_color.setColor(QtGui.QPalette.Background, QtGui.QColor(176, 55, 98))

            # Palete combobox background
            self.palette_cb_bkg = QtGui.QPalette()
            self.palette_cb_bkg.setColor(self.palette_cb_bkg.Background, QtGui.QColor(74, 76, 79))
            # Palete combobox text
            self.palette_cb_txt = QtGui.QPalette()
            self.palette_cb_txt.setColor(QtGui.QPalette.WindowText, QtGui.QColor(255, 255, 255))

            # Text Colors
            self.paletteTextBlack = QtGui.QPalette()
            self.paletteTextBlack.setColor(QtGui.QPalette.WindowText, QtGui.QColor(0, 0, 0))

            self.paletteTextWhite = QtGui.QPalette()
            self.paletteTextWhite.setColor(QtGui.QPalette.WindowText, QtGui.QColor(255, 255, 255))

            # Frame Colors
            self.paletteFrameOrange = QtGui.QPalette()
            self.paletteFrameOrange.setColor(QtGui.QPalette.Mid, QtGui.QColor(255, 102, 51))

            # Text initial color

            # Grid size
            self.available_grid_sizes = [4, 12, 20, 25, 30, 35, 42]
            self.logo_cluster_indexes = [1, 3, 4, 4, 5, 6, 6]

            ####################################################
            # Top frame stuff
            ####################################################
            self.top_bar_layout = None
            self.left_logo = None
            self.left_image_logo = None
            self.center_logo = None
            self.center_image_logo = None
            self.right_logo = None
            self.right_image_logo = None

            ####################################################
            # Bottom frame stuff
            ####################################################
            self.session_remaining_time_label = QtWidgets.QLabel("")
            self.session_remaining_time_label.setFont(self.font_session_timer)
            self.session_remaining_time_label.setPalette(self.paletteTextWhite)

            self.active_training_type_combobox = TrainingTypeComboBox()
            self.active_training_type_combobox.currentTextChanged.connect(self.change_training_type)  # TODO
            self.session_progress_timer = QtCore.QTimer(self)
            self.session_progress_timer.timeout.connect(self.update_time_and_progress)

            # Training type
            self.training_type_combobox = TrainingTypeComboBox()

            # Session time label/spin for session
            self.session_time_stacked_layout = QtWidgets.QStackedLayout()

            # Buttons
            # StartButton
            start_button = QtGui.QIcon(images_path + 'button_session_start.png')
            pixmap_sb = start_button.pixmap(64, 64, start_button.Active, start_button.On)
            self.start_session_button = ExtendedQLabel()
            self.start_session_button.setPixmap(pixmap_sb)
            self.start_session_button.connect(self.start_session)

            # Stop session
            stop_button = QtGui.QIcon(images_path + 'button_session_stop.png')
            pixmap_stb = stop_button.pixmap(64, 64, stop_button.Active, stop_button.On)
            self.stop_session_button = ExtendedQLabel('icon')
            self.stop_session_button.setPixmap(pixmap_stb)
            self.stop_session_button.connect(self.stop_session)

            # RefreshClusterLayoutButton
            refresh_button = QtGui.QIcon(images_path + 'button_session_clean.png')
            pixmap_rb = refresh_button.pixmap(64, 64, refresh_button.Active, refresh_button.On)
            self.reset_session_button = ExtendedQLabel('icon')
            self.reset_session_button.setPixmap(pixmap_rb)
            self.reset_session_button.connect(self.reset_session)

            ####################################################
            # Main frame stuff
            ####################################################
            self.frames_stacked_clusters = None
            self.layouts_stacked_clusters = None
            self.width_cluster = None
            self.height_cluster = None

            self.set_cluster_size()

            ####################################################
            # Enable cluster frame stuff
            ####################################################
            self.enable_cluster_buttons = None

            ####################################################
            # Selection cluster frame stuff
            ####################################################
            self.comboboxes_select_user_cluster = None
            self.comboboxes_select_sensor_cluster = None
            self.buttons_add_selected_user_cluster = None

            ####################################################
            # Acquisition cluster frame stuff
            ####################################################
            self.dot_read_indicator = [False] * self.users_to_show

            # Font Percentage
            scale = 0.35
            self.font_acq_wear_42 = QtGui.QFont()
            self.font_acq_wear_42.setPointSize(int(scale * 35))
            self.font_acq_wear_35 = QtGui.QFont()
            self.font_acq_wear_35.setPointSize(int(scale * 38))
            self.font_acq_wear_30 = QtGui.QFont()
            self.font_acq_wear_30.setPointSize(int(scale * 35))
            self.font_acq_wear_25 = QtGui.QFont()
            self.font_acq_wear_25.setPointSize(int(scale * 45))
            self.font_acq_wear_20 = QtGui.QFont()
            self.font_acq_wear_20.setPointSize(int(scale * 55))
            self.font_acq_wear_12 = QtGui.QFont()
            self.font_acq_wear_12.setPointSize(int(scale * 80))
            self.font_acq_wear_4 = QtGui.QFont()
            self.font_acq_wear_4.setPointSize(int(scale * 135))
            self.font_wear = QtGui.QFont()

            # Font HR
            scale = 0.9
            self.font_user_hr_42 = QtGui.QFont()
            self.font_user_hr_42.setPointSize(int(scale * 16))
            self.font_user_hr_35 = QtGui.QFont()
            self.font_user_hr_35.setPointSize(int(scale * 18))
            self.font_user_hr_30 = QtGui.QFont()
            self.font_user_hr_30.setPointSize(int(scale * 18))
            self.font_user_hr_25 = QtGui.QFont()
            self.font_user_hr_25.setPointSize(int(scale * 22))
            self.font_user_hr_20 = QtGui.QFont()
            self.font_user_hr_20.setPointSize(int(scale * 26))
            self.font_user_hr_12 = QtGui.QFont()
            self.font_user_hr_12.setPointSize(int(scale * 32))
            self.font_user_hr_4 = QtGui.QFont()
            self.font_user_hr_4.setPointSize(int(scale * 55))

            # Font Percentage
            self.font_user_hr_percentage_42 = QtGui.QFont()
            self.font_user_hr_percentage_42.setPointSize(int(scale * 35))
            self.font_user_hr_percentage_35 = QtGui.QFont()
            self.font_user_hr_percentage_35.setPointSize(int(scale * 38))
            self.font_user_hr_percentage_30 = QtGui.QFont()
            self.font_user_hr_percentage_30.setPointSize(int(scale * 35))
            self.font_user_hr_percentage_25 = QtGui.QFont()
            self.font_user_hr_percentage_25.setPointSize(int(scale * 45))
            self.font_user_hr_percentage_20 = QtGui.QFont()
            self.font_user_hr_percentage_20.setPointSize(int(scale * 55))
            self.font_user_hr_percentage_12 = QtGui.QFont()
            self.font_user_hr_percentage_12.setPointSize(int(scale * 80))
            self.font_user_hr_percentage_4 = QtGui.QFont()
            self.font_user_hr_percentage_4.setPointSize(int(scale * 135))
            self.font_effort = QtGui.QFont()

            # Font calories
            self.font_user_calories_42 = QtGui.QFont()
            self.font_user_calories_42.setPointSize(int(scale * 16))
            self.font_user_calories_35 = QtGui.QFont()
            self.font_user_calories_35.setPointSize(int(scale * 18))
            self.font_user_calories_30 = QtGui.QFont()
            self.font_user_calories_30.setPointSize(int(scale * 18))
            self.font_user_calories_25 = QtGui.QFont()
            self.font_user_calories_25.setPointSize(int(scale * 22))
            self.font_user_calories_20 = QtGui.QFont()
            self.font_user_calories_20.setPointSize(int(scale * 26))
            self.font_user_calories_12 = QtGui.QFont()
            self.font_user_calories_12.setPointSize(int(scale * 32))
            self.font_user_calories_4 = QtGui.QFont()
            self.font_user_calories_4.setPointSize(int(scale * 55))

            # Font name
            self.font_user_name_42 = QtGui.QFont()
            self.font_user_name_42.setPointSize(int(scale * 22))
            self.font_user_name_35 = QtGui.QFont()
            self.font_user_name_35.setPointSize(int(scale * 24))
            self.font_user_name_30 = QtGui.QFont()
            self.font_user_name_30.setPointSize(int(scale * 26))
            self.font_user_name_25 = QtGui.QFont()
            self.font_user_name_25.setPointSize(int(scale * 27))
            self.font_user_name_20 = QtGui.QFont()
            self.font_user_name_20.setPointSize(int(scale * 32))
            self.font_user_name_12 = QtGui.QFont()
            self.font_user_name_12.setPointSize(int(scale * 44))
            self.font_user_name_4 = QtGui.QFont()
            self.font_user_name_4.setPointSize(int(scale * 88))

            # Font name combobox selection
            self.font_user_name_selection_42 = QtGui.QFont()
            self.font_user_name_selection_42.setPointSize(int(scale * 11))
            self.font_user_name_selection_42.setBold(True)
            self.font_user_name_selection_35 = QtGui.QFont()
            self.font_user_name_selection_35.setPointSize(int(scale * 12))
            self.font_user_name_selection_35.setBold(True)
            self.font_user_name_selection_30 = QtGui.QFont()
            self.font_user_name_selection_30.setPointSize(int(scale * 12))
            self.font_user_name_selection_30.setBold(True)
            self.font_user_name_selection_25 = QtGui.QFont()
            self.font_user_name_selection_25.setPointSize(int(scale * 12))
            self.font_user_name_selection_25.setBold(True)
            self.font_user_name_selection_20 = QtGui.QFont()
            self.font_user_name_selection_20.setPointSize(int(scale * 12))
            self.font_user_name_selection_20.setBold(True)
            self.font_user_name_selection_12 = QtGui.QFont()
            self.font_user_name_selection_12.setPointSize(int(scale * 22))
            self.font_user_name_selection_12.setBold(True)
            self.font_user_name_selection_4 = QtGui.QFont()
            self.font_user_name_selection_4.setPointSize(int(scale * 38))
            self.font_user_name_selection_4.setBold(True)

            # Font sensor combobox selection
            self.font_sensor_selection_42 = QtGui.QFont()
            self.font_sensor_selection_42.setPointSize(int(scale * 11))
            self.font_sensor_selection_42.setBold(True)
            self.font_sensor_selection_35 = QtGui.QFont()
            self.font_sensor_selection_35.setPointSize(int(scale * 12))
            self.font_sensor_selection_35.setBold(True)
            self.font_sensor_selection_30 = QtGui.QFont()
            self.font_sensor_selection_30.setPointSize(int(scale * 12))
            self.font_sensor_selection_30.setBold(True)
            self.font_sensor_selection_25 = QtGui.QFont()
            self.font_sensor_selection_25.setPointSize(int(scale * 12))
            self.font_sensor_selection_25.setBold(True)
            self.font_sensor_selection_20 = QtGui.QFont()
            self.font_sensor_selection_20.setPointSize(int(scale * 12))
            self.font_sensor_selection_20.setBold(True)
            self.font_sensor_selection_12 = QtGui.QFont()
            self.font_sensor_selection_12.setPointSize(int(scale * 22))
            self.font_sensor_selection_12.setBold(True)
            self.font_sensor_selection_4 = QtGui.QFont()
            self.font_sensor_selection_4.setPointSize(int(scale * 38))
            self.font_sensor_selection_4.setBold(True)

            # Font sensor number
            self.font_sensor_number_42 = QtGui.QFont()
            self.font_sensor_number_42.setPointSize(int(scale * 5))
            self.font_sensor_number_42.setBold(True)
            self.font_sensor_number_35 = QtGui.QFont()
            self.font_sensor_number_35.setPointSize(int(scale * 5))
            self.font_sensor_number_35.setBold(True)
            self.font_sensor_number_30 = QtGui.QFont()
            self.font_sensor_number_30.setPointSize(int(scale * 6))
            self.font_sensor_number_30.setBold(True)
            self.font_sensor_number_25 = QtGui.QFont()
            self.font_sensor_number_25.setPointSize(int(scale * 8))
            self.font_sensor_number_25.setBold(True)
            self.font_sensor_number_20 = QtGui.QFont()
            self.font_sensor_number_20.setPointSize(int(scale * 10))
            self.font_sensor_number_20.setBold(True)
            self.font_sensor_number_12 = QtGui.QFont()
            self.font_sensor_number_12.setPointSize(int(scale * 12))
            self.font_sensor_number_12.setBold(True)
            self.font_sensor_number_4 = QtGui.QFont()
            self.font_sensor_number_4.setPointSize(int(scale * 16))
            self.font_sensor_number_4.setBold(True)

            self.acquisition_frame_hr_label = None
            self.acquisition_frame_effort_label = None
            self.acquisition_frame_calories_label = None
            self.acquisition_frame_steps_label = None
            self.acquisition_frame_pseudonym_label = None
            self.labels_interface_acquisition_cluster = None  #
            self.acquisition_frame_sensor_label = None

            # Battery stuff
            self.images_battery_acquisition_cluster = None

            self.icon_battery_full = None  # todo: Icon is required as self?
            self.pixmap_battery_full = None  # Pixmaps are required self

            self.icon_battery_mid = None
            self.pixmap_battery_mid = None

            self.icon_battery_low = None
            self.pixmap_battery_low = None

            self.icon_battery_none = None
            self.pixmap_battery_none = None

            self.icon_battery_out = None
            self.pixmap_battery_out = None

            self.icon_battery_hidden = None
            self.pixmap_battery_hidden = None

            self.icon_battery_none = None
            self.pixmap_battery_none = None

            # Signal stuff
            self.images_signal_acquisition_cluster = None

            self.icon_signal_hidden = None
            self.pixmap_signal_hidden = None

            self.buttons_remove_user_acquisition_cluster = None
            self.buttons_edit_user_acquisition_cluster = None

            ####################################################
            # Cluster icons
            ####################################################
            self.labels_disconnected_cluster = None
            self.images_disconnected_cluster = None

            self.icon_connected_true = None
            self.pixmap_connected_true = None

            self.icon_disconnected_false = None
            self.pixmap_disconnected_false = None

            self.icon_band_false = None
            self.pixmap_band_false = None

            self.icon_band_true = None
            self.pixmap_band_true = None

            self.images_disconnected_acquisition_cluster = None
            self.images_band_cluster = None

            self.images_calories_acquisition_cluster = None
            self.images_hr_acquisition_cluster = None

            ####################################################
            # Charts cluster frame stuff
            ####################################################
            self.chart_frame_pseudonym_label = None
            self.pixmap_empty_chart = None
            self.clusters_charts_images = None

            ####################################################
            # Carousel
            ####################################################
            self.carousel = Carousel(self.users_to_show)
            self.carousel.set_show_carousel_handler(self.show_carousel_frame)
            self.carousel.frame.set_clicked_handler(self.handle_carousel_chart_clicked)

            ####################################################
            # Main frame stuff
            ####################################################
            self.frames_enable_cluster = None
            self.frames_selection_cluster = None
            self.frames_acquisition_cluster = None
            self.frames_disconnected_cluster = None
            self.frames_graph_cluster = None

            self.layout_play_stop_buttons = QtWidgets.QStackedLayout()  # Stack for play stop buttons
            self.layout_play_stop_buttons.addWidget(self.start_session_button)  # Stack 0
            self.layout_play_stop_buttons.addWidget(self.stop_session_button)  # Stack 1
            self.layout_play_stop_buttons.addWidget(self.reset_session_button)  # Stack 2
            self.layout_play_stop_buttons.setAlignment(QtCore.Qt.AlignTop)
            self.playStopButtonQStakedLayoutFrame = QtWidgets.QFrame()
            self.playStopButtonQStakedLayoutFrame.setLayout(self.layout_play_stop_buttons)
            self.playStopButtonQStakedLayoutFrame.setContentsMargins(0, 0, 0, 0)
            self.playStopButtonQStakedLayoutFrame.setFixedWidth(66)

            self.disconnectedSensorList = [None] * constants.max_users_to_show
            # Progressbar session
            self.progressBar = QtWidgets.QProgressBar()

            # Time of session
            self.tiempoSesionSpinLayout = QtWidgets.QVBoxLayout()
            self.spin_session_duration = QtWidgets.QSpinBox()

            """self.timer_kill_clusters = QtCore.QTimer(self)
            self.timer_kill_clusters.timeout.connect(self.kill_dead_clusters)
            self.timer_kill_clusters.setInterval(60000)"""

            """self.timer_1min = QtCore.QTimer(self)
            self.timer_1min.timeout.connect(DebugTools.print_alive)
            self.timer_1min.setInterval(10 * 1000)
            self.timer_1min.start()"""

            # Clock on cluster
            self.clock_cluster = QtWidgets.QLabel(self)
            self.timer_clock = QtCore.QTimer(self)
            self.timer_clock.timeout.connect(self.update_clock_cluster)

            # Matriz de clusters de usuarios
            self.layout_grid_clusters_to_show = None
            self.layout_grid_4_clusters = None
            self.layout_grid_12_clusters = None
            self.layout_grid_20_clusters = None
            self.layout_grid_25_clusters = None
            self.layout_grid_30_clusters = None
            self.layout_grid_35_clusters = None
            self.layout_grid_42_clusters = None
            self.frame_grid_4_clusters = None
            self.frame_grid_12_clusters = None
            self.frame_grid_20_clusters = None
            self.frame_grid_25_clusters = None
            self.frame_grid_30_clusters = None
            self.frame_grid_35_clusters = None
            self.frame_grid_42_clusters = None

            # Stack Layout of the above frames
            self.layouts_stacked_grids = None

            self.top_logos_layout = None
            self.top_logos_layout_frame = None

            self.mainLayout = None

            # Frames
            self.frame_main_grid = None
            self.frame_top_bar = None
            self.frame_low_bar = None
            self.frame_session_time_spin = None
            self.frame_session_timer = None
            self.frame_session_time = None

            # TODO: Remove
            # self.upload_all_users()

            # Here we go
            self.setMenuBar(self.activity_mode, self.units)
            self.init_gui()

        except Exception as e:
            self.logger.exception("initializing GUI", e)
            self.elastic_logger.exception("gui_init_exception", e=e)
            return

    def init_gui(self):
        try:
            self.setWindowTitle('RookMotion')
            self.setGeometry(0, 0, constants.resolution_h, constants.resolution_v)

            self.setMinimumHeight(constants.resolution_v)
            self.setMinimumWidth(constants.resolution_h)

            self.setMaximumHeight(constants.resolution_v)
            self.setMaximumWidth(constants.resolution_h)

            self.showFullScreen()
            # self.showMaximized()
            # main palette
            dark_palette = ColorPalettes.dark_palette()
            self.setPalette(dark_palette)

            self.resize_lists()
            self.create_clusters_stacked_frames()

            self.set_grid_layout_dimension(self.users_to_show)

            self.create_main_frame(self.display_mode, self.activity_mode)
            self.setCentralWidget(self.main_frame)

            self.reset_time_and_progress()

            window_icon = QtGui.QIcon(images_path + 'icon_window.png')
            self.setWindowIcon(window_icon)

            # self.setCentralWidget(self.mainFrame)
            # Always show at the end

            if self.activity_mode == "group":
                last_session = self.get_last_session()

                # TODO: User session id is being created new again with every recovery
                if self.last_session_finished(last_session) is not True:
                    self.logger.info("Last session didn't finish, recovering")
                    self.load_last_group()
                    self.restart_last_session(last_session)

            self.verify_licence()
            # output current process id
            self.register_os_signals()

            # Enable scanning here, to prevent conflicts with session recover
            # self.thread_connections.enable_scanning()
            # self.load_last_group()
            # self.view_connections_dialog()
            OsSignals.notify_rookmotion_ready()
            self.elastic_logger.info("successful_start")
            self.show()
        except Exception as e:
            self.logger.exception("Initializing GUI", e)

    def showEvent(self, *args, **kwargs):
        pass

    def setMenuBar(self, mode, units):
        # Create actions

        # UserMenu action1 New User
        self.new_user_action = QtWidgets.QAction(_("Add new user"), self,
                                                 icon=QtGui.QIcon(images_path + 'button_user_new.png'),
                                                 triggered=self.new_user_menu, shortcut=QtGui.QKeySequence.New,
                                                 statusTip=_("Add new user"))
        self.new_user_action.setDisabled(True)

        # UserMenu action2 Edit User
        self.edit_user_action = QtWidgets.QAction(QtGui.QIcon(images_path + 'button_edit_user.png'),
                                                  _('Edit/delete existing user'),
                                                  self, shortcut="Ctrl+Shift+E",
                                                  # self, shortcut=QtGui.QKeySequence.MoveToEndOfLine,
                                                  statusTip=_("Edit/delete existing user"),
                                                  triggered=self.edit_user_menu)

        # UserMenu action3 Resend Mail
        self.resend_mail_action = QtWidgets.QAction(QtGui.QIcon(images_path + 'button_email_resend.png'),
                                                    _("Re-send report"),
                                                    self, shortcut="",
                                                    statusTip=_("Re-send report from previous sessions"),
                                                    triggered=self.resend_email)

        # sensor_menu action1 sensor_info
        self.sensor_info_action = QtWidgets.QAction(QtGui.QIcon(images_path + 'sensor_icon.png'),
                                                    _("Sensor information"),
                                                    self, shortcut="",
                                                    statusTip=_("Sensor information"),
                                                    triggered=self.get_sensor_info)

        # System menu action1
        # self.action_exit = QtWidgets.QAction(QtGui.QIcon('./images/button_application_exit.png'),
        #                                      'Salir de la aplicación',
        #                                      self, shortcut="Ctrl+Q",
        #                                      statusTip="Salir de la aplicación",
        #                                      triggered=self.exitFileMenu)
        # System menu  action2
        self.action_restart = QtWidgets.QAction(QtGui.QIcon(images_path + 'button_system_reboot.png'),
                                                _("Reboot system"),
                                                self, shortcut="Ctrl+R",
                                                statusTip=_("Reboot system"),
                                                triggered=self.restart_system)
        # System menu  action3
        self.action_shutdown = QtWidgets.QAction(QtGui.QIcon(images_path + 'button_system_shutdown.png'),
                                                 _("Shutdown system"),
                                                 self, shortcut="Ctrl+O",
                                                 statusTip=_("Shutdown system"),
                                                 triggered=self.turn_off_system)
        if self.activity_mode == "group":
            # SesionMenu action1
            self.startAction = QtWidgets.QAction(QtGui.QIcon(images_path + 'button_session_start.png'),
                                                 _("Start session"),
                                                 self, shortcut="Ctrl+I",
                                                 statusTip=_("Start session"),
                                                 triggered=self.start_session)
            # SesionMenu action2
            self.stopAction = QtWidgets.QAction(QtGui.QIcon(images_path + 'button_session_stop.png'),
                                                _("Stop session"),
                                                self, shortcut="Ctrl+D",
                                                statusTip=_("Stop session"),
                                                triggered=self.stop_session)
        # GroupMenu action1
        self.save_group_action = QtWidgets.QAction(QtGui.QIcon(images_path + 'button_save.png'), _('Save group'),
                                                   self, shortcut="Ctrl+G",
                                                   statusTip=_("Save the current users and sensors as a group"),
                                                   triggered=self.save_group_menu)
        # GroupMenu action2
        self.load_group_action = QtWidgets.QAction(QtGui.QIcon(images_path + 'button_group_load.png'),
                                                   _("Load/Delete a saved group"),
                                                   self, shortcut="Ctrl+Shift+l",
                                                   statusTip=_("Load/Delete a saved group"),
                                                   triggered=self.load_group_menu)
        # GroupMenu action3
        self.clear_group_action = QtWidgets.QAction(QtGui.QIcon(images_path + 'button_group_clear.png'),
                                                    _("Empty group"),
                                                    self, shortcut="Ctrl+B",
                                                    statusTip=_("Deselect all users and sensors"),
                                                    triggered=self.clear_group_menu)
        # ViewMenu action1
        self.action_show_cluster_logo = QtWidgets.QAction(QtGui.QIcon(images_path + 'button_show_logo.png'),
                                                          _("Show logos"),
                                                          self, shortcut="Ctrl+l",
                                                          statusTip=_("Remove user and show logos"),
                                                          triggered=self.show_cluster_logos)
        # ViewMenu action2
        self.action_show_cluster_clock = QtWidgets.QAction(QtGui.QIcon(images_path + 'button_show_clock.png'),
                                                           _("Show clock"),
                                                           self, shortcut="Ctrl+R",
                                                           statusTip=_("Show cluster clock"),
                                                           triggered=self.show_cluster_clock)

        # Settings actions
        self.sync = QtWidgets.QAction(QtGui.QIcon(images_path + 'icon_sync.png'), _("Sync data"),
                                      self, shortcut="",
                                      statusTip=_("Sync data from/to cloud"),
                                      triggered=self.sync_db)

        self.wifi = QtWidgets.QAction(QtGui.QIcon(images_path + 'icon_wifi.png'), 'WiFi',
                                      self, shortcut="",
                                      statusTip=_("WiFi settings"),
                                      triggered=self.configure_wifi)

        self.change_logo = QtWidgets.QAction(QtGui.QIcon(images_path + 'change_logo_icon.png'), _("Set image logo"),
                                             self, shortcut="",
                                             statusTip=_("Set image logo"),
                                             triggered=self.change_right_logo)

        # Solo Mode Action
        self.solo_mode_action = QtWidgets.QAction(QtGui.QIcon(images_path + 'button_display_mode_individual.png'),
                                                  _("Individual mode"),
                                                  self, shortcut="",
                                                  statusTip=_("Individual mode"),
                                                  triggered=self.change_activity_mode_solo)

        # Group Mode Action
        self.group_mode_action = QtWidgets.QAction(QtGui.QIcon(images_path + 'button_display_mode_group.png'),
                                                   _("Groupal mode"),
                                                   self, shortcut="",
                                                   statusTip=_("Groupal mode"),
                                                   triggered=self.change_activity_mode_group)

        # SI units Action
        self.si_units_action = QtWidgets.QAction(QtGui.QIcon(images_path + 'units_kg.png'), _("International system"),
                                                 self, shortcut="",
                                                 statusTip="Kg, m",
                                                 triggered=self.change_units_si)

        # English units Action
        self.english_units_action = QtWidgets.QAction(QtGui.QIcon(images_path + 'units_lb.png'), _("Standard units"),
                                                      self, shortcut="",
                                                      statusTip="Lb, ft",
                                                      triggered=self.change_units_es)

        # Discrete Mode Action
        # self.discrete_mode_action = QtWidgets.QAction(QtGui.QIcon('./images/menu_discrete_mode.png'), 'Modo discreto',
        #                                              self, checkable=True, shortcut="",
        #                                              statusTip="Modalidad grupal",
        #                                              triggered=self.change_discrete_mode)

        # Discrete Mode Action
        self.discrete_mode_action = QtWidgets.QAction(_("No warnings mode"),
                                                      self, checkable=True, shortcut="",
                                                      statusTip=_("Supress GUI warnings"),
                                                      triggered=self.change_discrete_mode)

        # Display Mode Action
        self.display_mode_action = QtWidgets.QAction(_("Show logos on top bar"),
                                                     self, checkable=True, shortcut="",
                                                     statusTip=_("Show logos on top bar"),
                                                     triggered=self.change_display_mode_to_top_bar)

        # Fast connection Action
        self.connection_mode = QtWidgets.QAction(_("Fast connection mode"),
                                                 self, checkable=True, shortcut="",
                                                 statusTip=_("Try to connect without searching the sensors before"),
                                                 triggered=self.change_connection_mode)

        # Fast connection Action
        self.beta_functions_action = QtWidgets.QAction(_("Use Beta Functions"),
                                                       self, checkable=True, shortcut="",
                                                       statusTip=_("Use Beta Functions"),
                                                       triggered=self.enable_disable_beta_functions)

        # Debug ViewGraphsAction
        if self.flag_debug_mode:
            self.ViewGraphsAction = QtWidgets.QAction(QtGui.QIcon(images_path + 'button_group_size.png'),
                                                      'Ver gráficas',
                                                      self, shortcut="",
                                                      statusTip="Graficas de la sesión",
                                                      triggered=self.view_graphs_debug)

        # RecoverSesionMenu action1
        if self.activity_mode == "group":
            # Disable stop action
            self.stopAction.setDisabled(True)
            if self.startAction.isEnabled():
                self.startAction.setDisabled(True)
                self.start_session_button.setDisabled(True)
        self.action_show_cluster_clock.setDisabled(True)
        self.action_show_cluster_logo.setDisabled(True)

        if self.discrete_mode:
            self.discrete_mode_action.setChecked(True)

        if self.display_mode == "top_bar":
            self.display_mode_action.setChecked(True)

        # try:
        #     mode = not self.settings.get_connection_mode()  # this returns: Can I scan? and we need, can I be fast?
        #     self.connection_mode.setChecked(mode)
        # except:
        #     pass

        try:
            beta_functions = self.settings.beta_functions_are_enabled()
            self.beta_functions_action.setChecked(beta_functions)
            self.logger.info("Beta Functions enabled: {}".format(beta_functions))
        except:
            pass

        # Main menu's
        self.system_menu = self.menuBar().addMenu("RookMotion")
        self.userMenu = self.menuBar().addMenu(_("User"))
        # self.userMenu.setEnabled(False)
        if self.activity_mode == "group":
            self.session_menu = self.menuBar().addMenu(_("Session"))
            self.session_menu.setEnabled(False)
        self.sensor_menu = self.menuBar().addMenu(_("Sensor"))
        self.sensor_menu.setEnabled(False)
        self.groupMenu = self.menuBar().addMenu(_("Group"))
        self.viewMenu = self.menuBar().addMenu(_("View"))
        self.settings_menu = self.menuBar().addMenu(_("Preferences"))

        self.mode_menu.setIcon(QtGui.QIcon(images_path + 'button_display_mode.png'))
        self.settings_menu.addMenu(self.mode_menu)

        self.units_menu.setIcon(QtGui.QIcon(images_path + 'units.png'))
        self.language_menu.setIcon(QtGui.QIcon(images_path + 'menu_translate.png'))
        self.settings_menu.addMenu(self.units_menu)
        self.settings_menu.addMenu(self.language_menu)
        self.settings_menu.addMenu(self.auto_adding_menu)

        self.versionMenu = self.menuBar().addMenu("[v%s]" % __version__)
        self.versionMenu.setDisabled(True)

        self.ip_menu = self.menuBar().addMenu(_("Internet not connected") + " []")
        self.ip_menu.setDisabled(True)

        menu_bar_sheet_styles = ColorPalettes.menu_bar_sheet_styles
        menu_sheet_styles = ColorPalettes.menu_sheet_styles

        self.menuBar().setStyleSheet(menu_bar_sheet_styles)

        self.userMenu.setStyleSheet(menu_sheet_styles)

        self.system_menu.setStyleSheet(menu_sheet_styles)

        self.sensor_menu.setStyleSheet(menu_sheet_styles)

        if self.activity_mode == "group":
            self.session_menu.setStyleSheet(menu_sheet_styles)
        self.groupMenu.setStyleSheet(menu_sheet_styles)
        self.viewMenu.setStyleSheet(menu_sheet_styles)
        self.settings_menu.setStyleSheet(menu_sheet_styles)
        self.mode_menu.setStyleSheet(menu_sheet_styles)
        self.units_menu.setStyleSheet(menu_sheet_styles)
        self.language_menu.setStyleSheet(menu_sheet_styles)
        self.auto_adding_menu.setStyleSheet(menu_sheet_styles)
        self.versionMenu.setStyleSheet(menu_sheet_styles)
        # self.ip_menu.setStyleSheet("color: red")  # TODO Change text color

        if self.flag_debug_mode:
            self.debug_menu = self.menuBar().addMenu("&debug")
            self.debug_menu.addAction(self.ViewGraphsAction)

        # File menu
        # self.system_menu.addAction(self.action_exit)
        self.system_menu.addAction(self.action_restart)
        # self.system_menu.addAction(self.action_shutdown)

        # User menu
        self.userMenu.addAction(self.new_user_action)
        # self.new_user_action.setEnabled(False)
        self.userMenu.addAction(self.edit_user_action)
        # self.edit_user_action.setEnabled(False)
        self.userMenu.addAction(self.resend_mail_action)
        self.resend_mail_action.setEnabled(False)
        # Sensor menu
        self.sensor_menu.addAction(self.sensor_info_action)
        # Sesion menu
        if self.activity_mode == "group":
            self.session_menu.addAction(self.startAction)
            self.session_menu.addAction(self.stopAction)
        # Group menu
        self.groupMenu.addAction(self.save_group_action)
        self.groupMenu.addAction(self.load_group_action)
        self.groupMenu.addAction(self.clear_group_action)
        # View menu
        self.viewMenu.addAction(self.action_show_cluster_clock)
        self.viewMenu.addAction(self.action_show_cluster_logo)
        self.viewMenu.addSeparator()

        # Grid size selector
        self.group_grid_action = QtWidgets.QActionGroup(self.viewMenu)
        for i_size, grid_size in enumerate(self.available_grid_sizes):
            action = QtWidgets.QAction(self)
            action.setText(_("Show {} users").format(grid_size))
            action.setCheckable(True)
            # action.setChecked(True)
            action.setData(grid_size)
            action.setActionGroup(self.group_grid_action)
            action.setIcon(QtGui.QIcon(images_path + 'button_group_size.png'))
            self.viewMenu.addAction(action)
            self.group_grid_action.addAction(action)
        self.group_grid_action.setExclusive(True)
        self.group_grid_action.triggered.connect(self.handle_grid_resize_action)

        # Settings menu
        self.settings_menu.addAction(self.sync)
        self.settings_menu.addAction(self.change_logo)
        # self.settings_menu.addAction(self.discrete_mode_action)
        # self.settings_menu.addAction(self.connection_mode)
        self.settings_menu.addAction(self.wifi)

        # Hidden menu (version)
        self.versionMenu.addAction(self.discrete_mode_action)
        self.versionMenu.addAction(self.display_mode_action)

        # upd: Now we only have fast connection mode
        # self.versionMenu.addAction(self.connection_mode)

        self.versionMenu.addAction(self.beta_functions_action)

        # Mode submenu
        self.mode_menu.addAction(self.solo_mode_action)
        self.mode_menu.addAction(self.group_mode_action)
        if self.activity_mode == "group":
            self.solo_mode_action.setEnabled(True)
            self.group_mode_action.setDisabled(True)
        else:
            self.solo_mode_action.setDisabled(True)
            self.group_mode_action.setEnabled(True)

        # Units submenu
        self.units_menu.addAction(self.si_units_action)
        self.units_menu.addAction(self.english_units_action)
        if units:
            self.si_units_action.setDisabled(True)
            self.english_units_action.setEnabled(True)
        else:
            self.english_units_action.setDisabled(True)
            self.si_units_action.setEnabled(True)

        # Language submenu
        """with DB(['system'], caller='gui') as db:
            lan = db.system.get_setting('language')"""
        lan = self.settings.get_language()
        self.group_language_action = QtWidgets.QActionGroup(self.language_menu)
        for i_size, language_ in enumerate(available_languages):
            action = QtWidgets.QAction(self)
            action.setText(language_[1])
            action.setData(language_[0])
            action.setActionGroup(self.group_language_action)
            # action.setIcon(QtGui.QIcon('./images/button_group_size.png'))
            if language_[0] == lan:
                # self.logger.debug(language_ + "=" + lan)
                action.setDisabled(True)
                action.setEnabled(False)
            self.language_menu.addAction(action)
            self.group_language_action.addAction(action)
        self.group_language_action.setExclusive(True)
        self.group_language_action.triggered.connect(self.handle_language_action)

        # Auto adding submenu
        auto_adding = self.settings.get_auto_adding_enabled()
        self.group_auto_adding_action = QtWidgets.QActionGroup(self.auto_adding_menu)
        auto_adding_options = [[0, _("Disable")], [1, _("Enable")]]  #, [2, _("About it")]]
        for auto_adding_ in auto_adding_options:
            action = QtWidgets.QAction(self)
            action.setText(auto_adding_[1])
            action.setData(auto_adding_[0])
            action.setActionGroup(self.group_auto_adding_action)
            if auto_adding_[0] == auto_adding:
                action.setDisabled(True)
                action.setEnabled(False)
            self.auto_adding_menu.addAction(action)
            self.group_auto_adding_action.addAction(action)
        self.group_auto_adding_action.setExclusive(True)
        self.group_auto_adding_action.triggered.connect(self.handle_auto_adding_action)

        # Exit shortchut
        self.exit_shortcut = QShortcut(QtGui.QKeySequence("Ctrl+Q"), self)
        self.exit_shortcut.activated.connect(self.exit_app)

        # Exception shortcut
        self.exception_shortcut = QShortcut(QtGui.QKeySequence("Ctrl+Shift+alt+e"), self)
        self.exception_shortcut.activated.connect(self.exception_simulator)

        # Advanced options shortchut
        self.advanced_options_shortcut = QShortcut(QtGui.QKeySequence("Ctrl+Shift+A"), self)
        self.advanced_options_shortcut.activated.connect(self.advanced_options_menu_change_status)

        # Exception shortcut
        self.load_last_group_shortcut = QShortcut(QtGui.QKeySequence("Ctrl+Shift+alt+u"), self)
        self.load_last_group_shortcut.activated.connect(self.load_last_group)

        # View connections
        self.view_connections = QShortcut(QtGui.QKeySequence("Ctrl+Shift+alt+v"), self)
        self.view_connections.activated.connect(self.view_connections_dialog)

    def view_connections_dialog(self):
        dialog_connections = DialogConnections(self.clusters)
        dialog_connections.setFont(self.font_messages)
        dialog_connections.exec_()
        delete_dialog(dialog_connections)

    def restart_rookmotion(self):
        message = "This is an induced Exception to restart RM"
        # raise Exception('This is an induced Exception to restart RM')
        self.logger.warning(message)
        exit(1)

    def exception_simulator(self):
        raise Exception('Induced exception')

    def load_last_group(self):
        msg = _("Do you want to add the users from the last session? \n\n {0} seconds")
        decision = show_dialog(msg, 2, accept_button_text=_("Add"), cancel_button_text=_("Cancel"))
        if decision == _("Add") or decision is None:
            self.logger.info('Users from last session will be loaded')
            self.load_group(_("Last"))

    def failure_simulator(self):
        self.logger.warning('Induce Failure')
        import ctypes
        ctypes.string_at(0)

    def handle_internet_test(self, ip_address, internet):
        # f = inspect.stack()[0][3]
        if ip_address != "None":
            try:
                self.ip_menu.setTitle(_("Internet connected") + " [" + str(ip_address) + "]")
                # self.ip_menu.setStyleSheet("color: green")  # TODO change text to green
                self.ip_menu.setEnabled(True)
                self.show_no_internet_dialog_flag = False

                if self.timer_update_ip_on_config_flag:
                    self.timer_update_ip_on_config.stop()
                    self.timer_update_ip_on_config_flag = False

            except Exception as e:
                self.logger.exception("setting the ip", e)
        else:
            self.ip_menu.setTitle(_("Internet not connected") + " []")
            # self.ip_menu.setStyleSheet("color: red")  # TODO change text to red
            self.ip_menu.setEnabled(True)

        if internet is False:
            if self.show_no_internet_dialog_flag:
                self.show_no_internet_dialog_flag = False
                self.show_no_internet_dialog()

    # noinspection PyMethodMayBeStatic
    def show_no_internet_dialog(self):
        try:
            msg = _("The system could not connect to internet")
            inf = _("As you are not connected to internet, you will not be able to update to the most recent version")
            message_box = DialogMessage()
            message_box.set_text(msg)
            message_box.set_informative_text(inf)

            message_box.add_button(_("Connect to internet"), role='accept')
            message_box.add_button(_("Continue without internet"), role='cancel')

            selection = message_box.execute_message()

            if selection == _("Connect to internet"):
                self.configure_wifi()
                if self.show_message_restart_required():
                    self.reboot_system()
                if Git.update_available():
                    self.show_update_available_dialog()
        except Exception as e:
            self.logger.exception("show_no_internet_dialog", e)

    def show_update_available_dialog(self):
        try:
            if not self.session_started_flag:
                msg = _("Update to the most recent version")
                inf = _("Remember that if you don't have the most recent version you could experience system failures." +
                        "\nThe update requires reboot the system")
                message_box = DialogMessage()
                message_box.set_text(msg)
                message_box.set_informative_text(inf)

                message_box.add_button(_("Update now"), role='accept')
                message_box.add_button(_("Update later"), role='cancel')

                selection = message_box.execute_message()

                if selection == _("Update now"):
                    self.reboot_system()
                else:
                    self.show_update_available_message()
        except Exception as e:
            self.logger.exception("show_update_available_dialog", e)

    def show_update_available_message(self):
        try:
            msg = _("UPDATE AVAILABLE, CLICK HERE")
            actions = self.menuBar().actions()
            for action in actions:
                if action.text() == msg:
                    return
            else:
                action = QtWidgets.QAction(msg, self, triggered=self.show_update_available_dialog)
                self.menuBar().addAction(action)  # TODO Change color
        except Exception as e:
            self.logger.exception("show_update_available_message", e)

    # noinspection PyMethodMayBeStatic
    def handle_reboot_scheduled(self, at_time):
        if at_time is not None:
            msg = _("RookMotion will be restarted and updated at {}").format(at_time)
            message_box = DialogMessage()
            message_box.set_text(msg)
            message_box.add_button(_("Postpone"), 'cancel')
            decision = message_box.execute_message()
            if decision == _("Postpone"):
                self.thread_schedule.thread.snooze_reboot()

    def change_activity_mode_solo(self):
        self.change_sessions_mode("solo")

    def change_activity_mode_group(self):
        self.change_sessions_mode("group")

    def change_units_si(self):
        self.change_units("si")

    def change_units_es(self):
        self.change_units("es")

    def change_discrete_mode(self):
        if self.discrete_mode_action.isChecked():
            self.discrete_mode_action.setChecked(True)
            InstallRequirements.create_silent_mode_settings(True, True)
            self.discrete_mode = True
        else:
            self.discrete_mode_action.setChecked(False)
            InstallRequirements.create_silent_mode_settings(False, True)
            self.discrete_mode = False

    def handle_language_action(self, action: QtWidgets.QAction = None):
        restart = False
        try:
            if action is not None:  # Means it comes from the menu
                if self.show_message_restart_required():
                    lan = action.data()
                    action.setDisabled(True)
                    action.setChecked(True)
                    self.logger.info("Language changed to {}".format(lan))
                    with DB(['system'], caller='gui') as db:
                        db.system.set_setting('language', lan)
                    restart = True
            else:
                pass  # This is to call the method without menu
        except Exception as e:
            self.logger.exception("handling language change", e)

        if restart:
            self.restart_rookmotion()

    def handle_auto_adding_action(self, action: QtWidgets.QAction = None):
        try:
            if action is not None:
                enable = action.data()
                if enable in [0, 1]:  # This is for enable, disable actions
                    for action_ in self.group_auto_adding_action.actions():
                        action_.setDisabled(False)
                        if action_.data() == enable:
                            action_.setDisabled(True)
                    action.setChecked(True)
                    with DB(['system'], caller='gui') as db:
                        db.system.set_setting('auto_adding', enable)
                else:  # This is for about it action
                    # self.logger.info("Opening base de conocimientos")
                    # url = QtCore.QUrl("https://www.rookmotion.com/")
                    # QtGui.QDesktopServices.openUrl(url)
                    pass
            else:
                pass
        except Exception as e:
            self.logger.exception("handle_auto_adding_action", e)

    # noinspection PyMethodMayBeStatic
    def show_message_restart_required(self):
        message_box = DialogMessage()
        message_box.set_title(_("Reboot"))
        message_box.set_text(_("This change requires RookMotion to restart"))
        message_box.set_informative_text(_("This will take a few minutes."))
        message_box.set_icon("question")
        message_box.add_button(_("Accept"), role='accept')
        message_box.add_button(_("Cancel"), role='cancel')

        decision = message_box.execute_message()

        if decision == _("Cancel"):
            return False
        return True

    def change_display_mode_to_top_bar(self):
        if self.show_message_restart_required():
            if self.display_mode_action.isChecked():
                mode = "top_bar"
            else:
                mode = "rm"
            self.logger.info("Gui logos changed to {}".format(mode))
            InstallRequirements.create_display_mode_settings(mode, True)
            self.restart_rookmotion()

    def change_connection_mode(self):
        # The element is not checked automatically, so it's not checked when clicked
        if self.connection_mode.isChecked():
            scan_allowed = False
            InstallRequirements.create_bluetooth_settings("Fast")
        else:
            InstallRequirements.create_bluetooth_settings("Normal")
            scan_allowed = True
        self.thread_connections.update_connection_mode(scan_allowed)
        self.connection_mode.setChecked(not scan_allowed)

    def enable_disable_beta_functions(self):
        try:
            if self.show_message_restart_required():
                enable = not self.settings.beta_functions_are_enabled()
                self.settings.enable_beta_functions(enable)
                self.restart_rookmotion()

        except Exception as e:
            self.logger.exception("enable_beta_functions", e)

    def change_sessions_mode(self, new_mode):
        if self.show_message_restart_required():
            self.logger.info("Activity mode changed to {}".format(new_mode))
            if new_mode == "group":
                InstallRequirements.create_activity_mode_settings("group", True)
            elif new_mode == "solo":
                InstallRequirements.create_activity_mode_settings("solo", True)
            self.restart_rookmotion()

    def change_units(self, new_mode):
        if new_mode == "si":
            # InstallRequirements.create_units_settings(True, True)  # units, rewrite. True for SI False for ES
            self.settings.store_si_units(True)
            self.english_units_action.setEnabled(True)
            self.si_units_action.setDisabled(True)
        elif new_mode == "es":
            # InstallRequirements.create_units_settings(False, True)  # units, rewrite. True for SI False for ES
            self.settings.store_si_units(False)
            self.english_units_action.setDisabled(True)
            self.si_units_action.setEnabled(True)

    def change_training_type(self):
        try:
            if self.active_training_type_combobox.currentData() != self.training_type_combobox.currentData():
                self.training_type_combobox.setCurrentIndex(self.active_training_type_combobox.currentIndex())
                self.training_type_combobox.store_current_selection()
                training_type = self.training_type_combobox.currentData()
                with DB(['sessions'], caller='gui') as db:
                    db.sessions.update_session(self.session_id, 'training_type_id', training_type)
                self.logger.info("Training type changed: {}".format(self.active_training_type_combobox.currentText()))
        except Exception as e:
            self.logger.exception("change_training_type", e)

    def create_main_frame(self, display_mode, activity_mode):
        try:
            # Widgets
            self.layouts_stacked_grids = QtWidgets.QStackedLayout()
            self.layouts_stacked_grids.addWidget(self.frame_grid_4_clusters)  # Index 0
            self.layouts_stacked_grids.addWidget(self.frame_grid_12_clusters)  # Index 1
            self.layouts_stacked_grids.addWidget(self.frame_grid_20_clusters)  # Index 2
            self.layouts_stacked_grids.addWidget(self.frame_grid_25_clusters)  # Index 3
            self.layouts_stacked_grids.addWidget(self.frame_grid_30_clusters)  # Index 4
            self.layouts_stacked_grids.addWidget(self.frame_grid_35_clusters)  # Index 5
            self.layouts_stacked_grids.addWidget(self.frame_grid_42_clusters)  # Index 6
            self.layouts_stacked_grids.addWidget(self.carousel.frame)  # Index 7
            self.show_clusters_grid_frame()
            # self.layouts_stacked_grids.setCurrentIndex() #TestGraphs

            self.frame_main_grid = QtWidgets.QFrame()
            self.frame_main_grid.setLayout(self.layouts_stacked_grids)
            self.frame_main_grid.setFrameStyle(QtWidgets.QFrame.StyledPanel | QtWidgets.QFrame.Raised)
            self.frame_main_grid.setLineWidth(3)
            self.frame_main_grid.setAutoFillBackground(True)
            self.frame_main_grid.setPalette(self.palette_black_color)
            # ExpoChange, fixed grid
            if self.display_mode == "top_bar":
                self.frame_main_grid.setFixedHeight(900)
            elif self.activity_mode == "group":
                self.frame_main_grid.setFixedHeight(980)
            else:  # solo mode
                self.frame_main_grid.setFixedHeight(1060)

            self.frame_main_grid.setFrameStyle(QtWidgets.QFrame.NoFrame)
            self.frame_main_grid.setContentsMargins(0, 0, 0, 0)

            self.frame_session_time_spin = QtWidgets.QFrame()
            self.frame_session_timer = QtWidgets.QFrame()
            self.frame_session_time = QtWidgets.QFrame()
            # ExpoChange, top bar
            if self.display_mode == "top_bar":
                self.create_top_bar_frame()

            if self.activity_mode != "solo":
                self.create_bottom_bar_frame()

            # Layout
            self.mainLayout = QtWidgets.QVBoxLayout()
            self.mainLayout.setContentsMargins(1, 1, 1, 1)

            if self.display_mode == "top_bar":
                self.mainLayout.addWidget(self.frame_top_bar)

            self.mainLayout.addWidget(self.frame_main_grid)

            if self.activity_mode != "solo":
                self.mainLayout.addWidget(self.frame_low_bar)

            self.mainLayout.setSpacing(4)

            # Frame
            self.main_frame = QtWidgets.QFrame()
            self.main_frame.setAutoFillBackground(True)
            self.main_frame.setPalette(self.palette_black_color)
            self.main_frame.setLayout(self.mainLayout)
        except Exception as e:
            self.logger.exception("create_main_frame", e)

    def create_clusters_stacked_frames(self):
        try:
            self.create_clusters_enable_frames()
            self.create_clusters_selection_frames()
            self.create_clusters_acquisition_frames()
            self.create_clusters_disconnected_frames()
            self.create_clusters_chart_frames()

            self.layouts_stacked_clusters = [None] * constants.max_users_to_show
            for id_cluster in range(constants.max_users_to_show):
                self.layouts_stacked_clusters[id_cluster] = QtWidgets.QStackedLayout()
                self.layouts_stacked_clusters[id_cluster].addWidget(self.frames_enable_cluster[id_cluster])
                self.layouts_stacked_clusters[id_cluster].addWidget(self.frames_selection_cluster[id_cluster])
                self.layouts_stacked_clusters[id_cluster].addWidget(self.frames_acquisition_cluster[id_cluster])
                self.layouts_stacked_clusters[id_cluster].addWidget(self.frames_disconnected_cluster[id_cluster])
                self.layouts_stacked_clusters[id_cluster].addWidget(self.frames_graph_cluster[id_cluster])
                self.layouts_stacked_clusters[id_cluster].setContentsMargins(1, 1, 1, 1)
                self.show_enable_frame(id_cluster)

            # Add Logo only to cluster 0 and 3, 4, 5 and 6
            # TODO Set index five in every cluster an image to show an add
            # RookMotion_logo
            self.icon_logo = QtGui.QIcon(images_path + 'cluster_logo.png')
            self.image_logo = ExtendedQLabel('image_logo')
            self.image_logo.connect(self.logo_cluster0_clicked)
            pixmap_logo = self.icon_logo.pixmap(self.width_cluster - 4, self.height_cluster - 4, self.icon_logo.Active,
                                                self.icon_logo.On)
            self.image_logo.setPixmap(pixmap_logo)
            self.image_logo.setAlignment(QtCore.Qt.AlignCenter)
            self.layouts_stacked_clusters[0].addWidget(self.image_logo)
            if not self.display_mode == "top_bar":
                self.show_logo_frame(0)

                # Client_logo
                self.icon_client_logo = QtGui.QIcon('./settings/logo_gim.png')
                self.image_client_logo = ExtendedQLabel('icon_client_logo')
                pixmap_client_logo = self.icon_client_logo.pixmap(self.width_cluster - 4, self.height_cluster - 4,
                                                                  self.icon_client_logo.Active,
                                                                  self.icon_client_logo.On)
                self.image_client_logo.setPixmap(pixmap_client_logo)
                self.image_client_logo.setAlignment(QtCore.Qt.AlignCenter)
                # Create the widget for the gym
                self.layouts_stacked_clusters[3].addWidget(self.image_client_logo)
                self.layouts_stacked_clusters[4].addWidget(self.image_client_logo)
                self.layouts_stacked_clusters[5].addWidget(self.image_client_logo)
                self.layouts_stacked_clusters[6].addWidget(self.image_client_logo)

            # Frames
            self.frames_stacked_clusters = [None] * constants.max_users_to_show
            for id_cluster in range(constants.max_users_to_show):
                self.frames_stacked_clusters[id_cluster] = QtWidgets.QFrame()
                self.frames_stacked_clusters[id_cluster].setLayout(self.layouts_stacked_clusters[id_cluster])
                self.frames_stacked_clusters[id_cluster].setFrameStyle(QtWidgets.QFrame.NoFrame)
                self.frames_stacked_clusters[id_cluster].setAutoFillBackground(True)
                self.frames_stacked_clusters[id_cluster].setContentsMargins(0, 0, 0, 0)
                # self.frames_stacked_clusters[id_cluster].setMaximumHeight(self.height_cluster)
        except Exception as e:
            self.logger.exception("Creating stacked frames", e)

    def create_clusters_enable_frames(self):
        # Widgets
        button_enable_cluster = QtGui.QIcon(images_path + 'cluster_enable_cluster.png')
        pixmap_enable_cluster = button_enable_cluster.pixmap(50, 50, button_enable_cluster.Active,
                                                             button_enable_cluster.On)

        self.enable_cluster_buttons = [None] * constants.max_users_to_show
        for id_cluster in range(constants.max_users_to_show):
            self.enable_cluster_buttons[id_cluster] = ExtendedQLabel('enable_button')
            self.enable_cluster_buttons[id_cluster].setPixmap(pixmap_enable_cluster)
            self.enable_cluster_buttons[id_cluster].connect(self.handler_button_enable_cluster)
            self.enable_cluster_buttons[id_cluster].set_id(id_cluster)

        # Layouts
        layouts_enable_cluster = [None] * constants.max_users_to_show
        for id_cluster in range(constants.max_users_to_show):
            layouts_enable_cluster[id_cluster] = QtWidgets.QVBoxLayout()
            layouts_enable_cluster[id_cluster].addWidget(self.enable_cluster_buttons[id_cluster])
            layouts_enable_cluster[id_cluster].setAlignment(QtCore.Qt.AlignHCenter)

        # Frame
        self.frames_enable_cluster = [None] * constants.max_users_to_show
        for id_cluster in range(constants.max_users_to_show):
            self.frames_enable_cluster[id_cluster] = QtWidgets.QFrame()
            self.frames_enable_cluster[id_cluster].setLayout(layouts_enable_cluster[id_cluster])
            self.frames_enable_cluster[id_cluster].setFrameStyle(
                QtWidgets.QFrame.StyledPanel | QtWidgets.QFrame.Raised | QtWidgets.QFrame.NoFrame)

    def create_clusters_selection_frames(self):
        # Widgets
        self.comboboxes_select_user_cluster = [None] * constants.max_users_to_show
        for id_cluster in range(constants.max_users_to_show):
            user_combobox = ExtendedQCombobox()
            self.comboboxes_select_user_cluster[id_cluster] = user_combobox

        self.comboboxes_select_sensor_cluster = [None] * constants.max_users_to_show
        for id_cluster in range(constants.max_users_to_show):
            self.comboboxes_select_sensor_cluster[id_cluster] = ExtendedQCombobox()

        button_add_font = QtGui.QFont()
        button_add_font.setPointSize(15)
        self.buttons_add_selected_user_cluster = [None] * constants.max_users_to_show
        if self.activity_mode == "solo":
            icon_add_user = QtGui.QIcon(images_path + 'button_add_start.png')
            for id_cluster in range(constants.max_users_to_show):
                self.buttons_add_selected_user_cluster[id_cluster] = ExtendedQPushButton('')
                self.buttons_add_selected_user_cluster[id_cluster].connect(self.start_acquisition_cluster, id_cluster)
                self.buttons_add_selected_user_cluster[id_cluster].setIcon(icon_add_user)
                self.buttons_add_selected_user_cluster[id_cluster].setIconSize(QtCore.QSize(30, 30))
                self.buttons_add_selected_user_cluster[id_cluster].setFont(button_add_font)
                self.buttons_add_selected_user_cluster[id_cluster].setFlat(True)
                self.buttons_add_selected_user_cluster[id_cluster].setStyleSheet(
                    'QPushButton {background-color: rgb(0, 0, 0); color: rgb(137, 244, 178); text-align: right;}')
        else:  # group mode
            icon_add_user = QtGui.QIcon(images_path + 'button_add.png')
            for id_cluster in range(constants.max_users_to_show):
                self.buttons_add_selected_user_cluster[id_cluster] = ExtendedQPushButton(" " + _("Add"))
                self.buttons_add_selected_user_cluster[id_cluster].connect(self.start_acquisition_cluster, id_cluster)
                self.buttons_add_selected_user_cluster[id_cluster].setIcon(icon_add_user)
                self.buttons_add_selected_user_cluster[id_cluster].setIconSize(QtCore.QSize(20, 20))
                self.buttons_add_selected_user_cluster[id_cluster].setFont(button_add_font)
                # self.buttons_add_selected_user_cluster[id_cluster].setMaximumHeight(45)
                # self.buttons_add_selected_user_cluster[id_cluster].setStyleSheet(
                #     'QPushButton {background-color: rgb(74, 76, 79); color: rgb(255, 255, 255);}')

        labels_user_cluster = [None] * constants.max_users_to_show
        for id_cluster in range(constants.max_users_to_show):
            labels_user_cluster[id_cluster] = QtWidgets.QLabel(_("User"))
            labels_user_cluster[id_cluster].setAlignment(QtCore.Qt.AlignCenter)
            labels_user_cluster[id_cluster].setFont(self.font_label_combobox)
            labels_user_cluster[id_cluster].setPalette(self.paletteTextWhite)

        labels_sensor_cluster = [None] * constants.max_users_to_show
        for id_cluster in range(constants.max_users_to_show):
            labels_sensor_cluster[id_cluster] = QtWidgets.QLabel(_("Sensor") + "  ")
            labels_sensor_cluster[id_cluster].setAlignment(QtCore.Qt.AlignCenter)
            labels_sensor_cluster[id_cluster].setFont(self.font_sensor_select)
            labels_sensor_cluster[id_cluster].setPalette(self.paletteTextWhite)

        # Layouts
        layouts_selection_sensor_cluster = [None] * constants.max_users_to_show
        for id_cluster in range(constants.max_users_to_show):
            layouts_selection_sensor_cluster[id_cluster] = QtWidgets.QHBoxLayout()
            layouts_selection_sensor_cluster[id_cluster].setContentsMargins(0, 2, 0, 2)
            layouts_selection_sensor_cluster[id_cluster].setSpacing(0)
            layouts_selection_sensor_cluster[id_cluster].addWidget(labels_sensor_cluster[id_cluster])
            layouts_selection_sensor_cluster[id_cluster].addWidget(self.comboboxes_select_sensor_cluster[id_cluster])

        # Frame required for next layout
        frames_selection_sensor = [None] * constants.max_users_to_show
        for id_cluster in range(constants.max_users_to_show):
            frames_selection_sensor[id_cluster] = QtWidgets.QFrame()
            frames_selection_sensor[id_cluster].setContentsMargins(0, 0, 0, 0)
            frames_selection_sensor[id_cluster].setLayout(layouts_selection_sensor_cluster[id_cluster])
            frames_selection_sensor[id_cluster].setFrameStyle(QtWidgets.QFrame.NoFrame)

        layouts_selection_user_cluster = [None] * constants.max_users_to_show
        for id_cluster in range(constants.max_users_to_show):
            layouts_selection_user_cluster[id_cluster] = QtWidgets.QVBoxLayout()
            layouts_selection_user_cluster[id_cluster].addWidget(labels_user_cluster[id_cluster])
            layouts_selection_user_cluster[id_cluster].addWidget(self.comboboxes_select_user_cluster[id_cluster])
            layouts_selection_user_cluster[id_cluster].addWidget(frames_selection_sensor[id_cluster])
            layouts_selection_user_cluster[id_cluster].addWidget(self.buttons_add_selected_user_cluster[id_cluster])

        # Frames
        self.frames_selection_cluster = [None] * constants.max_users_to_show
        for id_cluster in range(constants.max_users_to_show):
            self.frames_selection_cluster[id_cluster] = QtWidgets.QFrame()
            self.frames_selection_cluster[id_cluster].setLayout(layouts_selection_user_cluster[id_cluster])
            self.frames_selection_cluster[id_cluster].setFrameStyle(QtWidgets.QFrame.NoFrame)
            self.frames_selection_cluster[id_cluster].setAutoFillBackground(True)
            self.frames_selection_cluster[id_cluster].setPalette(self.palette_black_color)
            # self.frames_selection_cluster[id_cluster].setMaximumHeight(self.height_cluster)

    def create_clusters_acquisition_frames(self):
        try:
            def add_widget_to_grid(layout_: QtWidgets.QGridLayout(), widget_, grid_setup_: Dict, alignment_):
                from_row = grid_setup_["from_row"]
                from_col = grid_setup_["from_col"]
                count_row = grid_setup_["count_row"]
                count_col = grid_setup_["count_col"]
                layout_.addWidget(widget_, from_row, from_col, count_row, count_col, alignment_)

            # Widgets
            self.acquisition_frame_hr_label = [None] * constants.max_users_to_show
            for cluster_id in range(constants.max_users_to_show):
                self.acquisition_frame_hr_label[cluster_id] = QtWidgets.QLabel("0 {}".format(_('bpm')))

            self.acquisition_frame_calories_label = [None] * constants.max_users_to_show
            for cluster_id in range(constants.max_users_to_show):
                self.acquisition_frame_calories_label[cluster_id] = QtWidgets.QLabel("0 {}".format(_('kcal')))
                self.acquisition_frame_calories_label[cluster_id].setPalette(self.paletteTextWhite)

            self.acquisition_frame_steps_label = [None] * constants.max_users_to_show
            for cluster_id in range(constants.max_users_to_show):
                self.acquisition_frame_steps_label[cluster_id] = QtWidgets.QLabel("")
                self.acquisition_frame_steps_label[cluster_id].setPalette(self.paletteTextWhite)
                self.acquisition_frame_steps_label[cluster_id].hide()

            self.acquisition_frame_effort_label = [None] * constants.max_users_to_show
            for cluster_id in range(constants.max_users_to_show):
                self.acquisition_frame_effort_label[cluster_id] = QtWidgets.QLabel(_("wear"))
                self.acquisition_frame_effort_label[cluster_id].setPalette(self.paletteTextWhite)

            self.acquisition_frame_pseudonym_label = [None] * constants.max_users_to_show
            for cluster_id in range(constants.max_users_to_show):
                self.acquisition_frame_pseudonym_label[cluster_id] = QtWidgets.QLabel("")
                self.acquisition_frame_pseudonym_label[cluster_id].setPalette(self.paletteTextWhite)

            self.acquisition_frame_sensor_label = [None] * constants.max_users_to_show
            for cluster_id in range(constants.max_users_to_show):
                self.acquisition_frame_sensor_label[cluster_id] = QtWidgets.QLabel("")
                self.acquisition_frame_sensor_label[cluster_id].setPalette(self.paletteTextWhite)

            self.labels_interface_acquisition_cluster = [None] * constants.max_users_to_show
            for cluster_id in range(constants.max_users_to_show):
                self.labels_interface_acquisition_cluster[cluster_id] = QtWidgets.QLabel(" " + " ")
                self.labels_interface_acquisition_cluster[cluster_id].setText(" " + " ")
                self.labels_interface_acquisition_cluster[cluster_id].setPalette(self.paletteTextWhite)

            ################################
            # Battery
            ################################

            # Battery full
            self.icon_battery_full = QtGui.QIcon(images_path + 'acquisition_battery_full.png')
            self.pixmap_battery_full = self.icon_battery_full.pixmap(40, 30, self.icon_battery_full.Active,
                                                                     self.icon_battery_full.On)
            # Battery mid
            self.icon_battery_mid = QtGui.QIcon(images_path + 'acquisition_battery_mid.png')
            self.pixmap_battery_mid = self.icon_battery_mid.pixmap(40, 30, self.icon_battery_mid.Active,
                                                                   self.icon_battery_mid.On)
            # Battery low
            self.icon_battery_low = QtGui.QIcon(images_path + 'acquisition_battery_low.png')
            self.pixmap_battery_low = self.icon_battery_low.pixmap(40, 30, self.icon_battery_low.Active,
                                                                   self.icon_battery_low.On)
            # Battery out
            self.icon_battery_out = QtGui.QIcon(images_path + 'acquisition_battery_out.png')
            self.pixmap_battery_out = self.icon_battery_out.pixmap(40, 30, self.icon_battery_out.Active,
                                                                   self.icon_battery_out.On)
            # Battery hidden
            self.icon_battery_hidden = QtGui.QIcon(images_path + 'acquisition_battery_hidden.png')
            self.pixmap_battery_hidden = self.icon_battery_hidden.pixmap(40, 30, self.icon_battery_hidden.Active,
                                                                         self.icon_battery_hidden.On)
            # Battery none
            self.icon_battery_none = QtGui.QIcon(images_path + 'acquisition_battery_none.png')
            self.pixmap_battery_none = self.icon_battery_none.pixmap(40, 30, self.icon_battery_none.Active,
                                                                     self.icon_battery_none.On)

            self.images_battery_acquisition_cluster = [None] * constants.max_users_to_show
            for cluster_id in range(constants.max_users_to_show):
                self.images_battery_acquisition_cluster[cluster_id] = QtWidgets.QLabel('battery_icon')
                self.images_battery_acquisition_cluster[cluster_id].setPixmap(self.pixmap_battery_out)

            ################################
            # Heart and calories icons
            ################################
            self.images_calories_acquisition_cluster = [None] * constants.max_users_to_show
            self.images_hr_acquisition_cluster = [None] * constants.max_users_to_show
            self.images_steps_acquisition_cluster = [None] * constants.max_users_to_show

            ################################
            # Signal
            ################################

            # Signal hidden
            self.icon_signal_hidden = QtGui.QIcon(images_path + 'acquisition_signal_hidden.png')
            if constants.max_users_to_show == 42:
                self.pixmap_signal_hidden = self.icon_signal_hidden.pixmap(20, 20, self.icon_signal_hidden.Active,
                                                                           self.icon_signal_hidden.On)
            else:
                self.pixmap_signal_hidden = self.icon_signal_hidden.pixmap(60, 25, self.icon_signal_hidden.Active,
                                                                           self.icon_signal_hidden.On)

            self.images_signal_acquisition_cluster = [None] * constants.max_users_to_show
            for cluster_id in range(constants.max_users_to_show):
                self.images_signal_acquisition_cluster[cluster_id] = QtWidgets.QLabel('signal_icon')
                # self.images_signal_acquisition_cluster[id_cluster].setPixmap(self.pixmap_signal_none)
                self.images_signal_acquisition_cluster[cluster_id].setPixmap(self.pixmap_signal_hidden)

            ################################
            # Cluster items
            ################################

            # Connected_true icon
            self.icon_connected_true = QtGui.QIcon(images_path + 'indicator_connected_true.png')
            if constants.max_users_to_show == 42:
                self.pixmap_connected_true = self.icon_connected_true.pixmap(20, 20, self.icon_connected_true.Active,
                                                                             self.icon_connected_true.On)
            else:
                self.pixmap_connected_true = self.icon_connected_true.pixmap(27, 27, self.icon_connected_true.Active,
                                                                             self.icon_connected_true.On)
            # Disconnected_true icon
            self.icon_disconnected_true = QtGui.QIcon(images_path + 'indicator_disconnected_true.png')
            if constants.max_users_to_show == 42:
                self.pixmap_disconnected_true = self.icon_disconnected_true.pixmap(20, 20,
                                                                                   self.icon_disconnected_true.Active,
                                                                                   self.icon_disconnected_true.On)
            else:
                self.pixmap_disconnected_true = self.icon_disconnected_true.pixmap(27, 27,
                                                                                   self.icon_disconnected_true.Active,
                                                                                   self.icon_disconnected_true.On)
            # Disconnected_false icon
            self.icon_disconnected_false = QtGui.QIcon(images_path + 'indicator_disconnected_false.png')
            if constants.max_users_to_show == 42:
                self.pixmap_disconnected_false = self.icon_disconnected_false.pixmap(20, 20,
                                                                                     self.icon_disconnected_false.Active,
                                                                                     self.icon_disconnected_false.On)
            else:
                self.pixmap_disconnected_false = self.icon_disconnected_false.pixmap(27, 27,
                                                                                     self.icon_disconnected_false.Active,
                                                                                     self.icon_disconnected_false.On)
                # Band_true icon
            self.icon_band_true = QtGui.QIcon(images_path + 'indicator_band_true.png')
            if self.users_to_show == 42:
                self.pixmap_band_true = self.icon_band_true.pixmap(20, 20, self.icon_band_true.Active,
                                                                   self.icon_band_true.On)
            else:
                self.pixmap_band_true = self.icon_band_true.pixmap(40, 40, self.icon_band_true.Active,
                                                                   self.icon_band_true.On)
            # Band_false icon
            self.icon_band_false = QtGui.QIcon(images_path + 'indicator_band_false.png')
            if self.users_to_show == 42:
                self.pixmap_band_false = self.icon_band_false.pixmap(20, 20, self.icon_band_false.Active,
                                                                     self.icon_band_false.On)
            else:
                self.pixmap_band_false = self.icon_band_false.pixmap(40, 40, self.icon_band_false.Active,
                                                                     self.icon_band_false.On)

            self.images_band_cluster = [None] * constants.max_users_to_show
            for cluster_id in range(constants.max_users_to_show):
                self.images_band_cluster[cluster_id] = QtWidgets.QLabel('band_false_icon')
                self.images_band_cluster[cluster_id].setPixmap(self.pixmap_band_false)
                if constants.max_users_to_show == 42:
                    self.images_band_cluster[cluster_id].setFixedHeight(20)
                    self.images_band_cluster[cluster_id].setFixedWidth(38)

            self.images_disconnected_acquisition_cluster = [None] * constants.max_users_to_show
            for cluster_id in range(constants.max_users_to_show):
                self.images_disconnected_acquisition_cluster[cluster_id] = QtWidgets.QLabel('disconnected_false_icon')
                self.images_disconnected_acquisition_cluster[cluster_id].setPixmap(self.pixmap_disconnected_false)
                if constants.max_users_to_show == 42:
                    self.images_disconnected_acquisition_cluster[cluster_id].setFixedHeight(20)
                    self.images_disconnected_acquisition_cluster[cluster_id].setFixedWidth(20)

            # Buttons to remove user from cluster
            if self.activity_mode == 'group':
                icon_remove_user_cluster = QtGui.QIcon(images_path + 'acquisition_remove_user.png')

            else:  # 'solo' mode
                icon_remove_user_cluster = QtGui.QIcon(images_path + 'button_session_stop_solo.png')

            pixmap_remove_user_cluster = icon_remove_user_cluster.pixmap(20, 20, icon_remove_user_cluster.Active,
                                                                         icon_remove_user_cluster.On)

            self.buttons_remove_user_acquisition_cluster = [None] * constants.max_users_to_show
            for cluster_id in range(constants.max_users_to_show):
                self.buttons_remove_user_acquisition_cluster[cluster_id] = ExtendedQLabel('remove_button')
                self.buttons_remove_user_acquisition_cluster[cluster_id].setPixmap(pixmap_remove_user_cluster)
                if self.activity_mode == 'group':
                    self.buttons_remove_user_acquisition_cluster[cluster_id].connect(self.remove_cluster_acquisition)
                else:  # 'solo' mode
                    self.buttons_remove_user_acquisition_cluster[cluster_id].connect(self.stop_cluster_acquisition)
                self.buttons_remove_user_acquisition_cluster[cluster_id].set_id(cluster_id)
                if constants.max_users_to_show == 42:
                    self.buttons_remove_user_acquisition_cluster[cluster_id].setFixedHeight(20)
                    self.buttons_remove_user_acquisition_cluster[cluster_id].setFixedWidth(20)

            if self.activity_mode is not 'group':
                # Buttons to remove user from cluster
                icon_edit_user_cluster = QtGui.QIcon(images_path + 'button_session_edit_solo.png')
                pixmap_edit_user_cluster = icon_edit_user_cluster.pixmap(20, 20, icon_edit_user_cluster.Active,
                                                                         icon_edit_user_cluster.On)

                self.buttons_edit_user_acquisition_cluster = [None] * constants.max_users_to_show
                for cluster_id in range(constants.max_users_to_show):
                    self.buttons_edit_user_acquisition_cluster[cluster_id] = ExtendedQLabel('edit_button')
                    self.buttons_edit_user_acquisition_cluster[cluster_id].setPixmap(pixmap_edit_user_cluster)
                    self.buttons_edit_user_acquisition_cluster[cluster_id].connect(self.edit_user_cluster)
                    self.buttons_edit_user_acquisition_cluster[cluster_id].set_id(cluster_id)
                    if constants.max_users_to_show == 42:
                        self.buttons_edit_user_acquisition_cluster[cluster_id].setFixedHeight(20)
                        self.buttons_edit_user_acquisition_cluster[cluster_id].setFixedWidth(20)

            # Heart images
            icon_heart = QtGui.QIcon(images_path + 'acquisition_heart.png')
            if constants.max_users_to_show == 42:
                pixmap_heart = icon_heart.pixmap(20, 20, icon_heart.Active,
                                                 icon_heart.On)  # w,h effectively resize the ico
            else:
                pixmap_heart = icon_heart.pixmap(40, 40, icon_heart.Active, icon_heart.On)

            for cluster_id in range(constants.max_users_to_show):
                self.images_hr_acquisition_cluster[cluster_id] = QtWidgets.QLabel('hr_icon')
                self.images_hr_acquisition_cluster[cluster_id].setPixmap(pixmap_heart)

            # Calories image
            icon = QtGui.QIcon(images_path + 'acquisition_calories.png')
            if constants.max_users_to_show == 4:
                pixmap = icon.pixmap(100, 100, icon.Active, icon.On)  # antes 36, 36
            elif constants.max_users_to_show == 42:
                pixmap = icon.pixmap(20, 20, icon.Active, icon.On)  # antes 36, 36
            else:
                pixmap = icon.pixmap(40, 40, icon.Active, icon.On)  # antes 36, 36

            for cluster_id in range(constants.max_users_to_show):
                self.images_calories_acquisition_cluster[cluster_id] = QtWidgets.QLabel('hr_calories')
                self.images_calories_acquisition_cluster[cluster_id].setPixmap(pixmap)

            # Steps image
            icon = QtGui.QIcon(images_path + 'icon_steps.png')
            if constants.max_users_to_show == 4:
                pixmap = icon.pixmap(100, 100, icon.Active, icon.On)  # antes 36, 36
            elif constants.max_users_to_show == 42:
                pixmap = icon.pixmap(20, 20, icon.Active, icon.On)  # antes 36, 36
            else:
                pixmap = icon.pixmap(40, 40, icon.Active, icon.On)  # antes 36, 36

            for cluster_id in range(constants.max_users_to_show):
                self.images_steps_acquisition_cluster[cluster_id] = QtWidgets.QLabel('steps')
                self.images_steps_acquisition_cluster[cluster_id].hide()

            # Layouts
            layouts_acquisition_cluster = [None] * constants.max_users_to_show
            for cluster_id in range(constants.max_users_to_show):
                layouts_acquisition_cluster[cluster_id] = QtWidgets.QGridLayout()
                layout = layouts_acquisition_cluster[cluster_id]

                ###############
                # Row 0
                ###############
                widget = self.images_battery_acquisition_cluster[cluster_id]
                grid_setup = {"from_row": 0, "from_col": 0, "count_row": 1, "count_col": 1}
                add_widget_to_grid(layout, widget, grid_setup, QtCore.Qt.AlignLeft)

                widget = self.labels_interface_acquisition_cluster[cluster_id]
                grid_setup = {"from_row": 0, "from_col": 1, "count_row": 1, "count_col": 1}
                add_widget_to_grid(layout, widget, grid_setup, QtCore.Qt.AlignCenter)

                widget = self.images_signal_acquisition_cluster[cluster_id]
                grid_setup = {"from_row": 0, "from_col": 4, "count_row": 1, "count_col": 1}
                add_widget_to_grid(layout, widget, grid_setup, QtCore.Qt.AlignCenter)

                widget = self.images_disconnected_acquisition_cluster[cluster_id]
                grid_setup = {"from_row": 0, "from_col": 5, "count_row": 1, "count_col": 1}
                add_widget_to_grid(layout, widget, grid_setup, QtCore.Qt.AlignCenter)

                widget = self.images_band_cluster[cluster_id]
                grid_setup = {"from_row": 0, "from_col": 6, "count_row": 1, "count_col": 1}
                add_widget_to_grid(layout, widget, grid_setup, QtCore.Qt.AlignCenter)

                if self.activity_mode != 'group':  # Edit and close buttons
                    widget = self.buttons_edit_user_acquisition_cluster[cluster_id]
                    grid_setup = {"from_row": 0, "from_col": 7, "count_row": 1, "count_col": 1}
                    add_widget_to_grid(layout, widget, grid_setup, QtCore.Qt.AlignCenter)

                    widget = self.buttons_remove_user_acquisition_cluster[cluster_id]
                    grid_setup = {"from_row": 0, "from_col": 8, "count_row": 1, "count_col": 1}
                    add_widget_to_grid(layout, widget, grid_setup, QtCore.Qt.AlignRight)

                else:  # Just close button for 'group' mode
                    widget = self.buttons_remove_user_acquisition_cluster[cluster_id]
                    grid_setup = {"from_row": 0, "from_col": 7, "count_row": 1, "count_col": 1}
                    add_widget_to_grid(layout, widget, grid_setup, QtCore.Qt.AlignRight)

                # Sensor label is superposed over the other icons and also centered using all the columns
                columns = layouts_acquisition_cluster[cluster_id].columnCount()
                widget = self.acquisition_frame_sensor_label[cluster_id]
                grid_setup = {"from_row": 0, "from_col": 0, "count_row": 1, "count_col": columns}
                add_widget_to_grid(layout, widget, grid_setup, QtCore.Qt.AlignCenter)

                ##############################
                # Row 1: Pseudonym
                ##############################
                widget = self.acquisition_frame_pseudonym_label[cluster_id]
                grid_setup = {"from_row": 1, "from_col": 0, "count_row": 1, "count_col": columns}
                add_widget_to_grid(layout, widget, grid_setup, QtCore.Qt.AlignCenter)

                ############################################################
                # Right column (hr, calories, steps...)
                ############################################################

                # To handle effort col width automatically with future columns addition
                if (columns % 2) == 0:
                    effort_cols = columns / 2
                    right_col = columns / 2
                else:
                    effort_cols = int(columns / 2)
                    right_col = columns - effort_cols

                # To increase effort height automatically with future right indicators additions
                right_column_rows = 0

                ####################
                # Heart rate
                next_row = 3
                widget = self.images_hr_acquisition_cluster[cluster_id]
                grid_setup = {"from_row": next_row, "from_col": effort_cols, "count_row": 1, "count_col": 1}
                add_widget_to_grid(layout, widget, grid_setup, QtCore.Qt.AlignRight)

                widget = self.acquisition_frame_hr_label[cluster_id]
                grid_setup = {"from_row": next_row, "from_col": effort_cols + 1, "count_row": 1,
                              "count_col": right_col - 1}
                add_widget_to_grid(layout, widget, grid_setup, QtCore.Qt.AlignLeft)

                right_column_rows += 1
                next_row += 1

                ####################
                # Calories
                widget = self.images_calories_acquisition_cluster[cluster_id]
                grid_setup = {"from_row": next_row, "from_col": effort_cols, "count_row": 1, "count_col": 1}
                add_widget_to_grid(layout, widget, grid_setup, QtCore.Qt.AlignRight)

                widget = self.acquisition_frame_calories_label[cluster_id]
                grid_setup = {"from_row": next_row, "from_col": effort_cols + 1, "count_row": 1,
                              "count_col": right_col - 1}
                add_widget_to_grid(layout, widget, grid_setup, QtCore.Qt.AlignLeft)

                right_column_rows += 1
                next_row += 1

                ####################
                # Steps
                widget = self.images_steps_acquisition_cluster[cluster_id]
                grid_setup = {"from_row": next_row, "from_col": effort_cols, "count_row": 1, "count_col": 1}
                add_widget_to_grid(layout, widget, grid_setup, QtCore.Qt.AlignRight)

                widget = self.acquisition_frame_steps_label[cluster_id]
                grid_setup = {"from_row": next_row, "from_col": effort_cols + 1, "count_row": 1,
                              "count_col": right_col - 1}
                add_widget_to_grid(layout, widget, grid_setup, QtCore.Qt.AlignLeft)

                right_column_rows += 1
                next_row += 1

                ##############################
                # Effort column
                ##############################

                widget = self.acquisition_frame_effort_label[cluster_id]
                grid_setup = {"from_row": 3, "from_col": 0, "count_row": right_column_rows, "count_col": effort_cols}
                add_widget_to_grid(layout, widget, grid_setup, QtCore.Qt.AlignLeft)
                right_column_rows += 1

                layouts_acquisition_cluster[cluster_id].setHorizontalSpacing(5)
                layouts_acquisition_cluster[cluster_id].setVerticalSpacing(1)

            # Frames
            self.frames_acquisition_cluster = [None] * constants.max_users_to_show
            for cluster_id in range(constants.max_users_to_show):
                self.frames_acquisition_cluster[cluster_id] = QtWidgets.QFrame()
                self.frames_acquisition_cluster[cluster_id].setAutoFillBackground(True)
                self.frames_acquisition_cluster[cluster_id].setLayout(layouts_acquisition_cluster[cluster_id])
                self.frames_acquisition_cluster[cluster_id].setFrameStyle(
                    QtWidgets.QFrame.StyledPanel | QtWidgets.QFrame.Raised | QtWidgets.QFrame.NoFrame)
                # self.frames_acquisition_cluster[id_cluster].setMaximumHeight(self.height_cluster)
        except Exception as e:
            self.logger.exception("create_clusters_acquisition_frames", e)

    def create_clusters_disconnected_frames(self):
        # Widgets
        # Disconnected Image
        image_disconnected = QtGui.QIcon(images_path + 'disconnected_frame.png')
        pixmap_disconnected = image_disconnected.pixmap(100, 100, image_disconnected.Active, image_disconnected.On)

        self.images_disconnected_cluster = [None] * constants.max_users_to_show
        for id_cluster in range(constants.max_users_to_show):
            self.images_disconnected_cluster[id_cluster] = QtWidgets.QLabel('disconnected_image')
            self.images_disconnected_cluster[id_cluster].setPixmap(pixmap_disconnected)
            self.images_disconnected_cluster[id_cluster].setAlignment(QtCore.Qt.AlignCenter)

        self.labels_disconnected_cluster = [None] * constants.max_users_to_show
        for id_cluster in range(constants.max_users_to_show):
            self.labels_disconnected_cluster[id_cluster] = QtWidgets.QLabel(_("No sensor"))
            self.labels_disconnected_cluster[id_cluster].setFont(self.font_disconnected_sensor_30)
            self.labels_disconnected_cluster[id_cluster].setAlignment(QtCore.Qt.AlignCenter)
            self.labels_disconnected_cluster[id_cluster].setPalette(self.paletteTextWhite)

        # Layouts
        layouts_disconnected_cluster = [None] * constants.max_users_to_show
        for id_cluster in range(constants.max_users_to_show):
            layouts_disconnected_cluster[id_cluster] = QtWidgets.QVBoxLayout()
            layouts_disconnected_cluster[id_cluster].addWidget(self.labels_disconnected_cluster[id_cluster])
            layouts_disconnected_cluster[id_cluster].addWidget(self.images_disconnected_cluster[id_cluster])

        # Frames
        self.frames_disconnected_cluster = [None] * constants.max_users_to_show
        for id_cluster in range(constants.max_users_to_show):
            self.frames_disconnected_cluster[id_cluster] = QtWidgets.QFrame()
            self.frames_disconnected_cluster[id_cluster].setLayout(layouts_disconnected_cluster[id_cluster])
            self.frames_disconnected_cluster[id_cluster].setFrameStyle(
                QtWidgets.QFrame.StyledPanel | QtWidgets.QFrame.Raised | QtWidgets.QFrame.NoFrame)
            self.frames_disconnected_cluster[id_cluster].setMaximumHeight(self.height_cluster)

    def create_clusters_chart_frames(self):
        # Widgets
        self.chart_frame_pseudonym_label = [None] * constants.max_users_to_show
        for id_cluster in range(constants.max_users_to_show):
            self.chart_frame_pseudonym_label[id_cluster] = QtWidgets.QLabel('')
            self.chart_frame_pseudonym_label[id_cluster].setFont(self.font_user_name_30)
            self.chart_frame_pseudonym_label[id_cluster].setAlignment(QtCore.Qt.AlignCenter)
            self.chart_frame_pseudonym_label[id_cluster].setPalette(self.paletteTextWhite)

        image_empty_graph = QtGui.QIcon(images_path + 'graph_empty_zones.png')
        self.pixmap_empty_chart = image_empty_graph.pixmap(self.width_cluster - 10, self.height_cluster - 10,
                                                           image_empty_graph.Active,
                                                           image_empty_graph.On)

        self.clusters_charts_images = [None] * constants.max_users_to_show
        for id_cluster in range(constants.max_users_to_show):
            self.clusters_charts_images[id_cluster] = ExtendedQLabel('session_graph')
            self.clusters_charts_images[id_cluster].setPixmap(self.pixmap_empty_chart)
            self.clusters_charts_images[id_cluster].setAlignment(QtCore.Qt.AlignCenter)
            if self.activity_mode == "group":
                self.clusters_charts_images[id_cluster].connect(self.show_carousel_frame)
            if self.activity_mode == "solo":
                self.clusters_charts_images[id_cluster].connect(self.show_enable_frame)
            self.clusters_charts_images[id_cluster].set_id(id_cluster)

        # Layouts
        layouts_graph_cluster = [None] * constants.max_users_to_show
        for id_cluster in range(constants.max_users_to_show):
            layouts_graph_cluster[id_cluster] = QtWidgets.QVBoxLayout()
            layouts_graph_cluster[id_cluster].addWidget(self.chart_frame_pseudonym_label[id_cluster])
            layouts_graph_cluster[id_cluster].addWidget(self.clusters_charts_images[id_cluster])

        # Frames
        self.frames_graph_cluster = [None] * constants.max_users_to_show
        for id_cluster in range(constants.max_users_to_show):
            self.frames_graph_cluster[id_cluster] = QtWidgets.QFrame()
            self.frames_graph_cluster[id_cluster].setLayout(layouts_graph_cluster[id_cluster])
            self.frames_graph_cluster[id_cluster].setFrameStyle(
                QtWidgets.QFrame.StyledPanel | QtWidgets.QFrame.Raised | QtWidgets.QFrame.NoFrame)
            # self.frames_graph_cluster[id_cluster].setMaximumHeight(self.height_cluster)

    def create_progressbar(self):
        self.progressBar.setMinimum(0)
        self.progressBar.setMaximum(100)
        self.progressBar.setFixedHeight(65)  # Height
        self.progressBar.setFixedWidth(constants.resolution_h - 320)  # 1600 en FHD
        self.progressBar.setFont(FontsGUI.font_session_progressbar)
        self.progressBar.setPalette(ColorPalettes.palette_white_text)
        style = "QProgressBar {border: 2px solid grey; border-radius: 5px; text-align: center;}"
        style += "QProgressBar::chunk {background-color: #"
        style += constants.progressbar_color + "; width: 20px;}"
        self.progressBar.setStyleSheet(style)

        # self.progressBar.setMaximumHeight(100)
        # self.progressBar.setMinimumHeight(100)

    def create_session_duration_spin(self):
        self.spin_session_duration.setRange(1, 999)
        self.spin_session_duration.setValue(60)  # 60
        self.spin_session_duration.setSuffix(" min")
        self.spin_session_duration.setFont(FontsGUI.font_session_time_spin)
        self.spin_session_duration.setAlignment(QtCore.Qt.AlignTop)

        # TODO  COMPLETAR Centrar el texto
        tiempoSesionLabel = QtWidgets.QLabel(_("Session time"))
        tiempoSesionLabel.setFont(FontsGUI.font_session_time_label)
        tiempoSesionLabel.setPalette(ColorPalettes.palette_white_text)
        tiempoSesionLabel.setAlignment(QtCore.Qt.AlignCenter)

        self.frame_session_time_spin = QtWidgets.QFrame()
        self.tiempoSesionSpinLayout.addWidget(self.spin_session_duration)
        # self.tiempoSesionSpinLayout.addWidget(tiempoSesionLabel)
        self.tiempoSesionSpinLayout.addWidget(self.training_type_combobox)
        self.tiempoSesionSpinLayout.setSpacing(1)  # Sets the space between label & spin
        self.tiempoSesionSpinLayout.setContentsMargins(1, 1, 1, 1)
        self.frame_session_time_spin.setLayout(self.tiempoSesionSpinLayout)
        self.frame_session_time_spin.setContentsMargins(1, 1, 1, 1)
        self.frame_session_time_spin.setFixedWidth(200)
        # self.tiempoSesionSpinFrame.setFrameShape(QtWidgets.QFrame.StyledPanel) #Prueba para ver el tamaño del frame

        # Creacion de timerSesionLayout
        session_time_layout = QtWidgets.QVBoxLayout()
        session_time_layout.addWidget(self.session_remaining_time_label)
        # session_time_layout.addWidget(self.active_training_type_combobox)
        self.frame_session_timer = QtWidgets.QFrame()
        self.frame_session_timer.setLayout(session_time_layout)
        self.frame_session_timer.setFixedWidth(200)
        # self.timerSesionFrame.setFrameShape(QtWidgets.QFrame.StyledPanel) #Prueba para ver el tamaño del frame

        # Creacion de timer
        self.session_time_stacked_layout.addWidget(self.frame_session_time_spin)
        self.session_time_stacked_layout.addWidget(self.frame_session_timer)

        # Frame del QStackedLayou
        self.frame_session_time = QtWidgets.QFrame()
        self.frame_session_time.setLayout(self.session_time_stacked_layout)

    def create_top_bar_frame(self):
        self.top_bar_layout = QtWidgets.QHBoxLayout()

        # Individual changeable logos
        self.height_left_logo = 76
        self.height_center_logo = 76
        self.height_right_logo = 76

        base_path = '/home/pi/RookMotion/settings/'

        left_logo_path = base_path + 'left_logo.png'
        if os.path.isfile(left_logo_path) is False:
            self.left_logo = QtGui.QIcon('/home/pi/RookMotion/images/cluster_logo.png')
        else:
            self.left_logo = QtGui.QIcon(left_logo_path)

        center_logo_path = base_path + 'center_logo.png'
        if os.path.isfile(center_logo_path) is False:
            self.center_logo = QtGui.QIcon('/home/pi/RookMotion/images/acquisition_signal_hidden.png')
        else:
            self.center_logo = QtGui.QIcon(center_logo_path)

        right_logo_path = base_path + 'logo_gim.png'
        if os.path.isfile(right_logo_path) is False:
            self.right_logo = QtGui.QIcon('/home/pi/RookMotion/images/cluster_logo.png')
        else:
            self.right_logo = QtGui.QIcon(right_logo_path)

        self.left_image_logo = ExtendedQLabel('left_image_logo')
        # self.left_image_logo.connect(self.change_left_logo)
        pixmap_left_logo = self.left_logo.pixmap(1920 / 3 - 4, self.height_left_logo - 4, self.left_logo.Active,
                                                 self.left_logo.On)
        self.left_image_logo.setPixmap(pixmap_left_logo)
        self.left_image_logo.setAlignment(QtCore.Qt.AlignCenter)
        self.left_image_logo.setFixedHeight(self.height_left_logo - 4)

        self.center_image_logo = ExtendedQLabel('center_image_logo')
        # self.center_image_logo.connect(self.left_logo_clicked)
        pixmap_center_logo = self.center_logo.pixmap(1920 / 3 - 4, self.height_center_logo - 4,
                                                     self.center_logo.Active, self.center_logo.On)
        self.center_image_logo.setPixmap(pixmap_center_logo)
        self.center_image_logo.setAlignment(QtCore.Qt.AlignCenter)
        self.center_image_logo.setFixedHeight(self.height_center_logo - 4)

        self.image_client_logo = ExtendedQLabel('right_image_logo')
        self.image_client_logo.connect(self.change_right_logo)
        pixmap_right_logo = self.right_logo.pixmap(self.width_cluster - 4, self.height_right_logo - 4,
                                                   self.right_logo.Active, self.right_logo.On)
        # self.right_image_logo.setPixmap(pixmap_logo)
        self.image_client_logo.setPixmap(pixmap_right_logo)
        self.image_client_logo.setAlignment(QtCore.Qt.AlignCenter)
        self.image_client_logo.setFixedHeight(self.height_right_logo - 4)

        self.top_bar_layout.addWidget(self.left_image_logo)
        self.top_bar_layout.addWidget(self.center_image_logo)
        self.top_bar_layout.addWidget(self.image_client_logo)
        self.top_bar_layout.setContentsMargins(0, 0, 1, 1)
        # self.top_bar_layout.setSpacing(20)

        ############# Single changable logos #################
        #
        # height_top_bar_logo = 75
        # self.top_bar_logo = QtGui.QIcon('./settings/top_bar_logos.png')
        # self.top_bar_image_logo = ExtendedQLabel('top_bar_logo')
        # pixmap_logo = self.top_bar_logo.pixmap(1876, height_top_bar_logo - 4, self.top_bar_logo.Active,
        #                                     self.top_bar_logo.On)
        # self.top_bar_image_logo.setPixmap(pixmap_logo)
        # self.top_bar_image_logo.setAlignment(QtCore.Qt.AlignCenter)
        # self.top_bar_image_logo.setFixedHeight(height_top_bar_logo)
        #
        # self.top_bar_layout.addWidget(self.top_bar_image_logo)
        # self.top_bar_layout.setContentsMargins(5, 0, 1, 1)
        #
        # Add the top bar to the main frame
        self.frame_top_bar = QtWidgets.QFrame()
        self.frame_top_bar.setLayout(self.top_bar_layout)
        self.frame_top_bar.setFrameStyle(QtWidgets.QFrame.NoFrame)
        self.frame_top_bar.setContentsMargins(5, 0, 1, 1)
        self.frame_top_bar.setAutoFillBackground(True)
        self.frame_top_bar.setPalette(self.palette_black_color)
        self.frame_top_bar.setFixedHeight(80)

    def create_bottom_bar_frame(self):
        self.create_progressbar()
        self.create_session_duration_spin()

        self.barraInferiorLayout = QtWidgets.QHBoxLayout()
        self.barraInferiorLayout.addWidget(self.progressBar)
        self.barraInferiorLayout.addWidget(self.frame_session_time)
        self.barraInferiorLayout.addWidget(self.playStopButtonQStakedLayoutFrame)
        self.barraInferiorLayout.setContentsMargins(2, 2, 2, 2)
        self.barraInferiorLayout.setSpacing(20)

        self.frame_low_bar = QtWidgets.QFrame()
        self.frame_low_bar.setLayout(self.barraInferiorLayout)
        self.frame_low_bar.setAutoFillBackground(True)
        self.frame_low_bar.setPalette(self.palette_black_color)
        self.frame_low_bar.setFixedHeight(80)

    def create_clusters_grids_frames(self):
        # Remove items
        # TODO review if I need to make a function for every size
        # if they are already created
        if self.frame_grid_42_clusters is not None:
            for id_cluster in reversed(range(self.layout_grid_42_clusters.count())):
                self.layout_grid_clusters_to_show.removeWidget(self.frames_stacked_clusters[id_cluster])
        # if self.frame_grid_12_clusters is not None:
        #     for id_cluster in reversed(range(self.layout_grid_12_clusters.count())):
        #         self.layout_grid_clusters_to_show.removeWidget(self.frames_stacked_clusters[id_cluster])
        else:
            self.layout_grid_4_clusters = QtWidgets.QGridLayout()
            self.layout_grid_12_clusters = QtWidgets.QGridLayout()
            self.layout_grid_20_clusters = QtWidgets.QGridLayout()
            self.layout_grid_25_clusters = QtWidgets.QGridLayout()
            self.layout_grid_30_clusters = QtWidgets.QGridLayout()
            self.layout_grid_35_clusters = QtWidgets.QGridLayout()
            self.layout_grid_42_clusters = QtWidgets.QGridLayout()
            self.frame_grid_4_clusters = QtWidgets.QFrame()
            self.frame_grid_12_clusters = QtWidgets.QFrame()
            self.frame_grid_20_clusters = QtWidgets.QFrame()
            self.frame_grid_25_clusters = QtWidgets.QFrame()
            self.frame_grid_30_clusters = QtWidgets.QFrame()
            self.frame_grid_35_clusters = QtWidgets.QFrame()
            self.frame_grid_42_clusters = QtWidgets.QFrame()
            self.frame_grid_4_clusters.setLayout(self.layout_grid_4_clusters)
            self.frame_grid_12_clusters.setLayout(self.layout_grid_12_clusters)
            self.frame_grid_20_clusters.setLayout(self.layout_grid_20_clusters)
            self.frame_grid_25_clusters.setLayout(self.layout_grid_25_clusters)
            self.frame_grid_30_clusters.setLayout(self.layout_grid_30_clusters)
            self.frame_grid_35_clusters.setLayout(self.layout_grid_35_clusters)
            self.frame_grid_42_clusters.setLayout(self.layout_grid_42_clusters)

        if self.users_to_show == 4:
            lines = 2
            columns = 2
            self.layout_grid_clusters_to_show = self.layout_grid_4_clusters

        elif self.users_to_show == 12:
            lines = 3
            columns = 4
            self.layout_grid_clusters_to_show = self.layout_grid_12_clusters

        elif self.users_to_show == 20:
            lines = 4
            columns = 5
            self.layout_grid_clusters_to_show = self.layout_grid_20_clusters
        elif self.users_to_show == 25:
            lines = 5
            columns = 5
            self.layout_grid_clusters_to_show = self.layout_grid_25_clusters
        elif self.users_to_show == 30:
            lines = 5
            columns = 6
            self.layout_grid_clusters_to_show = self.layout_grid_30_clusters
        elif self.users_to_show == 35:
            lines = 5
            columns = 7
            self.layout_grid_clusters_to_show = self.layout_grid_35_clusters
        else:  # Case when 42
            lines = 6
            columns = 7
            self.layout_grid_clusters_to_show = self.layout_grid_42_clusters

        id_cluster = 0
        for vertical in range(lines):
            for horizontal in range(columns):
                if self.users_to_show == id_cluster:
                    break
                else:
                    self.layout_grid_clusters_to_show.addWidget(self.frames_stacked_clusters[id_cluster], vertical,
                                                                horizontal,
                                                                QtCore.Qt.AlignCenter)
                    id_cluster += 1
                self.layout_grid_clusters_to_show.setVerticalSpacing(4)
                self.layout_grid_clusters_to_show.setHorizontalSpacing(4)

    def set_cluster_size(self):
        if self.display_mode == "top_bar":
            if self.users_to_show == 4:
                self.width_cluster = 950
                self.height_cluster = 435
            elif self.users_to_show == 12:
                self.width_cluster = 470
                self.height_cluster = 290
            elif self.users_to_show == 20:
                self.width_cluster = 374
                self.height_cluster = 215
            elif self.users_to_show == 25:
                self.width_cluster = 374
                self.height_cluster = 171
            elif self.users_to_show == 30:
                self.width_cluster = 310
                self.height_cluster = 171
            elif self.users_to_show == 35:
                self.width_cluster = 267
                self.height_cluster = 171
            else:  # 42
                self.width_cluster = 267
                self.height_cluster = 143

        elif self.activity_mode == "group":
            if self.users_to_show == 4:
                self.width_cluster = 950
                self.height_cluster = 477
            elif self.users_to_show == 12:
                self.width_cluster = 470
                self.height_cluster = 318
            elif self.users_to_show == 20:
                self.width_cluster = 374
                self.height_cluster = 238
            elif self.users_to_show == 25:
                self.width_cluster = 374
                self.height_cluster = 189
            elif self.users_to_show == 30:
                self.width_cluster = 310
                self.height_cluster = 189
            elif self.users_to_show == 35:
                self.width_cluster = 267
                self.height_cluster = 189
            else:  # 42
                self.width_cluster = 267
                self.height_cluster = 157

        else:  # solo_mode
            if self.users_to_show == 4:
                self.width_cluster = 985
                self.height_cluster = 513
            elif self.users_to_show == 12:
                self.width_cluster = 470
                self.height_cluster = 342
            elif self.users_to_show == 20:
                self.width_cluster = 374
                self.height_cluster = 256
            elif self.users_to_show == 25:
                self.width_cluster = 374
                self.height_cluster = 204
            elif self.users_to_show == 30:
                self.width_cluster = 310
                self.height_cluster = 204
            elif self.users_to_show == 35:
                self.width_cluster = 267
                self.height_cluster = 204
            else:  # 42
                self.width_cluster = 267
                self.height_cluster = 169

    def set_cluster_fonts(self):
        if self.users_to_show == 4:
            self.font_wear = self.font_acq_wear_4
            fuenteUserHeartRate = self.font_user_hr_4
            fuenteUserCalories = self.font_user_calories_4
            fuenteUserHeartRatePercentage = self.font_user_hr_percentage_4
            fuenteUserName = self.font_user_name_4
            fuenteDisconnectedSensor = self.font_disconnected_sensor_4
            fuente_sensor_number = self.font_sensor_number_4
            fuente_user_selection = self.font_user_name_selection_4
            fuente_sensor_selection = self.font_sensor_selection_4

        elif self.users_to_show == 12:
            self.font_wear = self.font_acq_wear_12
            fuenteUserHeartRate = self.font_user_hr_12
            fuenteUserCalories = self.font_user_calories_12
            fuenteUserHeartRatePercentage = self.font_user_hr_percentage_12
            fuenteUserName = self.font_user_name_12
            fuenteDisconnectedSensor = self.font_disconnected_sensor_12
            fuente_sensor_number = self.font_sensor_number_12
            fuente_user_selection = self.font_user_name_selection_12
            fuente_sensor_selection = self.font_sensor_selection_12

        elif self.users_to_show == 20:
            self.font_wear = self.font_acq_wear_20
            fuenteUserHeartRate = self.font_user_hr_20
            fuenteUserCalories = self.font_user_calories_20
            fuenteUserHeartRatePercentage = self.font_user_hr_percentage_20
            fuenteUserName = self.font_user_name_20
            fuenteDisconnectedSensor = self.font_disconnected_sensor_20
            fuente_sensor_number = self.font_sensor_number_20
            fuente_user_selection = self.font_user_name_selection_20
            fuente_sensor_selection = self.font_sensor_selection_20

        elif self.users_to_show == 25:
            self.font_wear = self.font_acq_wear_25
            fuenteUserHeartRate = self.font_user_hr_25
            fuenteUserCalories = self.font_user_calories_25
            fuenteUserHeartRatePercentage = self.font_user_hr_percentage_25
            fuenteUserName = self.font_user_name_25
            fuenteDisconnectedSensor = self.font_disconnected_sensor_25
            fuente_sensor_number = self.font_sensor_number_25
            fuente_user_selection = self.font_user_name_selection_25
            fuente_sensor_selection = self.font_sensor_selection_25

        elif self.users_to_show == 30:
            self.font_wear = self.font_acq_wear_30
            fuenteUserHeartRate = self.font_user_hr_30
            fuenteUserCalories = self.font_user_calories_30
            fuenteUserHeartRatePercentage = self.font_user_hr_percentage_30
            fuenteUserName = self.font_user_name_30
            fuenteDisconnectedSensor = self.font_disconnected_sensor_30
            fuente_sensor_number = self.font_sensor_number_30
            fuente_user_selection = self.font_user_name_selection_30
            fuente_sensor_selection = self.font_sensor_selection_30


        elif self.users_to_show == 35:
            self.font_wear = self.font_acq_wear_35
            fuenteUserHeartRate = self.font_user_hr_35
            fuenteUserCalories = self.font_user_calories_35
            fuenteUserHeartRatePercentage = self.font_user_hr_percentage_35
            fuenteUserName = self.font_user_name_35
            fuenteDisconnectedSensor = self.font_disconnected_sensor_35
            fuente_sensor_number = self.font_sensor_number_35
            fuente_user_selection = self.font_user_name_selection_35
            fuente_sensor_selection = self.font_sensor_selection_35

        else:  # 42 usuarios
            self.font_wear = self.font_acq_wear_42
            fuenteUserHeartRate = self.font_user_hr_42
            fuenteUserCalories = self.font_user_calories_42
            fuenteUserHeartRatePercentage = self.font_user_hr_percentage_42
            fuenteUserName = self.font_user_name_42
            fuenteDisconnectedSensor = self.font_disconnected_sensor_42
            fuente_sensor_number = self.font_sensor_number_42
            fuente_user_selection = self.font_user_name_selection_42
            fuente_sensor_selection = self.font_sensor_selection_42

        self.font_effort = fuenteUserHeartRatePercentage

        for i in range(0, self.users_to_show):
            self.acquisition_frame_hr_label[i].setFont(fuenteUserHeartRate)
            # self.labels_hr_acquisition_cluster[i].setimumWidth(2 * self.width_cluster / 7)

            self.acquisition_frame_calories_label[i].setFont(fuenteUserCalories)
            # self.labels_calories_acquisition_cluster[i].setMaximumWidth(2 * self.width_cluster / 7)

            self.acquisition_frame_steps_label[i].setFont(fuenteUserCalories)

            if self.acquisition_frame_effort_label[i].text().startswith(_("wear")):
                self.acquisition_frame_effort_label[i].setFont(self.font_wear)
            else:
                self.acquisition_frame_effort_label[i].setFont(self.font_effort)
            # self.labels_effort_acquisition_cluster[i].setMaximumWidth(self.width_cluster)

            self.acquisition_frame_pseudonym_label[i].setFont(fuenteUserName)
            self.chart_frame_pseudonym_label[i].setFont(fuenteUserName)

            self.acquisition_frame_sensor_label[i].setFont(fuente_sensor_number)
            self.comboboxes_select_user_cluster[i].setFont(fuente_user_selection)
            self.comboboxes_select_user_cluster[i].update_popup_font_size(fuente_user_selection)
            self.comboboxes_select_sensor_cluster[i].setFont(fuente_sensor_selection)
            self.comboboxes_select_sensor_cluster[i].update_popup_font_size(fuente_sensor_selection)
            self.labels_disconnected_cluster[i].setFont(fuenteDisconnectedSensor)

    def set_grid_layout_dimension(self, new_users_to_show):
        try:
            if new_users_to_show != self.users_to_show:
                current_users_to_show = self.users_to_show

                # Reduce Size
                if current_users_to_show > new_users_to_show:
                    # Count active users
                    current_active_users = 0
                    for id_cluster, cluster in enumerate(self.clusters):
                        if cluster.frame['name'] == 'acquisition':
                            current_active_users += 1
                            if id_cluster + 1 > new_users_to_show:
                                msg = _("Grid size cannot be reduced")
                                msg += _("\n One container would be deleted")
                                decision = show_dialog(msg, timeout=0, accept_button_text=_("Ok"))
                                if decision == _("Ok"):
                                    return False

                    if current_active_users > new_users_to_show - 2:  # minus 2 because of the logos
                        msg = _("Grid size cannot be reduced")
                        msg += _("\n There are more active users than the new number of containers")
                        decision = show_dialog(msg, timeout=0, accept_button_text=_("Ok"))
                        if decision == _("Ok"):
                            return False

                    else:
                        # Change size and move clusters
                        for id_cluster in range(current_users_to_show):
                            #  ie 0..19   > 12
                            if id_cluster > new_users_to_show:
                                if self.layouts_stacked_clusters[id_cluster].currentIndex() == Cluster.get_frame_id(
                                        'acquisition'):
                                    self.logger.debug("Removing cluster {}".format(id_cluster))
                                    self.remove_cluster_acquisition(id_cluster)
                                    # Add the user
                                    # TODO Re-allocate clusters
                        # Massive actions
                        # self.save_group_menu(last_group=True)
                        # self.fill_comboboxes(None)
                        # self.thread_connections.update_clusters()

                InstallRequirements.create_gui_settings(new_users_to_show, True)

            self.users_to_show = new_users_to_show
            self.resize_lists()
            self.thread_connections.resize_lists(new_users_to_show)
            self.carousel.resize_lists(new_users_to_show)
            self.create_clusters_grids_frames()
            self.set_cluster_size()
            self.set_cluster_fonts()
            self.resize_icons()
            self.fill_comboboxes(None)
            self.enable_logo_and_grid_menus()

            return True
        except Exception as e:
            self.logger.exception("set_grid_layout_dimension", e)

    def resize_icons(self):
        if self.layouts_stacked_grids is not None:
            self.show_clusters_grid_frame()
        graphicsIcon = QtGui.QIcon(images_path + 'graph_empty_zones.png')
        pixmapG = graphicsIcon.pixmap(self.width_cluster - 10, self.height_cluster - 10, graphicsIcon.Active,
                                      graphicsIcon.On)
        if not self.display_mode == "top_bar":  # Group or solo modes
            # Update RookMotion and gym logos
            # RookMotion logo
            pixmap_logo = self.icon_logo.pixmap(self.width_cluster - 4, self.height_cluster - 4, self.icon_logo.Active,
                                                self.icon_logo.On)
            self.image_logo.setPixmap(pixmap_logo)
            if self.layouts_stacked_clusters[0].currentIndex() != Cluster.get_frame_id('acquisition'):
                self.show_logo_frame(0)

            # gym logo
            pixmap_client_logo = self.icon_client_logo.pixmap(self.width_cluster - 4, self.height_cluster - 4,
                                                              self.icon_client_logo.Active, self.icon_client_logo.On)
            self.image_client_logo.setPixmap(pixmap_client_logo)

            # disconnect every gym logo signal
            try:
                self.image_client_logo.disconnect(self.logo_cluster3_clicked)
            except Exception as e:
                if e.args[0] == "'method' object is not connected":
                    pass
                else:
                    self.logger.exception("disconnecting cluster 3 signal", e)
            try:
                self.image_client_logo.disconnect(self.logo_cluster4_clicked)
            except Exception as e:
                if e.args[0] == "'method' object is not connected":
                    pass
                else:
                    self.logger.exception("disconnecting cluster 4 signal", e)
            try:
                self.image_client_logo.disconnect(self.logo_cluster5_clicked)
            except Exception as e:
                if e.args[0] == "'method' object is not connected":
                    pass
                else:
                    self.logger.exception("disconnecting cluster 5 signal", e)
            try:
                self.image_client_logo.disconnect(self.logo_cluster6_clicked)
            except Exception as e:
                if e.args[0] == "'method' object is not connected":
                    pass
                else:
                    self.logger.exception("disconnecting cluster 6 signal", e)

            if self.users_to_show == 4:
                # self.layouts_stacked_clusters[3].addWidget(self.image_client_logo)  # Not in this case
                if self.layouts_stacked_clusters[3].currentIndex() != Cluster.get_frame_id('enable'):
                    self.show_enable_frame(3)

            elif self.users_to_show == 12:
                self.layouts_stacked_clusters[3].addWidget(self.image_client_logo)
                if self.layouts_stacked_clusters[3].currentIndex() != Cluster.get_frame_id('acquisition'):
                    self.show_logo_frame(3)
                if self.layouts_stacked_clusters[4].currentIndex() != Cluster.get_frame_id('acquisition'):
                    self.show_enable_frame(4)
                if self.layouts_stacked_clusters[5].currentIndex() != Cluster.get_frame_id('acquisition'):
                    self.show_enable_frame(5)
                if self.layouts_stacked_clusters[6].currentIndex() != Cluster.get_frame_id('acquisition'):
                    self.show_enable_frame(6)
                self.image_client_logo.connect(self.logo_cluster3_clicked)

            elif self.users_to_show <= 25:  # 20 or 25
                self.layouts_stacked_clusters[4].addWidget(self.image_client_logo)
                if self.layouts_stacked_clusters[4].currentIndex() != Cluster.get_frame_id('acquisition'):
                    self.show_logo_frame(4)
                if self.layouts_stacked_clusters[3].currentIndex() != Cluster.get_frame_id('acquisition'):
                    self.show_enable_frame(3)
                if self.layouts_stacked_clusters[5].currentIndex() != Cluster.get_frame_id('acquisition'):
                    self.show_enable_frame(5)
                if self.layouts_stacked_clusters[6].currentIndex() != Cluster.get_frame_id('acquisition'):
                    self.show_enable_frame(6)
                self.image_client_logo.connect(self.logo_cluster4_clicked)

            elif self.users_to_show == 30:
                self.layouts_stacked_clusters[5].addWidget(self.image_client_logo)
                if self.layouts_stacked_clusters[5].currentIndex() != Cluster.get_frame_id('acquisition'):
                    self.show_logo_frame(5)
                if self.layouts_stacked_clusters[3].currentIndex() != Cluster.get_frame_id('acquisition'):
                    self.show_enable_frame(3)
                if self.layouts_stacked_clusters[4].currentIndex() != Cluster.get_frame_id('acquisition'):
                    self.show_enable_frame(4)
                if self.layouts_stacked_clusters[6].currentIndex() != Cluster.get_frame_id('acquisition'):
                    self.show_enable_frame(6)
                self.image_client_logo.connect(self.logo_cluster5_clicked)

            else:  # 35 or 42
                self.layouts_stacked_clusters[6].addWidget(self.image_client_logo)
                if self.layouts_stacked_clusters[6].currentIndex() != Cluster.get_frame_id('acquisition'):
                    self.show_logo_frame(6)
                if self.layouts_stacked_clusters[3].currentIndex() != Cluster.get_frame_id('acquisition'):
                    self.show_enable_frame(3)
                if self.layouts_stacked_clusters[4].currentIndex() != Cluster.get_frame_id('acquisition'):
                    self.show_enable_frame(4)
                if self.layouts_stacked_clusters[5].currentIndex() != Cluster.get_frame_id('acquisition'):
                    self.show_enable_frame(5)
                self.image_client_logo.connect(self.logo_cluster6_clicked)

        for id_cluster in range(self.users_to_show):
            self.frames_enable_cluster[id_cluster].setFixedSize(self.width_cluster, self.height_cluster)
            self.frames_selection_cluster[id_cluster].setFixedSize(self.width_cluster, self.height_cluster)
            self.frames_acquisition_cluster[id_cluster].setFixedSize(self.width_cluster, self.height_cluster)
            self.frames_disconnected_cluster[id_cluster].setFixedSize(self.width_cluster, self.height_cluster)
            self.frames_graph_cluster[id_cluster].setFixedSize(self.width_cluster, self.height_cluster)
            self.frames_stacked_clusters[id_cluster].setFixedSize(self.width_cluster, self.height_cluster)
            self.clusters_charts_images[id_cluster].setPixmap(pixmapG)

            # Resize icons
            if self.users_to_show == 4:
                icon_size = 70
                self.buttons_add_selected_user_cluster[id_cluster].setIconSize(QtCore.QSize(60, 60))
                self.buttons_remove_user_acquisition_cluster[id_cluster].setFixedHeight(80)
                self.buttons_remove_user_acquisition_cluster[id_cluster].setFixedWidth(80)
                self.buttons_add_selected_user_cluster[id_cluster].setMaximumHeight(65)
            elif self.users_to_show == 12:
                icon_size = 40
                self.buttons_add_selected_user_cluster[id_cluster].setIconSize(QtCore.QSize(40, 40))
                self.buttons_remove_user_acquisition_cluster[id_cluster].setFixedHeight(40)
                self.buttons_remove_user_acquisition_cluster[id_cluster].setFixedWidth(40)
                self.buttons_add_selected_user_cluster[id_cluster].setMaximumHeight(45)
            elif self.users_to_show == 20:
                icon_size = 36
                self.buttons_add_selected_user_cluster[id_cluster].setIconSize(QtCore.QSize(35, 35))
                self.buttons_remove_user_acquisition_cluster[id_cluster].setFixedHeight(30)
                self.buttons_remove_user_acquisition_cluster[id_cluster].setFixedWidth(30)
                self.buttons_add_selected_user_cluster[id_cluster].setMaximumHeight(40)
            elif self.users_to_show == 25:
                icon_size = 23
                self.buttons_add_selected_user_cluster[id_cluster].setIconSize(QtCore.QSize(35, 35))
                self.buttons_remove_user_acquisition_cluster[id_cluster].setFixedHeight(25)
                self.buttons_remove_user_acquisition_cluster[id_cluster].setFixedWidth(25)
                self.buttons_add_selected_user_cluster[id_cluster].setMaximumHeight(40)
            elif self.users_to_show == 30:
                icon_size = 20
                self.buttons_add_selected_user_cluster[id_cluster].setIconSize(QtCore.QSize(35, 35))
                self.buttons_remove_user_acquisition_cluster[id_cluster].setFixedHeight(25)
                self.buttons_remove_user_acquisition_cluster[id_cluster].setFixedWidth(25)
                self.buttons_add_selected_user_cluster[id_cluster].setMaximumHeight(40)
            elif self.users_to_show == 35:
                icon_size = 20
                self.buttons_add_selected_user_cluster[id_cluster].setIconSize(QtCore.QSize(35, 35))
                self.buttons_remove_user_acquisition_cluster[id_cluster].setFixedHeight(25)
                self.buttons_remove_user_acquisition_cluster[id_cluster].setFixedWidth(25)
                self.buttons_add_selected_user_cluster[id_cluster].setMaximumHeight(40)
            else:  # 42
                icon_size = 18
                self.buttons_add_selected_user_cluster[id_cluster].setIconSize(QtCore.QSize(28, 28))
                self.buttons_remove_user_acquisition_cluster[id_cluster].setFixedHeight(20)
                self.buttons_remove_user_acquisition_cluster[id_cluster].setFixedWidth(20)
                self.buttons_add_selected_user_cluster[id_cluster].setMaximumHeight(30)

            icon = QtGui.QIcon(images_path + 'acquisition_heart.png')
            pixmap = icon.pixmap(icon_size, icon_size, icon.Active, icon.On)
            self.images_hr_acquisition_cluster[id_cluster].setPixmap(pixmap)

            icon = QtGui.QIcon(images_path + 'acquisition_calories.png')
            pixmap = icon.pixmap(icon_size, icon_size, icon.Active, icon.On)
            self.images_calories_acquisition_cluster[id_cluster].setPixmap(pixmap)

            icon = TrainingType.get_steps_icon()
            pixmap = icon.pixmap(icon_size, icon_size, icon.Active, icon.On)
            self.images_steps_acquisition_cluster[id_cluster].setPixmap(pixmap)

    def start_session(self, session: Dict):
        try:
            # f = inspect.stack()[0][3]
            self.logger.debug('Session started')
            Utilities.set_ip_based_timezone()
            self.training_type_combobox.store_current_selection()
            self.active_training_type_combobox.setCurrentIndex(self.training_type_combobox.currentIndex())
            self.session_started_flag = True
            self.session_dict['status'] = 'running'

            # Prevent the auto reboot to reboot the system
            self.thread_schedule.update_session_status(True)
            self.stopAction.setEnabled(True)  # Enable stopMenu
            self.clear_group_action.setDisabled(True)
            self.load_group_action.setEnabled(False)  # Evita que cargen un grupo una vez iniciada la sesión
            self.startAction.setDisabled(True)
            self.show_session_button('stop')

            # Session time variables
            if session is None:
                self.session_timestamp = Utilities.get_current_time_formatted()  # La BDD no acepta milis
                self.session_time_duration = self.spin_session_duration.text().split(" ")
                self.session_time_duration = int(self.session_time_duration[0])
                self.session_time_finish = datetime.datetime.now() + datetime.timedelta(
                    minutes=self.session_time_duration)
                with DB(['sessions'], caller='gui') as db:
                    training_type = self.settings.get_training_type()
                    self.session_id = db.sessions.add_session(self.session_timestamp, self.session_time_duration,
                                                              training_type, self.activity_mode)
            else:
                self.session_id = session.get('id', None)
                self.session_timestamp = session.get('created', None)
                self.session_time_duration = session.get('duration', None)
                self.session_time_finish = self.session_timestamp + datetime.timedelta(
                    minutes=self.session_time_duration)

            self.update_time_and_progress()
            self.session_progress_timer.start(1000)

            if self.activity_mode == 'group':
                self.show_session_time_countdown()

            for cluster_id, cluster in enumerate(self.clusters):
                if cluster.frame['name'] == 'acquisition':
                    self.acquisition_cluster_set_session(cluster_id)
                    self.threads_acquisition[cluster_id].start_session()
            self.reset_frame_on_unused_clusters()
        except Exception as e:
            self.logger.exception("start_session", e)

    def process_finished_group_session(self):
        self.thread_connections.stop_connections()
        self.send_reports()
        self.session_finished_flag = True
        self.session_started_flag = False
        self.session_dict['status'] = 'finished'

        # block change view menu
        if self.action_show_cluster_logo.isEnabled():
            self.action_show_cluster_logo.setDisabled(True)

        self.update_grid_resize_actions(disable_all=True)
        self.reset_frame_on_unused_clusters()
        self.carousel.start_timer()
        self.show_session_button('reset')
        # self.upload_session()

        msg = _("Sending reports and disconnecting sensors \n\n {0} seconds.")
        show_dialog(msg, timeout=1)

    def reset_frame_on_unused_clusters(self):
        try:
            logo_cluster_index = logos_cluster_indexes[self.users_to_show]
            for cluster_id, cluster in enumerate(self.clusters):
                current_frame_name = cluster.frame['name']
                if current_frame_name != 'acquisition' and current_frame_name != 'enable':
                    if logo_cluster_index == cluster_id or cluster_id == 0:
                        self.show_logo_frame(cluster_id)
                    else:
                        self.show_enable_frame(cluster_id)
        except Exception as e:
            self.logger.exception("reset_frame_on_unused_clusters", e)

    def stop_session(self):
        # f = inspect.stack()[0][3]
        session_progress = self.progressBar.value()

        if session_progress == 100:
            self.logger.info('Session finished at 100%')
            self.process_finished_group_session()
        else:
            if self.session_started_flag:
                message_box = DialogMessage()
                message_box.set_text(_("Do you want to terminate the session?"))
                message_box.add_button(_("Cancel"), role='cancel')
                message_box.add_button(_("Terminate"), role='accept')
                decision = message_box.execute_message()
                if decision == _("Cancel"):
                    return

                # Stop the session and decide what to do with the clusters
                self.session_progress_timer.stop()
                message_box = DialogMessage()
                message_box.set_text(_("Do you want to show summary and send the session reports?"))
                message_box.add_button(_("Stop and clear"), role='cancel')
                message_box.add_button(_("Show and send"), role='accept')
                decision = message_box.execute_message()

                # Send and exit
                if decision == _("Show and send"):
                    self.logger.debug('Session stopped')  # By stop button
                    self.process_finished_group_session()

                # Not send
                else:
                    self.logger.debug('Session stopped without sending reports')  # By stop button
                    self.thread_connections.stop_connections()
                    self.update_all_session_reports_status('canceled')
                    self.session_finished_flag = True
                    self.session_started_flag = False
                    self.end_session()
                    self.reset_session()

                    msg = _("Disconnecting sensors \n\n {0} seconds")
                    show_dialog(msg, timeout=2)

        # Allow the auto reboot to reboot the system
        self.thread_schedule.update_session_status(False)
        self.enable_logo_and_grid_menus()

        # self.web_service.sync_session_async(self.active_users, self.session)

    def remove_all_clusters(self):
        for cluster_id, cluster in enumerate(self.clusters):
            if cluster.frame['name'] == 'acquisition':
                self.remove_cluster_acquisition(cluster_id)

    def logo_clicked(self, cluster_id):
        self.handler_button_enable_cluster(cluster_id)
        self.action_show_cluster_logo.setDisabled(False)

    def logo_cluster0_clicked(self):
        self.handler_button_enable_cluster(0)
        self.action_show_cluster_logo.setDisabled(False)

    def logo_cluster3_clicked(self):
        self.handler_button_enable_cluster(3)
        self.action_show_cluster_logo.setDisabled(False)

    def logo_cluster4_clicked(self):
        self.handler_button_enable_cluster(4)
        self.action_show_cluster_logo.setDisabled(False)

    def logo_cluster5_clicked(self):
        self.handler_button_enable_cluster(5)
        self.action_show_cluster_logo.setDisabled(False)

    def logo_cluster6_clicked(self):
        self.handler_button_enable_cluster(6)
        self.action_show_cluster_logo.setDisabled(False)

    # BOTON PARA QUITAR LA HORA DEL MAIN
    def removeHora_pressed(self):
        # TODO AGRGAR UN DIALOGO QUE CONFIRMA EL CAMBIO
        self.timer_clock.stop()
        self.show_selection_frame(3)
        # self.viewHourAction.setDisabled(False)
        self.action_show_cluster_clock.setDisabled(True)

    def show_enable_frame(self, cluster_id):
        try:
            # During cluster creation, all 43 cluster frames are created, TODO: Create dynamically
            if cluster_id < len(self.clusters):  # So we need to check if this cluster() exists
                self.clusters[cluster_id].set_frame('enable')

            self.layouts_stacked_clusters[cluster_id].setCurrentIndex(Cluster.get_frame_id('enable'))
        except Exception as e:
            self.logger.exception("Showing enable frame. Cluster_id {}".format(cluster_id), e)

    def show_selection_frame(self, cluster_id):
        self.clusters[cluster_id].set_frame('selection')
        self.layouts_stacked_clusters[cluster_id].setCurrentIndex(Cluster.get_frame_id('selection'))
        self.comboboxes_select_user_cluster[cluster_id].setFocus()

    def show_acquisition_frame(self, cluster_id):
        self.logger.debug("Switching cluster {} to Acquisition Frame".format(cluster_id))
        self.clusters[cluster_id].set_frame('acquisition')
        self.layouts_stacked_clusters[cluster_id].setCurrentIndex(Cluster.get_frame_id('acquisition'))
        self.process_events()

    def show_disconnected_frame(self, cluster_id):
        self.clusters[cluster_id].set_frame('disconnected')
        self.layouts_stacked_clusters[cluster_id].setCurrentIndex(Cluster.get_frame_id('disconnected'))

    def show_logo_frame(self, cluster_id):
        self.clusters[cluster_id].set_frame('logo')
        self.layouts_stacked_clusters[cluster_id].setCurrentIndex(Cluster.get_frame_id('logo'))

    def all_plot_finished(self):
        for thread in self.threads_plot:
            try:
                if thread.thread.finished() is not True:
                    self.logger.debug('Pending thread {}'.format(thread))
                    return False
            except:
                continue
        return True

    def all_thread_acquisition_finished(self):
        for cluster in self.clusters:
            if cluster.cluster['status'] != 'finished':
                return False
        return True

    def handle_dead_acquisition_thread(self, id_cluster):
        # f = inspect.stack()[0][3]
        # self.last_action = f

        self.logger.warning("Hnd cluster {} dead thread".format(id_cluster))
        # self.handle_acquisition_status(id_cluster, 'stalled')

    ###################################################################################################################
    # Acquisition signals handlers
    ###################################################################################################################

    def handle_acquisition_status(self, cluster_id, status, data=None):
        try:
            # f = inspect.stack()[0][3]
            # # self.last_action = f
            # self.logger.debug("F: {} id: {} status: {} call: {}".format(f, cluster_id, status, inspect.stack()[1][3]))
            self.clusters[cluster_id].sensor['status'] = status
            if status == "disconnected":
                if self.discrete_mode is False:
                    # self.labels_hr_acquisition_cluster[cluster_id].setPalette(self.paletteTextWhite)
                    self.acquisition_frame_hr_label[cluster_id].setText("0 lpm")
                    self.images_disconnected_acquisition_cluster[cluster_id].setPixmap(self.pixmap_disconnected_true)
                self.labels_interface_acquisition_cluster[cluster_id].setText(" " + " ")
                self.clusters[cluster_id].sensor['hci'] = None

            elif status == "connecting":
                self.clusters[cluster_id].sensor['hci'] = data
                if not self.discrete_mode:
                    self.images_disconnected_acquisition_cluster[cluster_id].setPixmap(self.pixmap_connected_true)
                    self.labels_interface_acquisition_cluster[cluster_id].setText("{}".format(data))
                current_text = self.acquisition_frame_effort_label[cluster_id].text()  # type: str
                if current_text.startswith(_("wear")):
                    current_text += "."
                    self.acquisition_frame_effort_label[cluster_id].setText(current_text)

            elif status == "liberated":
                self.clusters[cluster_id].sensor['hci'] = None
                if not self.discrete_mode:
                    self.labels_interface_acquisition_cluster[cluster_id].setText("L" + " ")

            elif status == "acquiring":
                self.clusters[cluster_id].sensor['p_connected'] = data
                self.clusters[cluster_id].sensor['contact_ok'] = True
                # if not self.discrete_mode:
                #   interface = self.clusters[cluster_id]['hci']
                # self.labels_effort_acquisition_cluster[cluster_id].setFont(self.font_effort)
                pass

            elif status == "wear":
                # self.labels_name_acquisition_cluster[cluster_id].setPalette(self.paletteTextWhite)
                # self.labels_effort_acquisition_cluster[cluster_id].setPalette(self.paletteTextWhite)
                self.acquisition_frame_effort_label[cluster_id].setFont(self.font_wear)
                self.acquisition_frame_effort_label[cluster_id].setText(_("wear"))
                # self.labels_hr_acquisition_cluster[cluster_id].setPalette(self.paletteTextWhite)

                if not self.discrete_mode:
                    self.acquisition_frame_hr_label[cluster_id].setText("0 lpm")
                    self.images_disconnected_acquisition_cluster[cluster_id].setPixmap(self.pixmap_disconnected_true)
            elif status == "gone":
                self.stop_cluster_acquisition_based_on_activity_mode(cluster_id)
        except Exception as e:
            self.logger.exception("Handling th acq {} status".format(cluster_id), e)

    def stop_cluster_acquisition_based_on_activity_mode(self, cluster_id):
        if self.activity_mode == 'group':
            self.remove_cluster_acquisition(cluster_id)
        else:
            self.stop_cluster_acquisition(cluster_id)
        self.fill_comboboxes()

    def handle_acquisition_bad_contact(self, cluster_id):
        try:
            # f = inspect.stack()[0][3]
            # self.last_action = f

            # self.logger.debug(caller_format_id.format(inspect.stack()[0][3], cluster_id, inspect.stack()[1][3]))
            # self.labels_name_acquisition_cluster[cluster_id].setPalette(self.paletteTextWhite)
            # self.labels_effort_acquisition_cluster[cluster_id].setPalette(self.paletteTextWhite)
            self.images_band_cluster[cluster_id].setPixmap(self.pixmap_band_true)
            self.images_disconnected_acquisition_cluster[cluster_id].setPixmap(self.pixmap_disconnected_false)
            self.clusters[cluster_id].sensor['contact_ok'] = True
        except Exception as e:
            self.logger.exception("Handling th acq {} bad contact".format(cluster_id), e)

    def handle_acquisition_new_measurement(self, cluster_id, hr, effort, calories):
        try:
            # f = inspect.stack()[0][3]
            # self.last_action = f

            # self.logger.debug(caller_format.format(inspect.stack()[0][3], cluster_id, inspect.stack()[1][3]))
            self.set_last_cluster_upd(cluster_id)
            # self.labels_name_acquisition_cluster[cluster_id].setPalette(self.paletteTextWhite)
            # self.labels_effort_acquisition_cluster[cluster_id].setPalette(self.paletteTextWhite)
            # self.labels_effort_acquisition_cluster[cluster_id].setFont()
            # self.labels_hr_acquisition_cluster[cluster_id].setPalette(self.paletteTextWhite)
            self.acquisition_frame_hr_label[cluster_id].setText(str(hr) + " ")
            self.acquisition_frame_effort_label[cluster_id].setFont(self.font_effort)
            self.acquisition_frame_effort_label[cluster_id].setText(str(effort) + "%")
            self.set_acquisition_frame_color(cluster_id, effort)
            self.images_band_cluster[cluster_id].setPixmap(self.pixmap_band_false)
            self.images_disconnected_acquisition_cluster[cluster_id].setPixmap(self.pixmap_disconnected_false)
            self.acquisition_frame_calories_label[cluster_id].setText(str(calories) + " ")
            if not self.discrete_mode:

                interface = self.clusters[cluster_id].sensor.get('hci', None)
                if interface is not None:
                    if self.dot_read_indicator[cluster_id] is True:
                        self.labels_interface_acquisition_cluster[cluster_id].setText("." + str(interface))
                        self.dot_read_indicator[cluster_id] = False
                    else:
                        self.labels_interface_acquisition_cluster[cluster_id].setText(" " + str(interface))
                        self.dot_read_indicator[cluster_id] = True
        except Exception as e:
            self.logger.exception("Handling th {} new measurement".format(cluster_id), e)

    def handle_acquisition_battery(self, cluster_id, battery_level):
        try:
            # f = inspect.stack()[0][3]
            # self.last_action = f

            # self.logger.debug(caller_format_id.format(inspect.stack()[0][3], cluster_id, inspect.stack()[1][3]))
            self.clusters[cluster_id].sensor['battery'] = battery_level
            if battery_level > 75:
                # self.images_battery_acquisition_cluster[id_cluster].setPixmap(self.pixmap_battery_full)
                self.images_battery_acquisition_cluster[cluster_id].setPixmap(self.pixmap_battery_hidden)
            elif battery_level > 70:
                # self.images_battery_acquisition_cluster[id_cluster].setPixmap(self.pixmap_battery_mid)
                self.images_battery_acquisition_cluster[cluster_id].setPixmap(self.pixmap_battery_hidden)
            elif battery_level > 45:
                self.images_battery_acquisition_cluster[cluster_id].setPixmap(self.pixmap_battery_low)
            elif battery_level > 0:
                self.images_battery_acquisition_cluster[cluster_id].setPixmap(self.pixmap_battery_out)
            else:
                self.images_battery_acquisition_cluster[cluster_id].setPixmap(self.pixmap_battery_none)
        except Exception as e:
            self.logger.exception("Setting battery level", e)

    def handle_acquisition_steps(self, cluster_id, steps):
        self.show_steps_fields(cluster_id)
        self.acquisition_frame_steps_label[cluster_id].setText("{} ".format(steps))

    def handle_acquisition_show_steps_field(self, cluster_id):
        self.show_steps_fields(cluster_id)

    def handle_acquisition_sensor_not_visible(self, cluster_id):
        # f = inspect.stack()[0][3]
        # self.last_action = f

        # self.logger.debug(caller_format_id.format(inspect.stack()[0][3], cluster_id, inspect.stack()[1][3]))
        self.acquisition_frame_hr_label[cluster_id].setText(str(0) + " ")
        self.acquisition_frame_effort_label[cluster_id].setText(_("wear"))
        self.acquisition_frame_effort_label[cluster_id].setFont(self.font_wear)
        self.set_acquisition_frame_color(cluster_id, effort=None)
        self.images_disconnected_acquisition_cluster[cluster_id].setPixmap(self.pixmap_disconnected_true)

    # TODO: is This handler is required now that we don't wait for this signal to switch to plot frame?
    # Yes! because it is used to show the selection frame if the session is not over
    def handle_acquisition_terminated(self, cluster_id, next_action=None):
        try:
            # f = inspect.stack()[0][3]
            # self.last_action = f

            self.logger.debug("Hnd cluster {} acq terminated. Next action: {}".format(cluster_id, next_action))
            # if next_action == "re-create":
            if self.clusters[cluster_id].frame['name'] == 'acquisition':
                if self.clusters[cluster_id].next_action == 'enable':
                    self.start_acquisition_cluster(cluster_id)
            else:
                if not self.discrete_mode:
                    self.images_disconnected_acquisition_cluster[cluster_id].setPixmap(self.pixmap_disconnected_true)
                self.images_band_cluster[cluster_id].setPixmap(self.pixmap_disconnected_false)

        except Exception as e:
            self.logger.exception("Handling terminated cluster {}".format(cluster_id), e)

    def update_time_and_progress(self):
        # self.logger.debug(caller_format.format(inspect.stack()[0][3], inspect.stack()[1][3]))
        if self.session_time_finish < datetime.datetime.now():
            self.session_progress_timer.stop()
            self.set_session_progress(100)
            self.stop_session()
            # TODO Check this issue
            try:
                self.session_time_left = datetime.time(second=0)
            except Exception as e:
                self.logger.exception("setting time left", e)

        else:
            self.session_time_left = self.session_time_finish - datetime.datetime.now()
            if self.session_time_left.total_seconds() > 3600:
                time_to_show = str(self.session_time_left)
                time_to_show = time_to_show[0:7]
            else:
                time_to_show = str(self.session_time_left)
                time_to_show = time_to_show[2:7]
            self.session_remaining_time_label.setText(time_to_show)
            remaining_percent = 100 - (self.session_time_left.total_seconds() * 100) / datetime.timedelta(
                minutes=self.session_time_duration).total_seconds()

            self.set_session_progress(remaining_percent)

    def update_clock_cluster(self):
        if self.timer_clock.isActive():
            current_time = QtCore.QDateTime.currentDateTime().toString('hh:mm')
            self.clock_cluster.setText(current_time)

    def enable_logo_and_grid_menus(self):
        self.update_show_logo_action()
        self.update_grid_resize_actions()

    def update_show_logo_action(self):
        try:
            users_to_show = self.users_to_show
            self.action_show_cluster_logo.setEnabled(False)

            if self.display_mode == "top_bar":
                return

            logo_frame_index = Cluster.get_frame_id('logo')
            if self.layouts_stacked_clusters[0].currentIndex() == logo_frame_index:
                return

            logo_cluster_index = self.logo_cluster_indexes[self.available_grid_sizes.index(users_to_show)]
            if self.layouts_stacked_clusters[logo_cluster_index].currentIndex() != logo_frame_index:
                return

            self.action_show_cluster_logo.setEnabled(True)
        except Exception as e:
            self.logger.exception("updating logo show action", e)

    def reset_time_and_progress(self):
        self.session_remaining_time_label.setText("")
        self.progressBar.setTextVisible(False)
        self.progressBar.setValue(0)

    def set_session_progress(self, progress):
        self.progressBar.setTextVisible(True)
        self.progressBar.setValue(progress)

    def individual_acquisition_cluster_set_session(self, cluster_id):
        self.session_finished_flag = True
        self.session_started_flag = True
        self.session_dict['status'] = 'running'
        self.thread_schedule.update_session_status(True)

        session_id = None

        # if previous session exists
        if self.clusters[cluster_id].user_session is not None:
            session_id = self.clusters[cluster_id].session['id']
            self.logger.debug("Recovered session_id {} cluster {}".format(session_id, cluster_id))

        # if cluster not recreated or failed to recover session
        if session_id is None:
            session_timestamp = Utilities.get_current_time_formatted()
            training_type = self.settings.get_training_type()
            with DB(['sessions'], caller='gui') as db:
                session_id = db.sessions.add_session(session_timestamp, None, training_type, self.activity_mode)
                self.logger.test("session_id: {}".format(session_id))
            self.clusters[cluster_id].set_session(session_id)
            self.clusters[cluster_id].create_user_session()
            self.session_solo_mode[cluster_id] = session_id

    def start_acquisition_cluster_connection(self, cluster_id):
        thread = self.threads_acquisition[cluster_id]
        cluster = self.clusters[cluster_id]
        self.thread_connections.add_cluster(cluster, thread)

    def start_acquisition_cluster_verified(self, cluster_id):
        try:
            if self.create_acquisition_thread(cluster_id):
                self.thread_acquisition_connect_signals(cluster_id)
                self.thread_acquisition_start(cluster_id)
                self.fill_acquisition_frame_initial_values(cluster_id)
                self.show_acquisition_frame(cluster_id)
                self.start_acquisition_cluster_connection(cluster_id)

                if self.activity_mode == "solo":
                    self.individual_acquisition_cluster_set_session(cluster_id)
                    self.threads_acquisition[cluster_id].start_session()
                elif self.activity_mode == "group" and self.session_started_flag:
                    self.acquisition_cluster_set_session(cluster_id)
                    self.threads_acquisition[cluster_id].start_session()
            else:
                self.remove_cluster_acquisition(cluster_id)  # TODO: check if level
                self.logger.warning("Cluster {} failed to start thread".format(cluster_id))
        except Exception as e:
            self.logger.exception("Starting thread verified cluster_id: {}".format(cluster_id), e)

    def fill_acquisition_frame_initial_values(self, cluster_id):
        self.clear_acquisition_frame(cluster_id)
        self.fill_acquisition_cluster_pseudonym(cluster_id)
        self.fill_acquisition_cluster_sensor(cluster_id)
        self.images_battery_acquisition_cluster[cluster_id].setPixmap(self.pixmap_battery_out)
        self.images_signal_acquisition_cluster[cluster_id].setPixmap(self.pixmap_signal_hidden)

    def fill_acquisition_cluster_pseudonym(self, cluster_id):
        self.acquisition_frame_pseudonym_label[cluster_id].setText(self.clusters[cluster_id].user['pseudonym'])

    def fill_acquisition_cluster_sensor(self, cluster_id):
        owner_type = self.clusters[cluster_id].sensor['owner_type']
        if owner_type == 'center':
            sensor_number = self.clusters[cluster_id].sensor['sensor_id_center']
        else:
            sensor_number = self.clusters[cluster_id].sensor['own_id']
        self.acquisition_frame_sensor_label[cluster_id].setText(" {} {}".
                                                                format(_("Sensor"), str(sensor_number)))

    def reject_user_sensor_selected(self, cluster_id: int, object_rejected: str, cluster_were_found: int):
        try:
            if object_rejected == 'user':
                self.clear_combobox_selection(self.comboboxes_select_user_cluster[cluster_id])
            else:
                self.clear_combobox_selection(self.comboboxes_select_sensor_cluster[cluster_id])
            self.logger.warning("Can't add the cluster: {}, repeated {} on cluster: {}"
                                .format(cluster_id, object_rejected, cluster_were_found))
            msg = _("This {} is already in use".format(object_rejected))
            show_dialog(msg, timeout=1, accept_button_text=_('Ok'))
        except Exception as e:
            self.logger.exception("reject_user_sensor_selected. Cluster {}".format(cluster_id), e)

    def thread_acquisition_start(self, cluster_id):
        # start is executed appart because we need to connect signals first
        try:
            self.threads_acquisition[cluster_id].start()
        except Exception as e:
            self.logger.exception("thread_acquisition_start: {}".format(cluster_id), e)

    def thread_acquisition_connect_signals(self, cluster_id):
        # Signals
        try:
            thread = self.threads_acquisition[cluster_id].thread
            thread.status.connect(self.handle_acquisition_status, QtCore.Qt.QueuedConnection)
            thread.upd_hr_derivatives.connect(self.handle_acquisition_new_measurement, QtCore.Qt.QueuedConnection)
            thread.bad_contact.connect(self.handle_acquisition_bad_contact, QtCore.Qt.QueuedConnection)
            thread.upd_battery.connect(self.handle_acquisition_battery, QtCore.Qt.QueuedConnection)
            thread.show_steps_field.connect(self.handle_acquisition_show_steps_field, QtCore.Qt.QueuedConnection)
            thread.upd_steps.connect(self.handle_acquisition_steps, QtCore.Qt.QueuedConnection)
            # thread.sensor_not_visible.connect(self.handle_acquisition_sensor_not_visible, QtCore.Qt.QueuedConnection)
            thread.terminated.connect(self.handle_acquisition_terminated, QtCore.Qt.QueuedConnection)
        except Exception as e:
            self.logger.exception("Connecting cluster {} thread signals".format(cluster_id), e)

    def terminate_th_acquisition_thread(self, cluster_id, next_action):
        try:
            self.logger.debug(caller_format_id.format(inspect.stack()[0][3], cluster_id, inspect.stack()[1][3]))
            self.thread_connections.remove_cluster(self.clusters[cluster_id])
        except Exception as e:
            self.logger.exception("Requesting thread {} termination".format(cluster_id), e)

    def terminate_th_acquisition(self, cluster_id, next_action):
        try:
            thread = Thread(target=self.terminate_th_acquisition_thread, args=(cluster_id, next_action))
            thread.start()
        except Exception as e:
            self.logger.exception('Terminating Thread {}'.format(cluster_id), e)

    def enable_session_start_button_if_required(self):
        for cluster in self.clusters:
            if cluster.frame['name'] == 'acquisition':
                enable = True
                break
        else:
            enable = False

        self.enable_session_start_button(enabled=enable)

    def enable_session_start_button(self, enabled=True):
        if self.activity_mode == 'group':
            self.logger.debug("enable_session_start_button: {}".format(enabled))
            self.startAction.setEnabled(enabled)
            self.start_session_button.setEnabled(enabled)

    def edit_user_cluster(self, cluster_id, next_action=None):
        try:
            # f = inspect.stack()[0][3]
            # # self.last_action = f

            caller = inspect.stack()[1][3]
            sensor_info = self.clusters[cluster_id].sensor
            self.logger.debug("Fcn: {} cluster: {} caller: {}".format(inspect.stack()[0][3], cluster_id, caller))

            if caller == 'mousePressEvent':
                caller = 'human'
                self.clusters[cluster_id].cluster['status'] = 'edited'
            else:
                caller = 'killer'

            if self.threads_acquisition[cluster_id] is not None:
                # self.thread_connections.stop_cluster(cluster_id, next_action)
                self.thread_connections.remove_cluster(self.clusters[cluster_id])

            if caller == 'human':
                self.show_selection_frame(cluster_id)
                with DB(['sessions'], caller='gui') as db:
                    db.sessions.set_session_status(self.session_solo_mode[cluster_id], 'edited')
                self.set_user_session_status(cluster_id, 'edited')

            self.images_battery_acquisition_cluster[cluster_id].setPixmap(self.pixmap_battery_out)
            self.images_signal_acquisition_cluster[cluster_id].setPixmap(self.pixmap_signal_hidden)
            if self.clusters[cluster_id].get_cluster_added_type() == 'auto':
                self.remove_auto_added_cluster(cluster_id, "GUI", _("Modify"), sensor_info)

        except Exception as e:
            self.logger.exception("Removing cluster {}".format(cluster_id), e)

        # self.enable_disable_timer_cluster_killer()

    def actions_after_adding_removing_cluster(self):
        try:
            self.fill_comboboxes(None)
            self.save_group_menu(last_group=True)

            if self.activity_mode == 'group':
                self.enable_session_start_button_if_required()
        except Exception as e:
            self.logger.exception("actions_after_adding_removing_cluster", e)

    def remove_cluster_acquisition(self, cluster_id, caller_=None):
        """ For group mode """
        try:
            selection = None
            caller = Utilities.get_caller()
            if caller == ExtendedQLabel.mousePressEvent.__name__:
                msg = _("Do you want to modify or remove cluster?")
                message_box = DialogMessage()
                message_box.set_text(msg)
                message_box.add_button(_("Remove"), 'accept')
                message_box.add_button(_("Modify"), 'cancel')
                selection = message_box.execute_message()
                caller_ = "GUI"

            sensor_info = self.clusters[cluster_id].sensor

            self.buttons_remove_user_acquisition_cluster[cluster_id].setEnabled(False)
            self.terminate_th_acquisition(cluster_id, next_action=None)

            if self.session_started_flag:
                self.show_enable_frame(cluster_id)
            else:
                self.show_selection_frame(cluster_id)

            if caller == ExtendedQLabel.mousePressEvent.__name__:
                self.actions_after_adding_removing_cluster()
            elif caller == self.show_cluster_logos.__name__:
                self.actions_after_adding_removing_cluster()
            elif caller == self.start_acquisition_cluster.__name__:
                self.actions_after_adding_removing_cluster()

            # Forget/Clear the elements after re-filling it's comboboxes
            self.clusters[cluster_id].clear()
            self.buttons_remove_user_acquisition_cluster[cluster_id].setEnabled(True)

            if caller_ is not None:
                self.remove_auto_added_cluster(cluster_id, caller_, selection, sensor_info)

        except Exception as e:
            self.logger.exception("Removing cluster {}".format(cluster_id), e)

        # self.enable_disable_timer_cluster_killer()

    def remove_auto_added_cluster(self, cluster_id, caller_, selection, sensor_info):
        if caller_ in ("th_connections", "GUI"):
            if caller_ == "th_connections":
                self.logger.warning("Removing auto added cluster {} due to gray zone timeout".format(cluster_id))
            elif caller_ == "GUI":
                if self.clusters[cluster_id].get_cluster_added_type() == 'auto':
                    self.logger.warning("Removing auto added cluster {} due to manual ban".format(cluster_id))
                    sensor_dict = {"sensor_name": sensor_info["name"],
                                   "sensor_mac": sensor_info["mac"],
                                   "cluster_id": cluster_id,
                                   "ban_datetime": datetime.datetime.now()}
                    self.thread_connections.thread.add_banned_sensor(sensor_dict)
                if selection == _("Modify"):
                    return
            self.comboboxes_select_user_cluster[cluster_id].setCurrentIndex(0)
            self.comboboxes_select_sensor_cluster[cluster_id].setCurrentIndex(0)
            self.show_enable_frame(cluster_id)
        self.clusters[cluster_id].set_cluster_added_type("manual")

    def stop_cluster_acquisition(self, cluster_id):
        """ For individual activity mode"""
        try:
            # f = inspect.stack()[0][3]
            # self.last_action = f
            sensor_info = self.clusters[cluster_id].sensor
            self.buttons_remove_user_acquisition_cluster[cluster_id].setEnabled(False)
            self.buttons_edit_user_acquisition_cluster[cluster_id].setEnabled(False)

            # self.logger.debug(caller_format_id.format(inspect.stack()[0][3], cluster_id, caller))

            # Currently the only caller is mouseEvent
            # if caller == ExtendedQLabel.mousePressEvent.__name__:
            self.clusters[cluster_id].next_action = 'plot'
            self.preset_and_show_chart_frame(cluster_id)
            self.terminate_th_acquisition(cluster_id, next_action=None)
            self.send_report(cluster_id)

            self.clusters[cluster_id].clear()
            self.fill_comboboxes()

            self.session_solo_mode[cluster_id] = None

            self.buttons_remove_user_acquisition_cluster[cluster_id].setEnabled(True)
            self.buttons_edit_user_acquisition_cluster[cluster_id].setEnabled(True)
            if self.clusters[cluster_id].get_cluster_added_type() == 'auto':
                self.remove_auto_added_cluster(cluster_id, "GUI", _("Modify"), sensor_info)
        except Exception as e:
            self.logger.exception("Stopping user cluster", e)

    def handler_button_enable_cluster(self, cluster_id):
        if self.activity_mode == "group":
            allowed_session_status = ['idle', 'running']
            if self.session_dict['status'] in allowed_session_status:
                self.show_selection_frame(cluster_id)
        elif self.activity_mode == "solo":
            self.show_selection_frame(cluster_id)

    def fill_comboboxes_async(self, group_name=None):
        try:
            # f = inspect.stack()[0][3]
            # # self.last_action = f

            # self.logger.debug(caller_format_id.format(inspect.stack()[0][3], cluster_id, inspect.stack()[1][3]))
            thread = Thread(target=self.fill_comboboxes_thread, args=(group_name,))
            thread.start()
        except Exception as e:
            self.logger.exception('Fill comboboxes', e)

    def set_comboboxes_group_selection(self, group_name, users_id_list, sensors_id_list):
        try:
            if group_name is 'empty':  # True is only for the initial group
                for cluster_id in range(self.users_to_show):
                    self.comboboxes_select_user_cluster[cluster_id].setCurrentIndex(0)
                    self.comboboxes_select_sensor_cluster[cluster_id].setCurrentIndex(0)
                return  # For empty group no selection is required

            if group_name is None:
                return

            with DB(['groups'], caller='gui') as db:
                group_id = db.groups.get_group_id(group_name)

            if group_id is None:
                self.logger.warning("Group {} doesn't exists".format(group_name))
                return

            for cluster_id in range(self.users_to_show):
                with DB(['groups'], caller='gui') as db:
                    cluster_saved = db.groups.get_group_cluster(group_id, cluster_id)

                if cluster_saved is None:
                    continue

                group_user_id = cluster_saved[0]
                combo = self.comboboxes_select_user_cluster[cluster_id]
                ComboBoxesTools.set_combo_selection(combo, users_id_list, group_user_id)

                group_sensor_id = cluster_saved[1]
                combo = self.comboboxes_select_sensor_cluster[cluster_id]
                ComboBoxesTools.set_combo_selection(combo, sensors_id_list, group_sensor_id)

                if group_sensor_id is not None or group_user_id is not None:
                    self.show_selection_frame(cluster_id)

                # TODO: Move this, this is not a combobox stuff
                else:
                    if not self.display_mode == "top_bar":
                        logo_cluster_index = self.logo_cluster_indexes[
                            self.available_grid_sizes.index(self.users_to_show)]
                        if cluster_id == 0 or cluster_id == logo_cluster_index:
                            self.show_logo_frame(cluster_id)
                        else:
                            self.show_enable_frame(cluster_id)
        except Exception as e:
            self.logger.exception('Fill comboboxes group', e)

    def fill_comboboxes(self, group_name=None):
        try:
            users_on_use, sensors_on_use = self.get_lists_of_users_and_sensors_on_use()
            users_combobox_list, users_id_list = ComboBoxesTools.generate_users_list(users_on_use)
            sensors_combobox_list, sensors_id_list = ComboBoxesTools.generate_sensors_list(sensors_on_use)

            threads = []
            user_last_selections = [None] * self.users_to_show
            sensor_last_selections = [None] * self.users_to_show

            # Loop through expected clusters, because self.clusters[i] could be None
            for cluster_id in range(self.users_to_show):
                current_frame = self.clusters[cluster_id].frame['name']
                if current_frame == 'acquisition' or current_frame == 'chart':
                    continue  # Skip clusters on acquisition mode to save time

                # Save current selections
                user_last_selections[cluster_id] = self.comboboxes_select_user_cluster[cluster_id].currentData()
                sensor_last_selections[cluster_id] = self.comboboxes_select_sensor_cluster[cluster_id].currentData()

                # TODO: this is slow for long lists of users, improve, threads caused an async responses
                # fill_combo(self.comboboxes_select_user_cluster[cluster_id], users_combobox_list)
                # fill_combo(self.comboboxes_select_sensor_cluster[cluster_id], sensors_combobox_list)

                t = Thread(target=ComboBoxesTools.fill_combo,
                           args=(self.comboboxes_select_user_cluster[cluster_id], users_combobox_list))
                t.start()
                threads.append(t)

                t = Thread(target=ComboBoxesTools.fill_combo,
                           args=(self.comboboxes_select_sensor_cluster[cluster_id], sensors_combobox_list))
                t.start()
                threads.append(t)

            for t in threads:
                t.join()

            # If loading a group
            if group_name is not None:
                self.set_comboboxes_group_selection(group_name, users_id_list, sensors_id_list)

            # No group loaded not even the empty, means refilling after adding or removing to session
            else:
                for cluster_id in range(self.users_to_show):
                    current_frame = self.clusters[cluster_id].frame['name']
                    if current_frame == 'acquisition' or current_frame == 'chart':
                        continue

                    # The combobox is filled, with it's previous selection
                    combo = self.comboboxes_select_user_cluster[cluster_id]
                    id_to_set = user_last_selections[cluster_id]
                    ComboBoxesTools.set_combo_selection(combo, users_id_list, id_to_set)

                    combo = self.comboboxes_select_sensor_cluster[cluster_id]
                    id_to_set = sensor_last_selections[cluster_id]
                    ComboBoxesTools.set_combo_selection(combo, sensors_id_list, id_to_set)

        except Exception as e:
            self.logger.exception("Filling comboboxes", e)

    def fill_one_combobox(self, cluster_id, user_id, sensor_id):
        try:
            if cluster_id is None:
                self.logger.error("add_cluster_for_discovered_sensor got None cluster_id")
                return
            if user_id is None:
                self.logger.error("add_cluster_for_discovered_sensor got None user_id")
                return
            if sensor_id is None:
                self.logger.error("add_cluster_for_discovered_sensor got None user_id")
                return

            users_on_use, sensors_on_use = self.get_lists_of_users_and_sensors_on_use()
            users_combobox_list, users_id_list = ComboBoxesTools.generate_users_list(users_on_use)
            sensors_combobox_list, sensors_id_list = ComboBoxesTools.generate_sensors_list(sensors_on_use)

            combo = self.comboboxes_select_user_cluster[cluster_id]
            if ComboBoxesTools.set_combo_selection(combo, users_id_list, user_id) is not True:
                self.logger.error("Failed to fill user: {} on cluster: {}".format(user_id, cluster_id))
                return

            combo = self.comboboxes_select_sensor_cluster[cluster_id]
            if ComboBoxesTools.set_combo_selection(combo, sensors_id_list, sensor_id) is not True:
                self.logger.error("Failed to fill sensor: {} on cluster: {}".format(user_id, cluster_id))
                return

            return True
        except Exception as e:
            self.logger.exception("Filling comboboxes", e)

    def advanced_options_menu_change_status(self):
        if self.versionMenu.isEnabled():
            self.versionMenu.setDisabled(True)
        else:
            self.versionMenu.setEnabled(True)

    def exit_app(self):
        message_box = DialogMessage("warning")
        message_box.set_title(_("Exit"))
        message_box.set_text(_("Exit RookMotion?"))
        message_box.set_informative_text(_("This will close the application and go to desktop"))
        message_box.set_icon("question")
        message_box.add_button(_("Exit"), role='accept')
        message_box.add_button(_("Cancel"), role='cancel')

        decision = message_box.execute_message()

        if decision == _("Cancel"):
            return None
        else:

            if self.activity_mode == "group":
                if self.session_started_flag:
                    self.stop_session()

            self.terminate_all_clusters()

            self.thread_connections.terminate()
            self.thread_schedule.terminate()
            # self.db.disconnect()
            self.close()

    def restart_system(self):
        message_box = DialogMessage()
        message_box.set_title(_("Reboot"))
        message_box.set_text(_("Reboot system?"))
        message_box.set_informative_text(_("This will take a few minutes."))
        message_box.set_icon("question")
        message_box.add_button(_("Reboot"), role='accept')
        message_box.add_button(_("Cancel"), role='cancel')

        decision = message_box.execute_message()

        if decision == _("Cancel"):
            return None
        else:
            if self.activity_mode == "group":
                if self.session_started_flag:
                    self.stop_session()

            self.terminate_all_clusters()
            self.reboot_system()

    def reboot_system(self):
        subprocess.call("reboot", shell=True)

    def terminate_all_clusters(self):
        for cluster_id, cluster in enumerate(self.clusters):
            if cluster.frame['name'] == 'acquisition':
                self.thread_connections.remove_cluster(cluster)

    def turn_off_system(self):
        message_box = DialogMessage()
        message_box.set_title(_("Shut down"))
        message_box.set_text(_("Shut down the system"))
        message_box.set_informative_text(
            _("If you shut down the system you will have to switch the power button to resume it"))
        message_box.set_icon("question")
        message_box.add_button(_("Shut down"), role='accept')
        message_box.add_button(_("Cancel"), role='cancel')

        decision = message_box.execute_message()

        if decision == _("Cancel"):
            return None
        else:
            if self.activity_mode == "group":
                if self.session_started_flag:
                    self.stop_session()

            self.terminate_all_clusters()
            subprocess.call("poweroff", shell=True)

    def new_user_menu(self):
        dialog = DialogUser(self)
        dialog.format_dialog(_("Add new user"), self.font_dialogs)
        if dialog.exec_() == QtWidgets.QDialog.Accepted:
            show_dialog(_("Uploading user to server"), 1)
            name = dialog.get_name()
            last_name = dialog.get_last_name()
            pseudonym = dialog.get_pseudonym()
            sex = dialog.get_sex()
            birthdate = dialog.get_birthdate()
            height = dialog.get_height()
            weight = dialog.get_weight()
            email = dialog.get_email()
            hr_bas = dialog.get_hr_bas()

            with DB(['users'], caller='gui') as db:
                user_id = db.users.add_user(name, last_name[0], last_name[1], pseudonym, sex, birthdate, email)
                db.users.set_user_variable(user_id, 'hr_bas', hr_bas)
                db.users.set_user_variable(user_id, 'height', height)
                db.users.set_user_variable(user_id, 'weight', weight)
            if user_id is not None:
                # self.thread_welcome_email = ThreadEmailWelcome(user_id)
                self.fill_comboboxes(None)  # Update comboboxes
                try:
                    if self.network.test_internet():
                        with WS() as web_service:
                            if web_service.system.server_is_alive():
                                user_uuid = web_service.users.post_user(user_id)
                                if user_uuid is not None:
                                    show_dialog(_("User uploaded correctly"), 1, accept_button_text=_("Ok"))
                                else:
                                    show_dialog(_("Error uploading user"), 1, accept_button_text=_("Ok"))
                    else:
                        show_dialog(_("No internet available\n Uploading later"), 1, accept_button_text=_("Ok"))

                except Exception as e:
                    self.logger.exception("Posting user", e)
            else:
                self.logger.warning("User not added")

        dialog.deleteLater()

    # noinspection PyBroadException
    def edit_user_menu(self):
        try:
            flag_exit = False
            while flag_exit is not True:  # Simplest way to have a selection dialog open after each iteration
                dialog_selection = DialogUserSelection(self)
                dialog_selection.set_dialog(self.font_dialogs, selection_type="edit_user")
                if dialog_selection.exec_() == QtWidgets.QDialog.Accepted:
                    dialog = DialogUser(self)
                    try:
                        user_id = dialog_selection.user_selected_id
                        dialog.format_dialog(_("Edit user"), self.font_dialogs)
                        dialog.set_user_dialog(user_id)
                        dialog_selection.deleteLater()
                        dialog.setFont(self.font_messages)
                        if dialog.exec_() == QtWidgets.QDialog.Accepted:
                            delete_dialog(dialog_selection)
                            name = dialog.get_name()
                            pseudonym = dialog.get_pseudonym()
                            sex = dialog.get_sex()
                            birthdate = dialog.get_birthdate()
                            height = dialog.get_height()
                            weight = dialog.get_weight()
                            email = dialog.get_email()
                            hr_bas = dialog.get_hr_bas()
                            with DB(['users'], caller='gui') as db:
                                db.users.update_user_info(user_id, name, pseudonym, sex, birthdate, email)
                                db.users.set_user_variable(user_id, 'hr_bas', hr_bas)
                                db.users.set_user_variable(user_id, 'height', height)
                                db.users.set_user_variable(user_id, 'weight', weight)

                            self.fill_comboboxes(None)
                            # self.thread_welcome_email = ThreadEmailWelcome(user_id)
                            delete_dialog(dialog)
                        else:
                            delete_dialog(dialog)
                            # dialog.deleteLater()
                    except Exception as e:
                        self.logger.exception("editing user (the user doesn't exists)", e)
                    delete_dialog(dialog_selection)
                else:
                    delete_dialog(dialog_selection)
                    flag_exit = True

            self.fill_comboboxes(None)
        except Exception as e:
            self.logger.exception("edit_user_menu", e)

    # noinspection PyBroadException
    def resend_email(self):
        flag_exit = False
        while flag_exit is not True:  # Simplest way to have a selection dialog open after each iteration
            dialog_selection = DialogUserSelection(self)
            dialog_selection.set_dialog(self.font_dialogs)
            dialog_selection.setFont(self.font_messages)
            """dialog = DialogCalendar(self)
            if dialog_selection.exec_() == QtWidgets.QDialog.Accepted:
                id_user = dialog_selection.user_selected_id
                dialog.format_dialog(_("Re-send email report"), self.font_dialogs)
                dialog.set_user(id_user)
                dialog.exec_()
                delete_dialog(dialog)
            else:
                flag_exit = True"""
            delete_dialog(dialog_selection)

    def get_sensor_info(self):
        flag_exit = False
        while flag_exit is not True:  # Simplest way to have a selection dialog open after each iteration
            dialog = DialogSensorInfo(self)
            title = _("Sensor information")
            dialog.format_dialog(title, self.font_dialogs)
            if dialog.exec_() == QtWidgets.QDialog.Accepted:
                flag_exit = True
            else:
                flag_exit = True
            dialog.deleteLater()

    def save_group_menu(self, last_group=False):
        users_list = [None] * self.users_to_show  # type: List[int]
        sensors_list = [None] * self.users_to_show  # type: List[int]

        def check_if_not_empty():
            for i_cluster in range(self.users_to_show):  # Check if empty
                selected_user_id = self.comboboxes_select_user_cluster[i_cluster].currentData()

                if selected_user_id is not None or self.clusters[i_cluster].user['id'] is not None:
                    return True
                elif i_cluster == self.users_to_show - 1:
                    if last_group is not True:
                        message_box = DialogMessage()
                        message_box.set_title(_("Empty group"))
                        message_box.set_text(_("Cannot save an empty group"))
                        message_box.set_informative_text(_("Select at least one user to save the group"))
                        message_box.add_button(_("Ok"), role='accept')

                        decision = message_box.execute_message()
                        if decision == _("Ok"):
                            return False
                    return False

        def get_users_and_sensors():
            for i_cluster in range(self.users_to_show):
                if self.clusters[i_cluster].user['id'] is None:  # user not in acq mode
                    try:
                        user_text = self.comboboxes_select_user_cluster[i_cluster].currentText()
                        user_ = self.comboboxes_select_user_cluster[i_cluster].currentData()
                        sensor_text = self.comboboxes_select_sensor_cluster[i_cluster].currentText()
                        sensor_ = self.comboboxes_select_sensor_cluster[i_cluster].currentData()
                        if user_ is not None and user_text != '':
                            users_list[i_cluster] = int(user_)
                        if sensor_ is not None and sensor_text != '':
                            sensors_list[i_cluster] = int(sensor_)
                    except Exception as e:
                        self.logger.exception("get sensors and users", e)
                else:
                    users_list[i_cluster] = self.clusters[i_cluster].user['id']
                    sensors_list[i_cluster] = self.clusters[i_cluster].sensor['id']

        # self.logger.debug(caller_format.format(inspect.stack()[0][3], inspect.stack()[1][3]))
        if check_if_not_empty():
            # If group is not empty
            get_users_and_sensors()
            if last_group is not True:
                dialog = DialogNewGroup(self)
                dialog.format_dialog(_("New group"), self.font_dialogs)

                if dialog.exec_() == QtWidgets.QDialog.Accepted:
                    group_name = dialog.get_group_name()
                    with DB(['groups'], caller='gui') as db:
                        if dialog.buttonOk.text() == _("Add"):
                            db.groups.add_group(group_name, users_list, sensors_list)
                        elif dialog.buttonOk.text() == _("Overwrite"):  # Modify group
                            db.groups.modify_group(group_name, users_list, sensors_list)
                        else:
                            self.logger.warning("Adding group has a clause out of scope")
                    # self.fill_comboboxes(group_name)
                # else:
                # self.fill_comboboxes(None)
                dialog.deleteLater()
            else:
                group_name = _("Last")
                with DB(['groups'], caller='gui') as db:
                    groups_names = db.groups.get_groups_names()
                    if group_name in groups_names:
                        db.groups.modify_group(group_name, users_list, sensors_list)
                    else:
                        db.groups.add_group(group_name, users_list, sensors_list)

    def get_lists_of_users_and_sensors_on_use(self):
        users_on_use = []
        sensors_on_use = []
        for cluster in self.clusters:
            try:

                if cluster.frame['name'] == 'acquisition':
                    user_id = cluster.user['id']
                    users_on_use.append(user_id)

                    sensor_id = cluster.sensor['id']
                    sensors_on_use.append(sensor_id)
            except Exception as e_:
                self.logger.exception("Getting cluster: {} used elements".format(cluster.cluster['id']), e_)

        return users_on_use, sensors_on_use

    def load_group_menu(self):
        self.logger.debug("Loading group")
        dialog = DialogGroupSelection(self)
        title = _("Load group")
        dialog.format_dialog(title, self.font_dialogs)
        if dialog.exec_() == QtWidgets.QDialog.Accepted:
            group_name = dialog.get_selected_group()
            self.load_group(group_name)
        dialog.deleteLater()

    def load_group(self, group_name):
        if group_name is not None:
            self.remove_all_clusters()
            self.fill_comboboxes(group_name)
            self.enable_all_clusters()
            self.enable_session_start_button_if_required()
            # self.disable_menu_show_logos()

    def disable_menu_show_logos(self):
        self.action_show_cluster_logo.setDisabled(False)

    def sync_db(self):
        msg = _("Please wait while data are synced \n\n This may take a few minutes")
        show_dialog(msg, timeout=1)

        if self.network.test_internet():
            with WS() as web_service:
                web_service.system.post_daq_ip(self.network.get_ip())
                web_service.users.sync_all(wait=True)
                web_service.sensors.sync_all(wait=True)
                web_service.sessions.sync_all(wait=False)

        self.fill_comboboxes(None)

    def configure_wifi(self):
        flag_exit = False
        while flag_exit is not True:  # Simplest way to have a selection dialog open after each iteration
            dialog = DialogWiFi(self)
            title = _("Configure WiFi")
            dialog.format_dialog(title, self.font_dialogs)
            if dialog.exec_() == QtWidgets.QDialog.Accepted:
                wifi_name = dialog.get_wifi_name()
                wif_password = dialog.get_wifi_password()

                file_path = "/etc/wpa_supplicant/wpa_supplicant.conf"
                text = []

                text.append("ctrl_interface=DIR=/var/run/wpa_supplicant GROUP=netdev")
                text.append("update_config=1")
                text.append("country=MX")
                text.append("")
                text.append("")
                text.append("network={")
                text.append("	ssid=" + '"' + "{}".format(wifi_name) + '"')
                text.append("	psk=" + '"' + "{}".format(wif_password) + '"')
                text.append("	key_mgmt=WPA-PSK")
                text.append("}")
                text.append("")
                try:
                    f = open(file_path, "a+")
                    f.seek(0)
                    new_file = len(f.read(10)) < 1
                    if not new_file:
                        text = text[4:]
                    for line in text:
                        f.write(line + '\r\n')
                    f.close()
                    try:
                        process = subprocess.Popen('wpa_cli -i wlan0 reconfigure', shell=True, stdout=subprocess.PIPE,
                                                   stderr=subprocess.STDOUT)
                        iterable_lines = iter(process.stdout.readline, b'')
                        for line in iterable_lines:
                            try:
                                line_strip = line.rstrip().decode('ascii')
                                if line_strip.startswith("OK"):
                                    message_box = DialogMessage()
                                    message_box.set_title(_("Saved successfully"))
                                    message_box.set_text(_("Saved successfully") + ": {}")

                                    message_box.set_timer(3)
                                    message_box.execute_message()
                                    ip = self.network.get_ip()
                                    self.handle_internet_test(ip, True)
                                else:
                                    message_box = DialogMessage()
                                    message_box.set_text(_("You need to restart: {}"))

                                    message_box.set_timer(3)
                                    message_box.add_button(_("Ok"), role='accept')
                                    decision = message_box.execute_message()

                                    if decision == _("Ok"):
                                        subprocess.call("reboot", shell=True)

                            except Exception as e:
                                self.logger.exception("re-setting wlan0", e)
                    except Exception as e:
                        self.logger.exception("Reconfiguring wlan", e)

                    flag_exit = True

                except Exception as e:
                    self.logger.exception("saving wifi settings", e)
                    flag_exit = True
            else:
                flag_exit = True
            dialog.deleteLater()

    def change_left_logo(self):
        title = _("Select the image logo")
        dialog = QtWidgets.QFileDialog()
        dialog.setViewMode(QtWidgets.QFileDialog.Detail)
        try:
            file_name = dialog.getOpenFileName(self, title, "/home/pi/Desktop", "Image Files (*.png)")
            if file_name[0] is not "":
                subprocess.call("cp -R " + file_name[0] + " /home/pi/RookMotion/settings/left_logo.png", shell=True)
                self.icon_left_logo = QtGui.QIcon('/home/pi/RookMotion/settings/logo_gim.png')
                pixmap_left_logo = self.icon_left_logo.pixmap(self.width_cluster - 4, self.height_cluster - 4,
                                                              self.icon_left_logo.Active,
                                                              self.icon_left_logo.On)
                self.image_client_logo.setPixmap(pixmap_left_logo)
            else:
                return

        except Exception as e:
            self.logger.exception("loading new logo", e)

    def change_center_logo(self):
        title = _("Select the image logo")
        dialog = QtWidgets.QFileDialog()
        dialog.setViewMode(QtWidgets.QFileDialog.Detail)
        try:
            file_name = dialog.getOpenFileName(self, title, "/home/pi/Desktop", "Image Files (*.png)")
            if file_name[0] is not "":
                subprocess.call("cp -R " + file_name[0] + " /home/pi/RookMotion/settings/center_logo.png", shell=True)
                self.icon_center_logo = QtGui.QIcon('/home/pi/RookMotion/settings/center_logo.png')
                pixmap_center_logo = self.icon_center_logo.pixmap(self.width_cluster - 4, self.height_cluster - 4,
                                                                  self.icon_center_logo.Active,
                                                                  self.icon_center_logo.On)
                self.image_client_logo.setPixmap(pixmap_center_logo)
            else:
                return

        except Exception as e:
            self.logger.exception("loading new logo", e)

    def change_right_logo(self):
        title = _("Select the image logo")
        dialog = QtWidgets.QFileDialog()
        dialog.setViewMode(QtWidgets.QFileDialog.Detail)
        try:
            file_name = dialog.getOpenFileName(self, title, "/home/pi/Desktop", "Image Files (*.png)")
            if file_name[0] is not "":
                subprocess.call("cp -R " + file_name[0] + " /home/pi/RookMotion/settings/logo_gim.png", shell=True)
                self.icon_client_logo = QtGui.QIcon('/home/pi/RookMotion/settings/logo_gim.png')

                if self.display_mode == "top_bar":
                    pixmap_client_logo = self.icon_client_logo.pixmap(self.width_cluster - 4,
                                                                      self.height_right_logo - 4,
                                                                      self.icon_client_logo.Active,
                                                                      self.icon_client_logo.On)
                    self.image_client_logo.setPixmap(pixmap_client_logo)
                    self.image_client_logo.setFixedHeight(self.height_right_logo - 4)
                else:
                    pixmap_client_logo = self.icon_client_logo.pixmap(self.width_cluster - 4, self.height_cluster - 4,
                                                                      self.icon_client_logo.Active,
                                                                      self.icon_client_logo.On)
                    self.image_client_logo.setPixmap(pixmap_client_logo)
            else:
                return

        except Exception as e:
            self.logger.exception("loading new logo", e)

    def clear_group_menu(self):
        try:
            for id_cluster in range(self.users_to_show):
                if self.clusters[id_cluster].cluster['id'] is None:
                    continue

                if self.clusters[id_cluster].frame['name'] != 'enable':
                    self.remove_cluster_acquisition(id_cluster)
                    self.show_enable_frame(id_cluster)
            self.fill_comboboxes('empty')
            self.show_cluster_logos()
        except Exception as e:
            self.logger.exception("cleaning group", e)

    def show_cluster_logos(self):
        if not self.display_mode == "top_bar":
            # Check if there's an active connection in cluster 0, close it and show the logo
            # update: we should't stop clusters, user must stop them
            if self.clusters[0].frame['name'] != 'acquisition':
                # self.remove_cluster_acquisition(0)
                self.show_logo_frame(0)

            if self.users_to_show == 4:
                return
            else:
                logo_cluster_index = logos_cluster_indexes[self.users_to_show]
                if self.clusters[logo_cluster_index].frame['name'] != 'acquisition':
                    # update: we should't stop clusters, user must stop them
                    # self.remove_cluster_acquisition(logo_cluster_index)
                    self.show_logo_frame(logo_cluster_index)

        # self.action_show_cluster_logo.setDisabled(True)

    def show_cluster_clock(self):
        # MUESTRA LA HORA EN EL CLUSTER 3
        self.layouts_stacked_clusters[3].setCurrentIndex(0)
        self.action_show_cluster_clock.setDisabled(True)

    def show_clusters_grid_frame(self):
        stack_index = self.available_grid_sizes.index(self.users_to_show)
        self.layouts_stacked_grids.setCurrentIndex(stack_index)

    def set_trophy_winners(self, session_id):
        try:
            calories_winners = self.rewards.get_calories_winners(session_id)
            self.rewards.store_calories_winners(calories_winners)
            self.carousel.set_trophy_calories(calories_winners, self.clusters)

            green_zone_winner = self.rewards.get_green_zone_winner(session_id)
            self.rewards.store_winner(green_zone_winner, 'trophy_green_zone')
            self.carousel.set_trophy(green_zone_winner, 'trophy_green_zone', self.clusters)

        except Exception as e:
            self.logger.exception("Setting winner green zone", e)

    #################################################################################################################
    # Grid Resize stuff
    #################################################################################################################
    def handle_grid_resize_action(self, action: QtWidgets.QAction = None):
        try:
            if action is not None:  # Means it comes from the menu
                grid_size = action.data()
                action.setDisabled(True)
                action.setChecked(True)
            else:
                grid_size = self.users_to_show
            self.set_grid_layout_dimension(grid_size)
            self.update_grid_resize_actions()
        except Exception as e:
            self.logger.exception("handling grid resize action", e)

    def update_grid_resize_actions(self, disable_all=False):
        try:
            if self.group_grid_action is not None:
                actions = self.group_grid_action.actions()
                for action_ in actions:
                    # Is the selected or do I need to disable all?
                    if action_.data() == self.users_to_show or disable_all:
                        action_.setChecked(True)
                        action_.setDisabled(True)
                    else:
                        action_.setChecked(False)
                        action_.setDisabled(False)
        except Exception as e:
            self.logger.exception("updating grid resize actions", e)

    def resize_lists(self):
        try:
            if len(self.clusters) > self.users_to_show:
                self.clusters = self.clusters[:self.users_to_show]

                # Threads
                self.threads_acquisition = self.threads_acquisition[:self.users_to_show]
                self.threads_plot = self.threads_plot[:self.users_to_show]

                # Solo
                self.session_solo_mode = self.session_solo_mode[:self.users_to_show]
                self.dot_read_indicator = self.dot_read_indicator[:self.users_to_show]

                # Plots
                self.session_records = self.session_records[:self.users_to_show]

            elif len(self.clusters) < self.users_to_show:
                difference = self.users_to_show - len(self.clusters)
                list_extension = [None] * difference

                self.clusters.extend([Cluster() for _ in range(difference)])

                # Threads
                self.threads_acquisition.extend(list_extension)
                self.threads_plot.extend(list_extension)

                # Solo
                self.session_solo_mode.extend(list_extension)
                self.dot_read_indicator.extend(list_extension)

                # Plots
                self.session_records.extend(list_extension)

        except Exception as e:
            self.logger.exception("resizing lists", e)

    def get_last_session(self):
        try:
            with DB(['sessions'], caller='gui') as db:
                session_id = db.sessions.get_last_session_id()
                status = db.sessions.get_session_info(session_id, 'status')
                duration = db.sessions.get_session_info(session_id, 'duration')
                created = db.sessions.get_session_info(session_id, 'created')
                return {'id': session_id, 'status': status, 'duration': duration, 'created': created}
        except Exception as e:
            self.logger.exception("Getting last session", e)

    def last_session_finished(self, session: Dict):
        try:
            session_status = session.get('status', None)
            finished_status = ['finished', 'canceled']
            if session_status in finished_status:
                return True

            # The session didn't finish
            session_duration = session.get('duration', None)
            if session_duration is None:
                return True  # Invalid duration :( Won't recover

            session_start = session.get('created', None)
            if Utilities.is_datetime(session_start) is False:
                return True  # Invalid start time :( Won't recover

            expected_finish_time = session_start + datetime.timedelta(minutes=session_duration)
            current_time = datetime.datetime.now()

            if current_time > expected_finish_time:
                return True

            return False

        except Exception as e:
            self.logger.exception("Checking if last session finished", e)

    def verify_licence(self):
        try:
            with DB(['system'], caller='gui') as db:
                licence_ = db.system.get_licence()
            now = datetime.datetime.now()
            if licence_ is not None:
                licence_status = licence_[0]
                if licence_status is not None:
                    self.logger.warning("Licence status: {}".format(licence_status))
                    if licence_status == "active":
                        self.logger.info("Licence active")
                        return
                    elif licence_status == "inactive":
                        self.logger.warning("Licence inactive")
                        msg = _(
                            "RookMotion hasn't been able to receive your payment information\n"
                            "Please contact with us if this is an error\n"
                            "contacto@rookmotion.com")

                        message_box = DialogMessage()
                        message_box.set_text(msg)
                        message_box.set_icon("warning")
                        message_box.remove_buttons()
                        message_box.execute_message()

                    else:
                        self.logger.warning("Licence not initialized")

        except Exception as e:
            self.logger.exception("verifying licence", e)

    def enable_all_clusters(self):
        try:

            msg = _("Do you want to add all the group users? \n\n {0} seconds")
            decision = show_dialog(msg, timeout=4, accept_button_text=_("Add"), cancel_button_text=_("Cancel"))
            if decision == _("Add") or decision is None:
                self.logger.info('All user clusters have been enabled')
                msg = _("Adding users. \n\n Please wait")
                show_dialog(msg, timeout=1)

                for cluster_id in range(self.users_to_show):
                    if self.layouts_stacked_clusters[cluster_id].currentIndex() == Cluster.get_frame_id('selection'):
                        self.start_acquisition_cluster(cluster_id)
                        # time.sleep(0.01)
                # self.enable_disable_timer_cluster_killer()
                # self.thread_connections.update_clusters()
                return True

            else:
                return

        except Exception as e:
            self.logger.exception("Enabling all clusters", e)

    def restart_last_session(self, session: Dict):
        try:
            msg = _("Last session will resume \n\n {0} seconds")
            decision = show_dialog(msg, timeout=2, accept_button_text=_("Ok"), cancel_button_text=_("Cancel"))

            if decision == _("Ok") or decision is None:
                session_id = session.get('id', None)
                duration = session.get('duration', None)
                if session_id is None or duration is None:
                    self.logger.warning("restart_last_session got None session")
                    return
                self.start_session(session)
                self.logger.info('Session restored')
            else:
                return

        except Exception as e:
            self.logger.exception("enabling all clusters", e)

    def update_all_session_reports_status(self, status='finished'):
        try:
            with DB(['sessions'], caller='gui') as db:
                db.sessions.set_session_status(self.session_id, status)
                for cluster_id, cluster in enumerate(self.clusters):
                    self.set_user_session_status(cluster_id, status)
            return None
        except Exception as e:
            self.logger.exception("Update all session reports", e)

    def send_reports_thread(self):
        """
        :return:
        """
        try:
            self.update_all_session_reports_status('finished')
            for cluster in self.clusters:
                if cluster.frame['name'] == 'acquisition' is not None:
                    cluster_id = cluster.cluster['id']
                    self.preset_and_show_chart_frame(cluster_id)
                    try:
                        session_records = SessionRecords(cluster.user_session)
                        self.session_records[cluster_id] = session_records
                        thread_plot = ThreadPlot(cluster, session_records)
                        thread_plot.thread.dataReady.connect(self.handle_plot_finished, QtCore.Qt.QueuedConnection)
                        thread_plot.plot()
                    except Exception as e_:
                        self.logger.exception("Plotting cluster: {}".format(cluster_id), e_)

            self.set_trophy_winners(self.session_id)
            # self.upload_finished_sessions()

        except Exception as e_:
            self.logger.exception("Sending reports", e_)

    def upload_finished_sessions(self):
        try:
            if self.network.test_internet():
                with WS() as web_service:
                    # web_service.users.sync_all()
                    web_service.sessions.sync_all()
                    self.logger.info("Reports uploaded")

            else:
                self.logger.warning("Internet not available, sending later")

        except Exception as e_:
            self.logger.exception("Uploading to WebService", e_)

    def send_reports(self):
        try:
            thread = Thread(target=self.send_reports_thread)
            thread.start()
        except Exception as e:
            self.logger.exception('Sending reports', e)

    def send_report_thread(self, cluster_id):
        cluster = self.clusters[cluster_id]
        user_id = cluster.user['id']
        session_id = cluster.session['id']
        self.set_user_session_status(cluster_id, 'finished')
        with DB(['users', 'sessions'], caller='gui') as db:
            self.set_user_session_status(cluster_id, 'finished')
            db.sessions.set_session_status(session_id, 'finished')
            try:
                if user_id is None:
                    self.logger.error("send_report_thread: User id is None")
                    return

                try:
                    self.preset_and_show_chart_frame(cluster_id)
                    session_records = SessionRecords(cluster.user_session)
                    self.session_records[cluster_id] = session_records
                    thread_plot = ThreadPlot(cluster, session_records)
                    thread_plot.thread.dataReady.connect(self.handle_plot_finished, QtCore.Qt.QueuedConnection)
                    thread_plot.plot()
                except Exception as e_:
                    self.logger.exception("Plotting cluster: {}".format(cluster_id), e_)

                # self.upload_finished_sessions()
            except Exception as e:
                self.logger.exception("Generating report", e)

    def send_report(self, cluster_id):
        try:
            # f = inspect.stack()[0][3]
            # # self.last_action = f

            # self.logger.debug(caller_format_id.format(inspect.stack()[0][3], cluster_id, inspect.stack()[1][3]))
            thread = Thread(target=self.send_report_thread, args=(cluster_id,))
            thread.start()
            message_box = DialogMessage()
            message_box.set_text(_("Sending report"))
            message_box.set_timer(0)
            message_box.execute_message()
            thread.join()

        except Exception as e:
            self.logger.exception('Sending report', e)

    def process_events(self):
        try:
            QtCore.QCoreApplication.processEvents()
            pass
        except Exception as e:
            self.logger.exception("Processing events", e)

    """def store_all_clusters_selections(self):
        for cluster in self.clusters:
            frame = cluster.frame['name']
            if frame == 'acquisition' or frame == 'selection':
                self.store_cluster_selections(cluster.cluster['id'])"""

    # TODO: Move to tools
    def clear_combobox_selection(self, combobox: ExtendedQCombobox):
        try:
            combobox.setCurrentIndex(0)
        except Exception as e:
            self.logger.exception("Clearing combobox", e)

    # TODO: Move to tools
    def get_verified_cluster_selection(self, cluster_id, combobox_type: str):
        try:
            if combobox_type == 'user':
                combobox = self.comboboxes_select_user_cluster[cluster_id]
            elif combobox_type == 'sensor':
                combobox = self.comboboxes_select_sensor_cluster[cluster_id]
            else:
                self.logger.warning("Unhandled element type selection verification")
                return

            id_selected = combobox.currentData()

            # TODO: Remove verifications?
            #  All this verifications were because in the past the text entered in the
            #  combobox was used as id, but now we use the Data linked to the combobox row
            if Utilities.is_number(id_selected) is False:
                self.logger.warning("Selected {} on cluster: is not number".format(combobox_type, cluster_id))
                msg = _("Invalid {} selection".format(combobox_type))
                show_dialog(msg, timeout=0, accept_button_text=_('Ok'))
                self.clear_combobox_selection(combobox)
                return

            element_id = int(id_selected)
            with DB(['users', 'sensors'], caller='gui') as db:
                if combobox_type == 'user':
                    element_id = db.users.get_user_info(element_id, 'user_id')
                elif combobox_type == 'sensor':
                    element_id = db.sensors.get_sensor_info(element_id, 'sensor_id')

            if element_id is None:
                self.logger.warning("Invalid {} selection. Cleaning cluster {} combobox"
                                    .format(combobox_type, cluster_id))
                self.clear_combobox_selection(combobox)
                return

            return element_id
        except Exception as e:
            self.logger.exception("get_verified_cluster_selection, cluster: {}".format(cluster_id), e)

    def store_cluster_selections(self, cluster_id):
        try:
            # Store user selection
            last_id = self.clusters[cluster_id].user['id']
            user_id = self.get_verified_cluster_selection(cluster_id, 'user')
            self.clusters[cluster_id].set_user(user_id)

            # TODO: user_session should be cleared when cluster is removed
            if last_id is not None and last_id != user_id:
                self.logger.debug("User changed so new session is created")
                self.clusters[cluster_id].user_session = None

            # Store sensor selection
            sensor_id = self.get_verified_cluster_selection(cluster_id, 'sensor')
            self.clusters[cluster_id].set_sensor(sensor_id)

            # Store cluster id
            self.clusters[cluster_id].set_cluster_id(cluster_id)

        except Exception as e:
            self.logger.exception("store_cluster_selection, cluster: {}".format(cluster_id), e)

    def set_last_cluster_upd(self, cluster_id):
        try:
            # self.logger.debug(caller_format_id.format(inspect.stack()[0][3], cluster_id, inspect.stack()[1][3]))
            self.clusters[cluster_id].sensor['last_read'] = datetime.datetime.now()
        except Exception as e:
            self.logger.exception("Setting last cluster update", e)

    ############################
    # Acquisition frame methods
    ############################
    def clear_acquisition_frame(self, cluster_id):
        try:
            self.set_acquisition_frame_color(cluster_id, effort=None)
            self.acquisition_frame_pseudonym_label[cluster_id].setText("")
            self.acquisition_frame_sensor_label[cluster_id].setText("")
            self.acquisition_frame_hr_label[cluster_id].setText("0 {}".format(_('bpm')))
            self.acquisition_frame_calories_label[cluster_id].setText("0 {}".format(_('kcal')))
            self.acquisition_frame_steps_label[cluster_id].setText("0 {}".format(TrainingType.get_steps_units()))
            self.acquisition_frame_effort_label[cluster_id].setText(_("wear"))
            self.acquisition_frame_effort_label[cluster_id].setFont(self.font_wear)
        except Exception as e:
            self.logger.exception("Cleaning cluster: {} labels".format(cluster_id), e)

    def user_sensor_selected_are_valid(self, cluster_id):
        def id_is_already_on_use(cluster_id_: int, cluster_were_found_: int):
            try:
                if cluster_were_found_ is not None:
                    # Means: is used somewhere
                    if cluster_were_found_ != cluster_id_:
                        # Means: This user is being used in other cluster
                        return True  # cluster won't be added
            except Exception as e_:
                self.logger.exception("id_is_already_on_use. CLuster: {}".format(cluster_id), e_)

        try:
            # User
            user_id = self.get_verified_cluster_selection(cluster_id, 'user')
            if user_id is None:
                return
            cluster_where_found = ClustersTools.find_user_in_clusters(self.clusters, user_id)
            if id_is_already_on_use(cluster_id, cluster_where_found):
                self.reject_user_sensor_selected(cluster_id, 'user', cluster_where_found)
                return
            # Sensor
            sensor_id = self.get_verified_cluster_selection(cluster_id, 'sensor')
            if sensor_id is None:
                return
            cluster_where_found = ClustersTools.find_cluster_from_sensor_id(self.clusters, sensor_id)
            if id_is_already_on_use(cluster_id, cluster_where_found):
                self.reject_user_sensor_selected(cluster_id, 'sensor', cluster_where_found)
                return

            return True  # Cluster selections are ok :D
        except Exception as e:
            self.logger.exception("user_sensor_selected_are_valid. CLuster: {}".format(cluster_id), e)

    def set_acquisition_frame_color(self, cluster_id, effort):
        if effort == 0 or effort is None:
            self.frames_acquisition_cluster[cluster_id].setPalette(self.palette_black_color)
        elif effort < 60:
            self.frames_acquisition_cluster[cluster_id].setPalette(self.zone_1_pallete_color)
        elif effort < 70:
            self.frames_acquisition_cluster[cluster_id].setPalette(self.zone_2_pallete_color)
        elif effort < 80:
            self.frames_acquisition_cluster[cluster_id].setPalette(self.zone_3_pallete_color)
        elif effort < 90:
            self.frames_acquisition_cluster[cluster_id].setPalette(self.zone_4_pallete_color)
        else:
            self.frames_acquisition_cluster[cluster_id].setPalette(self.zone_5_pallete_color)

    def start_acquisition_cluster(self, cluster_id):
        try:
            allowed_session_status = ['idle', 'running']
            if self.session_dict['status'] not in allowed_session_status:
                return

            # Pay attention, this button is re-enabled until the end, so do not return before re-enabling it!
            self.buttons_add_selected_user_cluster[cluster_id].setDisabled(True)

            massive_creation = False
            caller = Utilities.get_caller()
            if caller == ExtendedQPushButton.mousePressEvent.__name__ or caller == "human_accepted":
                msg = _("Adding container \n Please wait")
                show_dialog(msg, timeout=1)

            elif caller == self.enable_all_clusters.__name__:
                massive_creation = True

            """elif caller == self.handle_acquisition_terminated.__name__:
                recreated = True"""

            if self.user_sensor_selected_are_valid(cluster_id):
                self.store_cluster_selections(cluster_id)
                self.start_acquisition_cluster_verified(cluster_id)

            if not massive_creation:
                self.actions_after_adding_removing_cluster()
                # self.enable_disable_timer_cluster_killer()

            self.buttons_add_selected_user_cluster[cluster_id].setDisabled(False)
            self.clusters[cluster_id].destroying = False

            if caller == ExtendedQPushButton.mousePressEvent.__name__ or caller == "human_accepted":
                if self.clusters[cluster_id].sensor['owner_type'] == 'owned':
                    self.request_unban_sensor(cluster_id)
                    self.change_sensor_owner(cluster_id)
                self.clusters[cluster_id].set_cluster_added_type("manual")

        except Exception as e:
            self.logger.exception("Enabling cluster", e)

    def acquisition_cluster_set_session(self, cluster_id):
        self.clusters[cluster_id].set_session(self.session_id)
        self.clusters[cluster_id].create_user_session()

    def request_unban_sensor(self, cluster_id):
        sensor_mac = self.clusters[cluster_id].sensor['mac']
        sensor_name = self.clusters[cluster_id].sensor['name']
        self.logger.info("Requesting to unban sensor {} {}".format(sensor_name, sensor_mac))
        self.thread_connections.thread.unban_sensor(sensor_name, sensor_mac)

    def change_sensor_owner(self, cluster_id):
        try:
            user_id = self.clusters[cluster_id].user['id']
            sensor_id = self.clusters[cluster_id].sensor['id']
            with WS() as web_service:
                web_service.sensors.change_owner(user_id, sensor_id)
        except Exception as e:
            self.logger.exception("change_sensor_owner", e)

    ############################
    # Chart frame methods
    ############################
    def preset_and_show_chart_frame(self, cluster_id):
        try:
            """ Set pseudonym & empty chart & show the chart frame"""
            pseudonym = self.clusters[cluster_id].user.get('pseudonym', "")
            self.chart_frame_pseudonym_label[cluster_id].setText(pseudonym)
            self.clusters_charts_images[cluster_id].setPixmap(self.pixmap_empty_chart)
            self.show_chart_frame(cluster_id)
        except Exception as e:
            self.logger.exception("preset_and_show_chart_frame", e)

    def show_chart_frame(self, cluster_id):
        self.clusters[cluster_id].set_frame('chart')
        self.layouts_stacked_clusters[cluster_id].setCurrentIndex(Cluster.get_frame_id('chart'))

    def clear_chart_frame(self, cluster_id):
        try:
            self.chart_frame_pseudonym_label[cluster_id].setText("")
            self.clusters_charts_images[cluster_id].setPixmap(self.pixmap_empty_chart)
        except Exception as e:
            self.logger.exception("Cleaning chart frame: {} labels".format(cluster_id), e)

    def set_cluster_chart(self, cluster_id, charts_file_paths):
        icon = QtGui.QIcon(charts_file_paths.get('zones', ''))  # Zones
        pixmap = icon.pixmap(self.width_cluster - 10, self.height_cluster - 10, icon.Active, icon.On)
        self.clusters_charts_images[cluster_id].setPixmap(pixmap)

    def handle_plot_finished(self, cluster_id, graphs_file_names: dict):
        """
        "Plot" is being used as a verb for the action of creating a chart
        "Chart" is the product of plotting
        """
        try:
            if len(graphs_file_names) < 2:
                self.logger.debug("Cluster {} Plot finished but returned missing charts".format(cluster_id))
                return

            self.thread_schedule.update_session_status(session_running=False)

            self.set_cluster_chart(cluster_id, graphs_file_names)

            if self.activity_mode == "group":
                cluster = self.clusters[cluster_id]
                session_records = self.session_records[cluster_id]
                if self.carousel.add_summary(cluster, graphs_file_names, session_records) is None:
                    self.logger.warning("Failed to add carousel summary for cluster_id: {}".format(cluster_id))
                    return

        except Exception as e:
            self.logger.exception('Handling plot finished', e)

    ############################
    # Carousel methods
    ############################
    def handle_carousel_chart_clicked(self):
        self.show_clusters_grid_frame()
        self.start_carousel_timer()

    def start_carousel_timer(self):
        self.carousel.start_timer()

    def stop_carousel_timer(self):
        self.carousel.stop_timer()

    def show_carousel_frame(self, cluster_id=None):
        def set_chart_image(chart_widget, chart_picture_path):
            try:
                chart_icon = QtGui.QIcon(chart_picture_path)
                pixmap_ = chart_icon.pixmap(1100, 490, chart_icon.Active, chart_icon.On)
                chart_widget.setPixmap(pixmap_)
            except Exception as e_:
                self.logger.exception("set_chart_image: {}".format(chart_picture_path), e_)

        self.session_dict['status'] = 'summary'
        try:
            # Called by timer tick
            if cluster_id is None:
                summary = self.carousel.get_next_summary()  # type: Dict

            # Called by human click on cluster
            else:
                summary = self.carousel.get_summary(cluster_id)  # type: Dict
                self.stop_carousel_timer()

            if summary is None:
                return

            cluster_id = summary.get('cluster_id', None)
            if cluster_id is None:
                self.logger.warning("Cluster id is None on carousel")
                self.show_clusters_grid_frame()
                return

            if cluster_id == 'show_grid':
                self.show_clusters_grid_frame()
                return

            self.carousel.frame.pseudonym_label.setText("   {}".format(summary['pseudonym']))
            self.carousel.frame.calories_label.setText("{} {}".format(summary['calories'], _('kcal')))
            self.carousel.frame.hr_avg_label.setText("{} {}".format(summary['hr_avg'], _('bpm')))

            steps = summary.get('steps', None)
            if steps is not None:
                self.carousel.frame.steps_label.setText("{} {}".format(steps, TrainingType.get_steps_units()))
                self.carousel.show_steps_fields()
            else:
                self.carousel.hide_steps_fields()

            set_chart_image(self.carousel.frame.effort_chart, summary['effort_chart_path'])
            set_chart_image(self.carousel.frame.zones_chart, summary['zones_chart_path'])
            set_chart_image(self.carousel.frame.steps_chart, summary['steps_chart_path'])

            if summary.get('trophy_calories', False):
                self.carousel.frame.calories_trophy_icon.show()
            else:
                self.carousel.frame.calories_trophy_icon.hide()

            self.layouts_stacked_grids.setCurrentIndex(7)

        except Exception as e:
            self.logger.exception("Showing maximized cluster_id: {}".format(cluster_id), e)

    ############################
    # Treads Acquisition methods
    ############################
    def create_acquisition_thread(self, cluster_id):
        try:
            # self.last_action = f"""

            # None is ok if not created
            thread = ThreadAcquisition(self.clusters[cluster_id])  # Type: ThreadAcquisition
            if thread is not None:
                if thread.set_user_and_sensor():  # User and sensor are in the CLuster Object
                    if thread.initiate():
                        self.threads_acquisition[cluster_id] = thread
                        self.logger.debug("Acquisition Thread {} created".format(cluster_id))
                        return True

            # noinspection PyTypeChecker
            self.threads_acquisition[cluster_id] = None
            self.logger.error("Acquisition Thread {} failed to initialize".format(cluster_id))
            return False
        except Exception as e:
            self.logger.exception("creating thread", e)

    ############################
    # Session methods
    ############################
    def set_user_session_status(self, cluster_id, status='finished'):
        try:
            user_session_id = self.clusters[cluster_id].session['user_session_id']
            if user_session_id is not None:
                with DB(['users', 'sessions'], caller='gui') as db:
                    db.sessions.update_user_session(user_session_id, 'status', status)
        except Exception as e:
            self.logger.exception("Updating user session status", e)

    def reset_all_clusters(self):
        for cluster_id, cluster in enumerate(self.clusters):  # reset graphics
            self.show_enable_frame(cluster_id)
            self.clear_chart_frame(cluster_id)

    def reset_all_lists(self):
        self.clusters = ([Cluster() for _ in range(self.users_to_show)])
        self.session_solo_mode = [None] * self.users_to_show
        self.dot_read_indicator = [None] * self.users_to_show
        self.session_records = [None] * self.users_to_show

        # Threads
        self.threads_acquisition = [None] * self.users_to_show
        self.threads_plot = [None] * self.users_to_show

    def reset_session_variables(self):
        self.session_finished_flag = False
        self.session_started_flag = False
        self.session_dict['status'] = 'idle'

    def reset_session(self):
        at_call = datetime.datetime.now()  # We're trying to reduce the reset time, so we measure it
        show_dialog(_("Resetting session"), timeout=1)
        self.carousel.stop_timer()
        self.show_clusters_grid_frame()
        self.enable_session_start_button(enabled=False)
        self.clear_group_action.setEnabled(True)
        self.load_group_action.setEnabled(True)
        self.show_session_button('start')
        self.show_session_time_setter()

        # All these should have been executed before # TODO: delete?
        # session_progress = self.progressBar.value()
        # self.end_session(session_progress, stop_connections=False)
        # self.flag_generate_graphs = False
        # self.clear_group_menu(stop_connections=False)  # included self.show_cluster_logos()

        self.reset_all_clusters()
        self.reset_all_lists()
        self.reset_session_variables()
        self.reset_time_and_progress()
        self.carousel.reset()
        self.hide_all_steps_fields()
        self.enable_logo_and_grid_menus()
        self.show_cluster_logos()
        self.fill_comboboxes('empty')

        now = datetime.datetime.now()
        self.logger.debug("Time to reset session: {}".format((now - at_call).total_seconds()))

    def end_session(self, stop_connections=False):
        # f = inspect.stack()[0][3]
        self.session_started_flag = False
        self.session_dict['status'] = 'finished'
        self.session_progress_timer.stop()
        self.remove_all_clusters()

        if stop_connections:
            self.thread_connections.stop_connections()

        # Change menus states
        self.stopAction.setDisabled(True)  # Enable stopMenu
        self.startAction.setEnabled(False)
        self.load_group_action.setEnabled(True)
        self.edit_user_action.setEnabled(True)

        self.session_finished_flag = False

    def show_session_button(self, button_to_show):
        try:
            buttons_indexes = {'start': 0, 'stop': 1, 'reset': 2}
            button_index = buttons_indexes[button_to_show]
            self.layout_play_stop_buttons.setCurrentIndex(button_index)
        except Exception as e:
            self.logger.exception("switch_session_button", e)

    def show_session_time_setter(self):
        self.session_time_stacked_layout.setCurrentIndex(0)

    def show_session_time_countdown(self):
        self.session_time_stacked_layout.setCurrentIndex(1)

    def show_steps_fields(self, cluster_id):
        try:

            # Acquisition frame steps
            self.acquisition_frame_steps_label[cluster_id].setText("0 {}".format(TrainingType.get_steps_units()))
            self.acquisition_frame_steps_label[cluster_id].show()

            # Steps image
            icon = TrainingType.get_steps_icon()
            icon_size = self.get_acquisition_icon_size()
            pixmap = icon.pixmap(icon_size, icon_size, icon.Active, icon.On)
            self.images_steps_acquisition_cluster[cluster_id].setPixmap(pixmap)
            self.images_steps_acquisition_cluster[cluster_id].show()

            self.carousel.show_steps_fields()
        except Exception as e:
            self.logger.exception("show_acquisition_cluster_steps_field", e)

    def hide_acquisition_cluster_steps_field(self, cluster_id):
        try:
            self.acquisition_frame_steps_label[cluster_id].setText("")
            self.acquisition_frame_steps_label[cluster_id].hide()
            self.images_steps_acquisition_cluster[cluster_id].hide()
        except Exception as e:
            self.logger.exception("hide_acquisition_cluster_steps_field", e)

    def hide_all_steps_fields(self):
        try:
            self.carousel.hide_steps_fields()
            for cluster_id, cluster in enumerate(self.clusters):
                self.hide_acquisition_cluster_steps_field(cluster_id)
        except Exception as e:
            self.logger.exception("hide_all_acquisition_cluster_steps_field", e)

    def get_acquisition_icon_size(self):
        # Resize icons
        sizes = {4: 70, 12: 40, 20: 36, 25: 23, 30: 20, 35: 20, 42: 18}
        return sizes.get(self.users_to_show, 23)

    def thread_connections_init(self):
        th = self.thread_connections
        th.initiate(self.users_to_show)
        th.thread.issues_signal.connect(self.handle_thread_connection_issues, QtCore.Qt.QueuedConnection)
        th.thread.sensor_discovered_signal.connect(self.handle_sensor_discovered, QtCore.Qt.QueuedConnection)
        th.thread.sensor_banned.connect(self.remove_cluster_acquisition, QtCore.Qt.QueuedConnection)
        th.start()

    def handle_thread_connection_issues(self, issue):
        if show_critical_dialog(issue) == _('Restart'):
            self.reboot_system()

    def handle_sensor_discovered(self, sensor_discovered: Dict):
        try:
            mac_discovered = sensor_discovered.get("mac", '').upper()
            if mac_discovered == '':
                self.logger.error("Sensor discovered with None MAC")
                return

            if self.cluster_is_being_added:
                self.logger.warning("Skipped {}. adding is busy".format(mac_discovered))
                return

            with DB(['users', 'sensors'], caller='gui') as db:
                owners = ["owned", "borrowed"]
                for owner in owners:
                    if ClustersTools.find_cluster_from_sensor_mac(self.clusters, mac_discovered):
                        self.logger.debug("Found mac {}. But already used. Connecting?".format(mac_discovered))
                        return

                    sensor_id = db.sensors.get_sensor_id_from_mac(mac_discovered, owner)

                    if sensor_id is None:
                        continue  # Sensor is not in DB

                    if ClustersTools.find_cluster_from_sensor_id(self.clusters, sensor_id):
                        continue  # Sensor already in use

                    user_id = db.sensors.get_sensor_owner(sensor_id)
                    if user_id is None:
                        self.logger.error("Sensor {} has None owner {}".format(mac_discovered, owner))
                        continue

                    if db.users.get_user_info(user_id, 'active') != 1:
                        self.logger.warning("Sensor {} owner {} is deactivated".format(mac_discovered, user_id))
                        continue

                    if ClustersTools.find_user_in_clusters(self.clusters, user_id):
                        self.logger.warning("Found sensor_id {} of user_id {}. But user used.".format(sensor_id, user_id))
                        continue  # User already in use

                    # All tests passed, add the user and sensor
                    self.logger.info("Auto adding sensor_id {} with user_id {}".format(sensor_id, user_id))
                    self.add_cluster_for_discovered_sensor(user_id, sensor_id)
                    break
        except Exception as e:
            self.logger.exception("handle_sensor_discovered", e)

    #################################################################
    # Auto adding
    #################################################################
    def add_cluster_for_discovered_sensor(self, user_id, sensor_id):
        self.cluster_is_being_added = True  # Be careful to set false before returns
        try:
            if self.check_user_and_sensor_not_none(user_id, sensor_id) is not True:
                self.cluster_is_being_added = False
                return

            cluster_id = self.find_best_cluster_for_auto_adding(user_id, sensor_id)
            if cluster_id is None:
                self.logger.error("add_cluster_for_discovered_sensor got None cluster_id")
                self.cluster_is_being_added = False
                return

            # print(ComboBoxesTools.get_combo_selection(self.comboboxes_select_user_cluster[cluster_id]))
            # print(ComboBoxesTools.get_combo_selection(self.comboboxes_select_sensor_cluster[cluster_id]))

            self.show_enable_frame(cluster_id)
            self.show_selection_frame(cluster_id)

            # self.fill_comboboxes()  # TODO @pepe This is necessary?
            # self.logger.debug("Auto adding cluster: {} user: {} sensor: {}".format(cluster_id, user_id, sensor_id))
            # print(ComboBoxesTools.get_combo_selection(self.comboboxes_select_user_cluster[cluster_id]))
            # print(ComboBoxesTools.get_combo_selection(self.comboboxes_select_sensor_cluster[cluster_id]))

            if self.fill_one_combobox(cluster_id, user_id, sensor_id):
                # time.sleep(5)
                self.logger.info("Auto adding cluster: {} user: {} sensor: {}".format(cluster_id, user_id, sensor_id))
                # print(ComboBoxesTools.get_combo_selection(self.comboboxes_select_user_cluster[cluster_id]))
                # print(ComboBoxesTools.get_combo_selection(self.comboboxes_select_sensor_cluster[cluster_id]))
                self.clusters[cluster_id].set_cluster_added_type("auto")
                self.start_acquisition_cluster(cluster_id)
            else:
                # failed, so reset frame
                self.show_enable_frame(cluster_id)
        except Exception as e:
            self.logger.exception("add_cluster_for_discovered_sensor", e)
        self.cluster_is_being_added = False

    def find_best_cluster_for_auto_adding(self, user_id, sensor_id):
        cluster_id = None
        try:
            # Any cluster with same user and sensor?
            cluster_id = ComboBoxesTools.find_cluster_with_exact_selection(
                user_id, sensor_id, self.comboboxes_select_user_cluster, self.comboboxes_select_sensor_cluster)
            if cluster_id is not None:
                # ok, cluster found but is it acquiring?
                if self.clusters[cluster_id].get_frame_name() == 'acquisition':
                    self.logger.warning("Found sensor: {} of user: {}. But both already acquiring".
                                        format(sensor_id, user_id))
                    return  # User and sensor are already on acq in another cluster
            else:
                # user and sensor exact selection not found, so find the first one empty
                cluster_id = ClustersTools.find_first_empty_cluster(self.clusters)

        except Exception as e:
            self.logger.exception("find_best_cluster_for_auto_adding", e)
        return cluster_id

    def check_user_and_sensor_not_none(self, user_id, sensor_id):
        if user_id is None:
            self.logger.error("add_cluster_for_discovered_sensor got None user_id")
            return
        if sensor_id is None:
            self.logger.error("add_cluster_for_discovered_sensor got None user_id")
            return

        return True

    #################################################################
    # OsSignals
    #################################################################
    def register_os_signals(self):
        try:
            signal.signal(signal.SIGUSR1, self.handle_user1_signal)
            self.logger.info("Registered OS signals to PID: {}".format(os.getpid()))
        except Exception as e:
            self.logger.exception("register_os_signals", e)

    def handle_user1_signal(self, signal_id, frame):
        self.logger.debug("User/Sensors db change signal received. Refilling combo-boxes")
        self.fill_comboboxes()
