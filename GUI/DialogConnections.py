# -*- coding: utf-8 -*-
"""
Created on 22 Feb 2020
@author: Ein
"""
import datetime

import psutil
from PyQt5 import QtGui, QtWidgets, QtCore

import ColorPalettes
import constants
from Language import _
from Logger import Logger


class DialogConnections (QtWidgets.QDialog):
    # noinspection PyArgumentList
    def __init__(self, clusters, parent=None):
        try:
            QtWidgets.QDialog.__init__(self, parent)
            # remove window title
            # self.setWindowFlags(QtCore.Qt.SplashScreen)
            self.setWindowTitle("Connections")

            self.logger = Logger(name='DiaConn', level="debug")
            self.clusters = clusters
            headers = ['user_id', 'sensor_id', 'hci', 'battery', 'contact_ok', 'mac', 'frame', 'last_read', 'th_status', 'destroying', 'next_action']

            self.headers = headers
            # self.showFullScreen()
            # self.showMinimized()
            self.showMaximized()
            # self.setFixedSize(250, 250)
            font = QtGui.QFont()
            font.setPointSize(9)
            self.setFont(font)

            self.layout = QtWidgets.QVBoxLayout(self)

            self.table_connections = QtWidgets.QTableWidget()
            self.table_connections.setRowCount(len(clusters))
            self.table_connections.setColumnCount(len(headers))

            self.table_connections.setHorizontalHeaderLabels(headers)
            self.table_connections.horizontalHeader().setSectionResizeMode(QtWidgets.QHeaderView.Stretch)
            self.table_connections.setVerticalHeaderLabels(map(str, range(len(clusters))))

            self.table_connections.setFont(font)
            self.font_user = QtGui.QFont()

            self.system_headers = ["CPU", "m tot", "m available", "m percent", "m used", "m free", "m active", "m inactive", "m buffers", "m cached", "m shared", "m slab"]

            self.table_system = QtWidgets.QTableWidget()
            self.table_system.setRowCount(1)
            self.table_system.setColumnCount(len(self.system_headers))

            self.table_system.setHorizontalHeaderLabels(self.system_headers)
            self.table_system.horizontalHeader().setSectionResizeMode(QtWidgets.QHeaderView.Stretch)
            self.table_system.setVerticalHeaderLabels(["info"])

            self.table_system.setFont(font)
            self.table_system.setMaximumHeight(50)

            self.font_user = QtGui.QFont()

            self.close = QtWidgets.QPushButton(_('Close'), self)
            # noinspection PyUnresolvedReferences
            self.close.clicked.connect(self.reject)

            self.layout.addWidget(self.table_system)
            self.layout.addWidget(self.table_connections)
            self.layout.addWidget(self.close)
            self.setLayout(self.layout)

            # dark theme
            dark_palette = ColorPalettes.dialog_palette()
            self.autoFillBackground()
            self.setPalette(dark_palette)

            self.adjustSize()
            self.timer = QtCore.QTimer(self)
            self.timer.setInterval(999)
            # noinspection PyUnresolvedReferences
            self.timer.timeout.connect(self.update_tables)
            self.timer.start()

        # def handle_thread_signal(self, cluster_id):

        except Exception as e:
            self.logger.exception("Dialog connections", e)

    def update_tables(self):
        self.update_system_info()
        self.update_connections()

    # noinspection PyCallByClass,PyTypeChecker
    def update_connections(self):
        try:
            # self.logger.debug("Updating")
            now = datetime.datetime.now()
            for cluster_id, cluster, in enumerate(self.clusters):
                for i_header, key in enumerate(self.headers):
                    try:
                        # self.logger.debug(cluster)
                        text = str(cluster[key])
                        item = QtWidgets.QTableWidgetItem(text)
                        if key == 'last_read':
                            if cluster[key] is not None:
                                elapsed = (now - cluster['last_read']).total_seconds()
                                if elapsed > constants.time_to_recreate:
                                    item.setForeground(QtGui.QColor.fromRgb(255, 0, 0))
                                elif elapsed > constants.time_to_recreate * 0.5:
                                    item.setForeground(QtGui.QColor.fromRgb(255, 255, 0))
                        if key == 'th_status':
                            if cluster[key] == "disconnected":
                                item.setForeground(QtGui.QColor.fromRgb(255, 0, 0))
                            elif cluster[key] == "connecting":
                                item.setForeground(QtGui.QColor.fromRgb(255, 255, 0))
                            elif cluster[key] == "notifying":
                                item.setForeground(QtGui.QColor.fromRgb(0, 255, 255))
                            elif cluster[key] == "acquiring":
                                item.setForeground(QtGui.QColor.fromRgb(0, 255, 0))
                        if key == 'contact_ok':
                            if cluster[key] is True:
                                item.setForeground(QtGui.QColor.fromRgb(0, 255, 0))
                            elif cluster[key] is False:
                                item.setForeground(QtGui.QColor.fromRgb(255, 0, 0))
                        if key == 'destroying':
                            if cluster[key] is True:
                                item.setForeground(QtGui.QColor.fromRgb(255, 0, 0))
                        self.table_connections.setItem(cluster_id, i_header, item)
                    except Exception as e:
                        self.logger.exception("Setting item", e)
        except Exception as e:
            self.logger.exception("Updating table", e)

    def update_system_info(self):
        try:
            headers = self.system_headers[1:]

            # gives a single float value
            cpu_float = psutil.cpu_percent()
            cpu = "{}".format("{}".format(cpu_float))
            item = QtWidgets.QTableWidgetItem(cpu)
            if cpu_float > 80:
                item.setForeground(QtGui.QColor.fromRgb(255, 0, 0))
            self.table_system.setItem(0, 0, item)

            # gives an object with many fields
            # OrderedDict([('total', 51491315712), ('available', 34883887104), ('percent', 32.3), ('used', 16607428608), ('free', 34883887104)])
            memory = psutil.virtual_memory()
            for i_header, header in enumerate(headers):
                value = memory[i_header]
                text = "{}".format(value)
                item = QtWidgets.QTableWidgetItem(text)

                if header == "m_percent":
                    value = 100 - value  # = m used
                    if value > 80:
                        item.setForeground(QtGui.QColor.fromRgb(255, 0, 0))
                self.table_system.setItem(0, i_header + 1, item)

        except Exception as e:
            self.logger.exception("Updating table system", e)

