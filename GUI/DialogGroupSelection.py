# -*- coding: utf-8 -*-
"""
Created on Tue Feb 14 09:47:23 2017

@author: Develupertz
"""
from PyQt5 import QtWidgets, QtGui, QtCore

import ColorPalettes
import FontsGUI
from DataBase.DB import DB
from Language import _


class MyListWidget(QtWidgets.QListWidget):
    def clicked(self, item=None):
        if not item:
            item = self.mplfigs.currentItem()


class DialogGroupSelection (QtWidgets.QDialog):
    def __init__(self, parent=None):
        QtWidgets.QDialog.__init__(self, parent)
        try:
            # db
            self.db = DB(['users', 'sensors', 'groups'])

            self.setFixedSize(500, 370)
            self.setFont(FontsGUI.font_dialogs)

            # dark theme
            dark_palette = ColorPalettes.dialog_palette()
            self.autoFillBackground()
            self.setPalette(dark_palette)
            # remove window title
            self.setWindowFlags(QtCore.Qt.SplashScreen)

            groups_names_list = self.db.groups.get_groups_names()
            if groups_names_list is None:
                groups_names_list = []

            self.name_group_delete = None
            self.selected_group = None

            # editUserListLayout
            edit_group_list_layout = QtWidgets.QVBoxLayout(self)

            self.edit_group_list_widget = MyListWidget()

            self.edit_group_list_widget.addItems(groups_names_list)
            self.edit_group_list_widget.itemSelectionChanged.connect(self.enable_buttons)
            self.edit_group_list_widget.itemDoubleClicked.connect(self.accept)

            buttons_layout = QtWidgets.QHBoxLayout()
            edit_user_list_button_frame = QtWidgets.QFrame()

            self.button_ok_select = QtWidgets.QPushButton(_("Load group"), self)
            self.button_ok_select.clicked.connect(self.accept)
            self.disable_ok_select_button()

            self.button_cancel_select = QtWidgets.QPushButton(_("Cancel"), self)
            self.button_cancel_select.clicked.connect(self.reject)

            self.button_delete = QtWidgets.QPushButton(_("Delete group"), self)
            self.button_delete.clicked.connect(self.delete_group)
            self.disable_delete_button()

            buttons_layout.addWidget(self.button_cancel_select)
            buttons_layout.addWidget(self.button_ok_select)

            edit_group_list_layout.addWidget(self.edit_group_list_widget)
            edit_group_list_layout.addWidget(self.button_delete)
            edit_group_list_layout.addWidget(edit_user_list_button_frame)

            edit_user_list_button_frame.setLayout(buttons_layout)
            self.edit_group_list_widget.setFocus()
            if self.edit_group_list_widget.count() is not 0:
                self.edit_group_list_widget.setCurrentRow(0)
        except Exception as e:
            pass

    def delete_group(self):
        self.name_group_delete = self.get_selected_group()
        # original_index_delete = self.edit_group_list_widget.currentRow()

        msg_confirm_delete = QtWidgets.QMessageBox()
        msg_confirm_delete.setText(_("The group will be deleted. \nThis cannot be undone"))
        msg_confirm_delete.setInformativeText("Are you sure to delete group: {}?".format(self.name_group_delete))
        # dark theme
        dark_palette = ColorPalettes.dialog_palette()

        msg_confirm_delete.autoFillBackground()
        msg_confirm_delete.setPalette(dark_palette)

        msg_confirm_delete.setFont(FontsGUI.font_dialogs)
        msg_confirm_delete.setWindowFlags(QtCore.Qt.SplashScreen)
        msg_confirm_delete.setStandardButtons(QtWidgets.QMessageBox.Cancel | QtWidgets.QMessageBox.Ok)
        
        answer = msg_confirm_delete.exec_()
        
        if answer == QtWidgets.QMessageBox.Cancel:
            pass
        else:
            self.db.groups.delete_group(self.name_group_delete)
            groups_names_list = self.db.groups.get_groups_names()
            self.edit_group_list_widget.clear()
            self.edit_group_list_widget.addItems(groups_names_list)
            self.enable_buttons()

    def enable_buttons(self):
        self.selected_group = self.get_selected_group()
        if self.selected_group is not None:
            self.enable_delete_button()
            self.enable_ok_select_button()
        else:
            self.disable_delete_button()
            self.disable_ok_select_button()
            self.button_cancel_select.setFocus()

    def disable_ok_select_button(self):
        self.button_ok_select.setDisabled(True)
        disabled_palette = QtGui.QPalette()
        disabled_palette.setColor(QtGui.QPalette.ButtonText, QtGui.QColor(53, 53, 53))
        self.button_ok_select.setPalette(disabled_palette)

    def disable_delete_button(self):
        self.button_delete.setDisabled(True)
        disabled_palette = QtGui.QPalette()
        disabled_palette.setColor(QtGui.QPalette.ButtonText, QtGui.QColor(53, 53, 53))
        self.button_delete.setPalette(disabled_palette)

    def enable_ok_select_button(self):
        self.button_ok_select.setDisabled(False)
        disabled_palette = QtGui.QPalette()
        disabled_palette.setColor(QtGui.QPalette.ButtonText, QtGui.QColor(255, 255, 255))
        self.button_ok_select.setPalette(disabled_palette)

    def enable_delete_button(self):
        self.button_delete.setDisabled(False)
        disabled_palette = QtGui.QPalette()
        disabled_palette.setColor(QtGui.QPalette.ButtonText, QtGui.QColor(255, 255, 255))
        self.button_delete.setPalette(disabled_palette)

    def format_dialog(self, title, font):
        self.setWindowTitle(title)

    def get_selected_group(self):
        if self.edit_group_list_widget.currentItem() is not None:
            return str(self.edit_group_list_widget.currentItem().text())
        else:
            return None

