# -*- coding: utf-8 -*-
"""
Created on Tue Feb 14 09:47:23 2017

@author: Develupertz
"""
from PyQt5 import QtGui, QtWidgets, QtCore

import ColorPalettes
import FontsGUI
import constants
from DataBase.SQL import SQL
from Language import _


class DialogCalendar (QtWidgets.QDialog):
    def __init__(self, parent=None):
        QtWidgets.QDialog.__init__(self, parent)

        # db
        self.db = SQL()
        # self.db.connect()

        self.id_user = None
        self.session_to_send = None
        self.calendar = QtWidgets.QCalendarWidget()
        self.calendar_frame = QtWidgets.QFrame()
        self.calendar_layout = QtWidgets.QGridLayout(self)
        self.calendar.currentPageChanged.connect(self.update_calendar)  # Change month
        self.calendar.clicked.connect(self.handle_date_selected)       # Day has been selected

        self.char_normal_format = self.calendar.headerTextFormat()
        self.char_session_format = self.calendar.headerTextFormat()

        self.list_user_sessions = []
        self.day_selected = None
        self.session_ids_list = []
        # self.thread_plot = None
        self.thread_email = None
        self.session_id_selected = None

        self.font_user = QtGui.QFont()

        self.zones = constants.zones_percentages

        self.button_cancel_send_mail = QtWidgets.QPushButton(_('Close'), self)
        self.button_cancel_send_mail.clicked.connect(self.reject)

        self.button_send_mail = QtWidgets.QPushButton('Reenviar', self)
        self.button_send_mail.clicked.connect(self.button_send_mail_pressed)
        self.disable_send_button()
        # self.button_send_mail.setDisabled(True)

        self.calendar_layout.addWidget(self.calendar, 1, 0, 7, 5)
        self.calendar_layout.addWidget(self.button_cancel_send_mail, 8, 0, 1, 2)
        self.calendar_layout.addWidget(self.button_send_mail, 8, 2, 1, 3)

        self.setLayout(self.calendar_layout)

    def format_dialog(self, title, font):
        self.setWindowTitle(title)
        self.setFixedSize(900, 570)
        self.setFont(FontsGUI.font_dialogs)
        self.font_user.setPointSize(22)

        # dark theme
        dark_palette = ColorPalettes.dialog_palette()

        self.autoFillBackground()
        self.setPalette(dark_palette)
        # remove window title
        self.setWindowFlags(QtCore.Qt.SplashScreen)

        self.calendar.setDisabled(True)
        self.calendar.setGridVisible(True)
        self.disable_send_button()
        self.calendar.setFirstDayOfWeek(QtCore.Qt.Monday)

        pixmap = QtGui.QPixmap(100, 100)
        color_session = QtGui.QBrush(QtCore.Qt.green, pixmap)

        self.char_session_format.setForeground(color_session)
        self.char_session_format.setFontWeight(90)

        self.calendar.setVerticalHeaderFormat(QtWidgets.QCalendarWidget.NoVerticalHeader)
        self.calendar.setHorizontalHeaderFormat(QtWidgets.QCalendarWidget.SingleLetterDayNames)

        self.calendar.setWeekdayTextFormat(QtCore.Qt.Saturday, self.char_normal_format)  # Prevents red color
        self.calendar.setWeekdayTextFormat(QtCore.Qt.Sunday, self.char_normal_format)

    # def format_days_with_session(self):
    #     for session in self.list_user_sessions:
    #         self.calendar.setDateTextFormat(session, self.char_normal_format)

    def set_user(self, id_user):
        self.id_user = id_user
        self.list_user_sessions = []
        self.calendar.setDisabled(False)
        self.update_calendar()
        self.handle_date_selected()

    def button_send_mail_pressed(self):
        self.disable_send_button()

        for session_id in self.session_ids_list:
            dia = self.get_session_info(session_id, 'timestamp').day
            if dia == self.day_selected:
                self.session_id_selected = session_id
                self.send_email()
                # self.thread_plot = ThreadPlot(self.session_selected, 0, self.id_user)
                # self.thread_plot.thread.dataReady.connect(self.send_email, QtCore.Qt.QueuedConnection)
                break

    # Cannot be performed outside the dialog because we're not closing the dialog on each send, so the dialog won't
    # return a result
    def send_email(self):
        # self.thread_email = ThreadEmail(session=self.session_id_selected, user_id=self.id_user, db=self.db)
        # self.thread_email.send(wait=True)
        pass

    def enable_send_button(self):
        if not self.button_send_mail.isEnabled():
            self.button_send_mail.setDisabled(False)
            enabled_palette = QtGui.QPalette()
            enabled_palette.setColor(QtGui.QPalette.ButtonText, QtGui.QColor(255, 255, 255))
            self.button_send_mail.setPalette(enabled_palette)
            self.button_cancel_send_mail.setFocus()

    def disable_send_button(self):
        if self.button_send_mail.isEnabled():
            self.button_send_mail.setDisabled(True)
            disabled_palette = QtGui.QPalette()
            disabled_palette.setColor(QtGui.QPalette.ButtonText, QtGui.QColor(53, 53, 53))
            self.button_send_mail.setPalette(disabled_palette)

    def update_calendar(self):
        selected_month = self.calendar.monthShown()
        selected_year = self.calendar.yearShown()

        self.session_ids_list = self.db.read_user_month_sessions(selected_month, selected_year, self.id_user)

        if self.session_ids_list is not None:
            for session_id in self.session_ids_list:
                session_datetime = self.db.get_session_info(session_id, 'timestamp')
                session = QtCore.QDate(session_datetime.year, session_datetime.month, session_datetime.day)
                self.list_user_sessions.append(session)
                self.calendar.setDateTextFormat(session, self.char_session_format)
                # self.calendar.updateCell(session)

    def handle_date_selected(self):
        selected_day = self.calendar.selectedDate()
        if self.calendar.dateTextFormat(selected_day) == self.char_session_format:
            self.enable_send_button()
            self.day_selected = selected_day.day()
        else:
            self.disable_send_button()

