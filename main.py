from PyQt5 import QtWidgets
from GUI.WindowMain import MainWindow
import sys
from Logger import Logger
logger = Logger(name="Main", level="info")


if __name__ == "__main__":
    logger.info("Running application")
    closed = None
    try:
        app = QtWidgets.QApplication(sys.argv)
        window = MainWindow()
        closed = app.exec_()
        app.closeAllWindows()
        # sys.exit(0)
        # exit()

    except Exception as e:
        template = "Exception on main:  {0} {1!r} Line:{2}"
        exc_type, exc_obj, exc_tb = sys.exc_info()
        message = template.format(type(e).__name__, e.args, exc_tb.tb_lineno)
        logger.error(message)

    logger.info("Return Code: {}".format(closed))




