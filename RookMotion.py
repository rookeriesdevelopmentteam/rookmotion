import sys
import time

from Logger import Logger
from subprocess import call

logger = Logger(name="RM", level="info")


if __name__ == "__main__":

    closed = False
    output = None
    while closed is not True:
        try:
            output = call(["sudo", "python3", "rookmotion_main.py"])
            if output == 0:
                logger.error("Exit was requested")
                closed = True
            else:
                logger.error("Restarting RM. cod{}".format(output))

        except Exception as e:
            template = "Exception on rm:  {0} {1!r} Line:{2}"
            exc_type, exc_obj, exc_tb = sys.exc_info()
            message = template.format(type(e).__name__, e.args, exc_tb.tb_lineno)
            logger.error(message)
        time.sleep(0.1)

